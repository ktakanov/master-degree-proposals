from numpy.lib import ufunclike
import lib.data.construct as construct
import lib.data.preprocess as preprocess
import argparse
import lib.data.features as feat
import lib.input.baseline as bs
import lib.input.two_stg as twostg
from lib.utils import *
from lib.input.baseline.common import load_or_build_item_probs
import lib.config.logger as log

logger = log.get_logger()

def sample_recsys(args):
    logger.info("Reading recsys data")
    dc, db, dt = construct.read_recsys_data(False)
    
    logger.info("Original dc, db, dt" + str(dc.shape) + "," + str(db.shape) + "," + str(dt.shape)) 
    
    sample_rate = float(args.sample_rate) if args.sample_rate != None else None
    
    logger.info("Using sample_rate: " + str(sample_rate))

    logger.info("---------- Sampling data. --------- ")
    dc = construct.sample_data(dc, sample_rate)
    # db = construct.sample_data(db, sample_rate)
    # dt = construct.sample_data(dt, sample_rate)
    
    logger.info("Saving samples.") 
    
    logger.info("dc example: " + str(dc.head(5))) 
    logger.info("Total dc, db, dt" + str(dc.shape) + "," + str(db.shape) + "," + str(dt.shape)) 
    construct.save_sample_data(dc, db, dt)

def remove_nans_for_input_data(train_df,  test_df):
    train_df = train_df.fillna(0.0)

    test_df = test_df.fillna(0.0)
    return train_df, test_df

def data_construct_01(args):
    logger.info("---------- Construct base data 01 --------- ")
    use_samples = args.sample_rate is not None
    logger.info("Usa samples: " + str(use_samples))
    dc, db, _ = construct.read_recsys_data(use_samples=use_samples)
    
    logger.info("splitting") 
    train_df, test_df = construct.create_split_sids(dc) 

    logger.info("Total train_df" + str(train_df.shape))
    
    logger.info("Applying label") 
    train_df, test_df = construct.create_labels_for_input_data(db, train_df, test_df)
    
    logger.info('Train_df labels\n' + str(train_df[['session_id','label']].drop_duplicates().label.value_counts()))
    logger.info('Test_df labels\n' + str(test_df[['session_id','label']].drop_duplicates().label.value_counts()))
    
    logger.info("Applying item probs. ") 
    load_or_build_item_probs(True, train_df, db)

    train_df,  test_df = feat.apply_item_probs_for_input_data(train_df,  test_df)
    
    #TODO: create most clicked items

    logger.info("Applying item_qty_rank")
    train_df,  test_df = feat.apply_item_qty_rank_for_input_data(db, train_df, test_df)
    
    logger.info("Applying item_clicks_rank")
    train_df,  test_df = feat.apply_item_clicks_rank_for_input_data(db, train_df, test_df)
    
    logger.info("Applying basic time features") 
    train_df,  test_df = feat.apply_basic_timefeatures_for_input_data(train_df, test_df)
    
    logger.info("Applying price dist average") #removed: not affect final labels
    train_df, test_df = feat.apply_dist_price_avg_for_input_data(db, train_df, test_df)

    construct.save_constructed(train_df, test_df, '00') 
    
def data_construct_02(args, apply_under=False):   
    logger.info("---------- Construct base data 02 --------- ")
    train_df, test_df = construct.load_constructed('00')
    use_samples = args.sample_rate is not None
    logger.info("Usa samples: " + str(use_samples))
    _, db, _ = construct.read_recsys_data(use_samples=use_samples, read_dc=False)

    if apply_under:
        logger.info("Before Undersampling: train_df.shape " + str(train_df.shape))
        train_df = preprocess.apply_undersampling(train_df,  5) # TODO: apply args
        logger.info("After undersampling train_df.shape " + str(train_df.shape))
        
    logger.info("Applying time features") 
    train_df,  test_df = feat.apply_dwelltime_for_input_data(train_df, test_df)

    logger.info("Final train_df:")
    logger.info(str(train_df.head()))
    construct.save_constructed(train_df, test_df, '01') 
    
def data_construct_03(args):
    logger.info("---------- Construct base data 03 --------- ")
    train_df, test_df = construct.load_constructed('01')

    logger.info("Initial train_df:")
    logger.info(str(train_df.head()))
    logger.info(str(train_df.describe()))
    
    logger.info("Total buyers in train_df: "+ str(train_df.groupby('session_id').agg({'label': 'first'}).value_counts()))
    logger.info("Total buyers in test_df: "+ str(test_df.groupby('session_id').agg({'label': 'first'}).value_counts()))

    logger.info("Encode categories")
    train_df, test_df = construct.encode_category(train_df, test_df)

   
    logger.info("Select features of interest")
    train_df, test_df = construct.select_feat_of_interest(train_df, test_df)

    logger.info("Categorize hour")
    train_df, test_df = construct.categorize_hour_for_input_data(train_df, test_df)
   
    logger.info("Removing nans")
    train_df, test_df = remove_nans_for_input_data(train_df, test_df)

    logger.info("Total buyers in train_df: "+ str(train_df.groupby('session_id').agg({'label': 'first'}).value_counts()))
    logger.info("Total buyers in test_df: "+str(test_df.groupby('session_id').agg({'label': 'first'}).value_counts()))
    
    logger.info("Saving constructed") 
    logger.info("Total train_df, test_df" + str(train_df.shape) +  "," + str(test_df.shape)) 
    
    logger.info("Final train_df:")
    logger.info(str(train_df.head()))
    construct.save_constructed(train_df, test_df, '02') 
    
def remove_outliers(train_df):
    magic_number = 306*1.5
    train_df = train_df[train_df.dwelltime <= magic_number]
    # magic_number2 = 1000
    # train_df = train_df[train_df.item_rank <= magic_number2]
    return train_df

def binnarize_some_features(train_df):
    train_df['item_qty_rank'] = pd.qcut(train_df['item_qty_rank'],100,duplicates='drop',labels=False)
    return train_df
    
   
def preprocess_features():
    logger.info("---------- Refine features --------- ")
    train_df, test_df = construct.load_constructed('02')
    logger.info("--------- initial train_df ---------") 
    logger.info(str(train_df.head()))
    logger.info(str(train_df.describe()))
    logger.info("Total buyers in train_df: "+ str(train_df.groupby('session_id').agg({'label': 'first'}).value_counts()))
    
    # train_df = remove_outliers(train_df)
    # train_df = binnarize_some_features(train_df)
    logger.info("Select features of interest")
    train_df, test_df = preprocess.select_feat_of_interest(train_df, test_df)

    logger.info("Applying truncate")
    train_df, test_df = preprocess.apply_truncate_for_input_data(train_df,  test_df)
    logger.info("Saving prepared for single-stg")
    preprocess.save_prepared_for_single_stg(train_df, test_df)
    
    logger.info("Scaling features")
    train_df, test_df = preprocess.apply_scale_for_input_data(train_df, test_df) 
    logger.info("Saving prepared for baseline.") 
    preprocess.save_prepared_for_baseline(train_df, test_df)
   
    logger.info("Reorder features of interest")
    train_df, test_df = preprocess.select_feat_of_interest(train_df, test_df)
    
    logger.debug('Describe train_df:\n\n' + str(train_df.head()))
 
    logger.info("Total buyers in train_df: "+ str(train_df.groupby('session_id').agg({'label': 'first'}).value_counts()))
    logger.info("Saving prepared")
    
    preprocess.save_prepared(train_df, test_df)

def run_single_job_with_asking(args):
    if args.single_job == None:
        print("Select what you want!") 
        print("")
        print("Sample data: (0)")
        print("Construct data: (1)")
        print("Process data for input: (2)")
    data_process_mode = int(input()) if args.single_job == None else int(args.single_job)
    
    print("Selected mode: ",data_process_mode)
    
    run_single_job(data_process_mode, args)

   
def run_single_job(data_process_mode, args):
    logger.info("Run single jobs")
    if data_process_mode == 0:
        sample_recsys(args)
    elif data_process_mode == 1:
        data_construct_01(args)
    elif data_process_mode == 2:
        data_construct_02(args)
    elif data_process_mode == 3:
        data_construct_03(args)
    elif data_process_mode == 4:
        preprocess_features()        
    else:
        raise Exception("Passed a wrong data_process_mode: " + str(data_process_mode))

def run_multiple_jobs(args):
    logger.info("Run multiple jobs")
    jobs_to_run = [0,1,2]
    if args.run_all != True:
        jobs_to_run = args.run_jobs 
    logger.info("Jobs to run: "+str(jobs_to_run)) 
    for job in jobs_to_run:
        run_single_job(int(job), args)
     
def main():
    parser = argparse.ArgumentParser(description = 'Data manager')
    parser.add_argument('--sample-rate', type= float, default = None, help = 'Set samples proportion, ex: 0.3')
    parser.add_argument('--logger-level', type= int, default = 0, help = 'Set level debug.')
    parser.add_argument('--run-all', action='store_true', help = 'Run all jobs')

    parser.add_argument('--single-job', type= int, default = None, help = 'Pass single job to run to avoid..')
    parser.add_argument("--run-jobs", nargs="+", default=None,help="Run jobs by number list. Ex: 1 2")
    args = parser.parse_args()
    
    log.set_log_level(args.logger_level)
    
    if args.run_all == True or args.run_jobs != None:
        run_multiple_jobs(args)
    else:
        run_single_job_with_asking(args)

       

if __name__ == "__main__":
    main()