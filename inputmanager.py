import argparse
import lib.models.baseline as base
from lib.utils import *
from lib.data import construct
import lib.data.preprocess as preprocess
import lib.input.baseline as bs
import lib.input.two_stg as twostg
import lib.input.single_stg as singlestg
from lib.config.controls.baseline import Baseline
import lib.data.preprocess as preprocess
import lib.config.logger as log
from  lib.input.common import build_buyer_sessions_order_targets
logger = log.get_logger()
from sklearn.preprocessing import PowerTransformer

def build_baseline_input(args):
    logger.info("Reading recsys data:")
    _, db, _ = construct.read_recsys_data(False, False, True, False)
    train_df, test_df = preprocess.load_prepared_for_baseline()
    item_id_probs = Baseline.get_item_probs()
    logger.info("Building baseline input for the SECOND stage")
    bs.build_snd_stg_input_for_data(db, item_id_probs, train_df, test_df)
    logger.info("Building baseline input for the FIRST stage")
    bs.build_fst_stg_input_for_data(item_id_probs, train_df,  test_df, args.rebuild_slice_baseline_first_stg)


def build_twostg_input(args):
    logger.info("Reading recsys data:")
    _, db, _ = construct.read_recsys_data(args.use_samples, False, True, False)
    
    train_df,  test_df = preprocess.load_prepared()
    
    train_df,  test_df = preprocess.select_feats_for_twosttrans(train_df, test_df)
    
    logger.info('Describe train_df:\n\n' + str(train_df.describe()))
    
    for_stage = args.for_stage
    
    if for_stage == 0 or for_stage == None:
        logger.info("Building two-stg input for the FIRST stage")
    twostg.create_inputs_for_first(train_df, test_df, db)

    if for_stage == 1 or  for_stage == None:
        logger.info("Building two-stg input for the SECOND stage")
        use_ml = bool(args.use_multi_label)
        twostg.create_inputs_for_second(db, train_df,  test_df, use_ml)
        
def build_singlestg_input(args):
    _, db, _ = construct.read_recsys_data(False, False, True, False)
    logger.info("Reading recsys data:")

    train_df,  test_df = preprocess.load_prepared_for_single_Stg()

    # logger.info("Before Undersampling: train_df.shape " + str(train_df.shape))
    # train_df = preprocess.apply_undersampling(train_df,  10) # TODO: apply args
    # logger.info("After undersampling train_df.shape " + str(train_df.shape))

    # train_df, valid_df, test_df = preprocess.load_prepared()
    logger.info('Describe train_df:\n\n' + str(train_df.describe()))

    train_df,  test_df = preprocess.select_feats_for_singlesttrans(train_df, test_df)
    
    logger.info("Scale for gan")
    train_df,  test_df = singlestg.scale_for_gan(train_df,  test_df)
    
    logger.info("Building singlestg input")
    singlestg.create_input_for_data(train_df,  test_df, db, int(args.single_stage_batch_size))
    
    logger.info('Describe train_df:\n\n' + str(train_df.describe()))

def build_prerequisite_for_evaluation():
    _, db, _ = construct.read_recsys_data(False, False, True, False)
    _, test_df = preprocess.load_prepared()

    build_buyer_sessions_order_targets(test_df, db)

def build_model_input(args):
    logger.info("---------- Building model input --------- ")
    if args.model_input == 0:
        logger.info("Building baseline input **")
        build_baseline_input(args)
    elif args.model_input == 1:
        logger.info("Building two-stg input **")
        build_twostg_input(args)
    elif args.model_input == 2:
        logger.info("Building single-stg input **")
        build_singlestg_input(args)
    elif args.model_input == 3:
        logger.info("Building prerequisites for evaluation **")
        build_prerequisite_for_evaluation()

def run_all(args):
    build_baseline_input(args)
    build_twostg_input(args)
    build_singlestg_input(args)
    build_prerequisite_for_evaluation()
    
def main():
    parser = argparse.ArgumentParser(description = 'Models training and test.')
    parser.add_argument('--logger-level', type= int, default = 0, help = 'Set level debug.')
    parser.add_argument('--use-samples', action='store_true',default=False, help = 'Read samples instead of all yoochoose files.')
    parser.add_argument('--use-multi-label', action='store_true',default=False, help = 'Construct input for crf model.')
    parser.add_argument('--model-input', type= int, default = 0, help = 'Set model input to construct input.')
    parser.add_argument('--single-stage-batch-size', type= int, default = 64*5000, help = 'Batch for single-stage.')
    parser.add_argument('--for-stage', type= int, default = None, help = 'Select stage') # implement for other models
    parser.add_argument('--build-item-probs', action='store_true', default=False, help = 'Run all jobs')
    parser.add_argument('--rebuild-slice-baseline-first-stg', action='store_true', help = 'Rebuild first stage slice features')
    parser.add_argument('--run-all', action='store_true', help = 'Run for all models.')
    
    args = parser.parse_args() 
    log.set_log_level(args.logger_level)

    if args.run_all == True:
        run_all(args)
    else:
        build_model_input(args)
    
if __name__ == "__main__":
    main()