import argparse
import lib.models.baseline as base
import lib.models.two_stage as twostg
import lib.models.single_stage as singlestg
import lib.models.single_nogan as nogan
from lib.utils import *
from lib.data import construct

import lib.config.logger as log
logger = log.get_logger()

def train_baseline(args):
    
    sel_stage = args.for_stage
    only_evaluate = bool(args.only_evaluate)
    
    if sel_stage == None or sel_stage == 0:
        base.train_and_evaluate_fst(only_evaluate)
    if sel_stage == None or sel_stage == 1:
        base.train_and_evaluate_snd(only_evaluate)
    
def train_twostg(args):
    only_evaluate = bool(args.only_evaluate)
    train_epochs =(args.train_epochs)
    batch_size =(args.batch_size)
    patience = (args.patience)

    undersampling_prop = float(args.under_prop)

    sel_stage = args.for_stage

    if sel_stage == None or sel_stage == 0:
        logger.info("Training first stage TWOSTTRANS")
        twostg.train_and_evaluate_fst(only_evaluate, train_epochs, batch_size, patience, undersampling_prop)

    if sel_stage == None or sel_stage == 1:
        logger.info("Training second stage TWOSTTRANS")
        twostg.train_and_evaluate_snd(only_evaluate, train_epochs, batch_size, patience)

def train_singlestg(args):
    singlestg.train_and_evalute(args)

def train_nosingle(args):
    only_evaluate = (args.only_evaluate)
    train_epochs = (args.train_epochs)
    batch_size =(args.batch_size)
    patience = (args.patience)
    use_gru = (args.use_gru)
    
    nogan.train_and_evalute_nogan(only_evaluate, train_epochs, batch_size, patience, use_gru)
    
    
def select_model(args):
    choice = int(args.model_input)
    if choice == 0:
        train_baseline(args)
    elif choice == 1:
        train_twostg(args)
    elif choice == 2:
        train_singlestg(args)
    else:
        train_nosingle(args)

def run_all(args):
    train_baseline(args)
    train_twostg(args)
    train_singlestg(args)
    train_nosingle(args)
 
def main():
    parser = argparse.ArgumentParser(description = 'Models training and test.')
    parser.add_argument('--logger-level', type= int, default = 0, help = 'Set debug level.')
    parser.add_argument('--model-input', type= int, default = 0, help = 'Set model input to construct input.')
    parser.add_argument('--run-all', action='store_true', help = 'Run for all models.')
    parser.add_argument('--use-gru', action='store_true', default=False,  help = 'Use GRU.')
    parser.add_argument('--load-c-model', action='store_true', default=None, help = 'Load c_model in training single-stage.')
    parser.add_argument('--only-evaluate', action='store_true', default=False, help = 'Evaluate model only.')
    parser.add_argument('--for-stage', type= int, default = None, help = 'Select which stage model to run')
    parser.add_argument('--under-prop', type= float, default = 1, help = 'Undersampling proportion')

    parser.add_argument('--train-epochs', type= int, default = None, help = 'Training epochs')
    parser.add_argument('--patience', type= int, default = None, help = 'Patience for training')
    parser.add_argument('--batch-size', type= int, default = None, help = 'Total batch size')
    parser.add_argument('--with-date', type= str, default = None, help = 'Pass date to run job')
    parser.add_argument('--with-label', type= str, default = "", help = 'Pass label to run job')
    parser.add_argument('--disable-gan', action='store_true', default=False, help = 'Disable gan in single-stage.')
    
    args = parser.parse_args() 
    log.set_log_level(args.logger_level)
    
    if args.run_all:
        run_all(args)
    else:
        select_model(args)
  
if __name__ == "__main__":
    main()