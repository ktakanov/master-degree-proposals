echo "single-stage\n";
echo "\nBASELINE"
cat results/baseline/first/report.txt;

echo "\nSINGLE_STAGE"
cat results/single_stage/c_model_report.txt ;

echo "\nSINGLE_STAGE NO GAN"
cat results/single_stage_nogan/c_model_report_nogan.txt ;

echo "\nTWO-STAGE"
cat results/two_stage/first/report.txt

echo "\nsecond-stage\n";

echo "\nBASELINE"
cat results/baseline/second/report.txt;

echo "\nSINGLE_STAGE"
cat results/single_stage/i_model_report.txt ;

echo "\nSINGLE_STAGE NO GAN"
cat results/single_stage_nogan/i_model_report_nogan.txt ;

echo "\nTWO-STAGE"
cat results/two_stage/second/report.txt
