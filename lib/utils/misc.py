import numpy as np
import tensorflow as tf
import numpy as np
import tensorflow.keras.backend as K
from tensorflow.keras.preprocessing.sequence import pad_sequences
import pickle
from numpy.random import choice
from numpy.random import random as rnd
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
import datetime
import os
from lib.config.controls.dataprocess import Construct
from lib.config.controls.paths import Paths
import pandas as pd

c_date = None

def load_scaler_by_feat(feat):
    return pickle.load(open(Paths.scalers_path + 'scaler_' + feat + ".pkl","rb"))

def create_readable_df(inp, save_csv=True):
  cat_cols = Construct.cat_cols 
  num_cols = Construct.num_cols

  cols = cat_cols + num_cols

  i = cols.index(num_cols[0])
  for feat in num_cols:
    scaler = load_scaler_by_feat(feat)
    inp[:,i] =scaler.inverse_transform(inp[:,i])
    i+=1
  
  df = pd.DataFrame(inp,columns=cols)

  return df


def remove_last_path(path):
  if path[len(path)-1] == '/':
    path = path[:-1]

  return path[:path.rfind('/')+1]

def change_last_folder_path(path, new_folder):
  path = remove_last_path(path)
  return path + new_folder + '/'

def get_current_datetime():
  global c_date
  if c_date == None:
    c_date = str(datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S"))
  return c_date

def update_c_date(new_date):
  global c_date
  if new_date != None and new_date != False:  
    c_date = new_date
  return c_date

def smooth_positive_labels(y):
	return y + 0.1 

def smooth_positive_labels_2(y):
	return y - 0.05 + (rnd(y.shape) * 0.01)
  
def smooth_negative_labels(y):
	return y + rnd(y.shape) * 0.1

def noisy_labels(y, p_flip):
	# determine the number of labels to flip
	n_select = int(p_flip * y.shape[0])
	# choose labels to flip
	flip_ix = choice([i for i in range(y.shape[0])], size=n_select)
	# invert the labels in place
	y[flip_ix] = 1 - np.round(y[flip_ix])
	return y

def save_total_categories(total):
  f = open("assets/misc/total_categories.txt","w+")
  f.write(str(total))
  f.close()


def load_total_categories():
  f = open("assets/misc/total_categories.txt","r")
  return int(f.readline().split(" ")[0].rstrip())

def flat_list(l):
  return np.array([item for sublist in l for item in sublist])

def access_column(arr,col):
  return arr[:,:,col]


# Compatible with tensorflow backend
def binary_focal_loss(gamma=2., alpha=.25):
  def binary_focal_loss_fixed(y_true, y_pred):
      pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
      pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))

      epsilon = K.epsilon()
      # clip to prevent NaN's and Inf's
      pt_1 = K.clip(pt_1, epsilon, 1. - epsilon)
      pt_0 = K.clip(pt_0, epsilon, 1. - epsilon)

      return -K.sum(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1)) \
              -K.sum((1 - alpha) * K.pow(pt_0, gamma) * K.log(1. - pt_0))

  return binary_focal_loss_fixed

def focal_loss(gamma=2., alpha=.25):
    def focal_loss_fixed(y_true, y_pred):
        pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
        pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
        return -K.mean(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1)) - K.mean((1 - alpha) * K.pow(pt_0, gamma) * K.log(1. - pt_0))
    return focal_loss_fixed

def pad_arr(arr,i,N):
    diff_ = N - arr.shape[0] 
    if diff_ == 0:
        return arr
    if diff_ < 0:
        return arr[-N:]
    return np.pad(arr, [(diff_,0), (0,0)],'constant', constant_values=-10.0 )
#     return pad_sequences(arr, maxlen=size, padding='pre')

def filter_buyers_test(X,y_target,y_domain,test_sessions,len_inp):
  X_buyer = np.array(X[np.where(y_domain == 1.0)[0]])
  y_buyer = y_target[np.where(y_domain == 1.0)[0]]
  y_buyer_sessions = test_sessions[np.where(y_domain == 1.0)[0]]
  len_inp = len_inp[np.where(y_domain == 1.0)[0]]
  return X_buyer,y_buyer,y_buyer_sessions,len_inp

def filter_buyers(X,y_target,y_domain, len_inp):
  X_buyer = np.array(X[np.where(y_domain == 1.0)[0]])
  y_buyer = y_target[np.where(y_domain == 1.0)[0]]
  y_buyer_session = y_domain[np.where(y_domain == 1.0)[0]]
  len_inp = len_inp[np.where(y_domain == 1.0)[0]]
  return X_buyer,y_buyer_session,y_buyer,len_inp

def filter_nonbuyers(X,y_target,y_domain, len_inp):
  X_nonbuyer = np.array(X[np.where(y_domain == 0)[0]])
  y_nonbuyer = y_target[np.where(y_domain == 0)[0]]
  y_nonbuyer_session = y_domain[np.where(y_domain == 0)[0]]
  len_inp = len_inp[np.where(y_domain == 0)[0]]
  
  return X_nonbuyer, y_nonbuyer_session, y_nonbuyer, len_inp

def get_roc_curve(y_pred, y_test, base_path, final_name='roc_curve.png', plot=True):
    # Compute fpr, tpr, thresholds and roc auc
    y_test = np.array(y_test)
    y_predicted = np.array(y_pred)
    fpr, tpr, thresholds = roc_curve(y_test, y_predicted)

    auc_purchase = roc_auc_score(y_test, y_predicted)
  
    # Plot ROC curve
    
    if plot:

      plt.figure(figsize=(10,10))
      plt.margins(1000.2,1.2)
      plt.plot(fpr, tpr, label='ROC curve (area = %0.3f)' % auc_purchase,color="blue", linewidth=2.5, linestyle="-")
      plt.plot([0, 1], [0, 1], 'k--')  # random predictions curve

      plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)
      plt.xlim([0.0, 1.0])
      plt.ylim([0.0, 1.0])
      plt.xlabel('False Positive Rate or (1 - Specifity)')
      plt.ylabel('True Positive Rate or (Sensitivity)')
      plt.title('Receiver Operating Characteristic')
      plt.legend(loc="lower right")
      
      plot_margin = 0.05

      x0, x1, y0, y1 = plt.axis()
      plt.axis((x0 - plot_margin,
              x1 + plot_margin,
              y0 - plot_margin,
              y1 + plot_margin))

      plt.savefig(base_path + final_name)

    return auc_purchase

    # plt.plot(purchase_fpr, purchase_tpr,"rv",linewidth=4., marker="o",  markersize=10)


def apply_pad(mat, N):
    res = [0 for i in range(0,mat.shape[0])]
    i = 0
    for sample in mat:
        res[i] = pad_arr(sample,i,N)
        i+=1

    return np.array(res)
  
def pad_array_bef(sample, N):
  s_sample = len(sample)
  return np.pad(sample, (N - s_sample,0), 'constant')

def apply_pad_arr(mat, N):
  res = [0 for i in range(0,mat.shape[0])]
  i = 0
  for sample in mat:
    s_sample = len(sample)
    res[i] = np.pad(sample, (N - s_sample,0), 'constant')
    i+=1
  return np.array(res)

def recall_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

def precision_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

# focal loss with multi label
def m_focal_loss(classes_num, gamma=2., alpha=.25, e=0.1):
    # classes_num contains sample number of each classes
    def focal_loss_fixed(target_tensor, prediction_tensor):

        zeros = array_ops.zeros_like(prediction_tensor, dtype=prediction_tensor.dtype)
        one_minus_p = array_ops.where(tf.greater(target_tensor,zeros), target_tensor - prediction_tensor, zeros)
        FT = -1 * (one_minus_p ** gamma) * tf.math.log(tf.clip_by_value(prediction_tensor, 1e-8, 1.0))

        #2# get balanced weight alpha
        classes_weight = array_ops.zeros_like(prediction_tensor, dtype=prediction_tensor.dtype)

        total_num = float(sum(classes_num))
        classes_w_t1 = [ total_num / ff for ff in classes_num ]
        sum_ = sum(classes_w_t1)
        classes_w_t2 = [ ff/sum_ for ff in classes_w_t1 ]   #scale
        classes_w_tensor = tf.convert_to_tensor(classes_w_t2, dtype=prediction_tensor.dtype)
        classes_weight += classes_w_tensor

        alpha = array_ops.where(tf.greater(target_tensor, zeros), classes_weight, zeros)

        #3# get balanced focal loss
        balanced_fl = alpha * FT
        balanced_fl = tf.reduce_mean(balanced_fl)

        #4# add other op to prevent overfit
        # reference : https://spaces.ac.cn/archives/4493
        nb_classes = len(classes_num)
        fianal_loss = (1-e) * balanced_fl + e * K.categorical_crossentropy(K.ones_like(prediction_tensor)/nb_classes, prediction_tensor)

        return fianal_loss
    return focal_loss_fixed