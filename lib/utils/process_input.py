import numpy as np
from scipy import sparse
from .misc import load_total_categories
from sklearn.preprocessing import OneHotEncoder

MASK_VALUE = 0

oh = OneHotEncoder(sparse=False)

def get_one_hot_encoders():
    return fit_one_hots(get_gen_outs())

def get_gen_outs(use_time_as_numerical=True):
    total_categories = load_total_categories()
    if use_time_as_numerical:
        gen_outs = [13, total_categories, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    else:
        gen_outs = [13, total_categories, 31, 4, 12, 7, 1, 1, 1, 1, 1] # TODO refactor this hardcoded]
    
    return gen_outs


def fit_one_hots(gen_out, use_sparse=False):
    encoders = {}
    for col in range(0,len(gen_out)):
        one_hot_encoder = OneHotEncoder(sparse=use_sparse, handle_unknown='ignore')
        for_fit = np.array([float(k) for k in range(0,gen_out[col]+1)]).reshape(-1,1)
        one_hot_encoder.fit(for_fit)
        encoders[col] = one_hot_encoder
    return encoders

def encoder_by_col(X, N, col, encoders):
    ccol = X[:,col].reshape(-1,1)
    one_hot_encoder = encoders[col]
    res = one_hot_encoder.transform(ccol)
    return res.reshape(len(ccol),res.shape[-1])

def apply_encoder_in_batch(X, N, cat_cols, num_cols, encoders, use_sparse=False):
    X_num_cols = X[:,num_cols].copy()
    X_num_cols = sparse.coo_matrix(X_num_cols)
    X_cat_cols = None 
    for col in range(0,len(cat_cols)):
        encoded = encoder_by_col(X, N, col, encoders)
        if X_cat_cols is not None:
            X_cat_cols = np.hstack((X_cat_cols, encoded)) if use_sparse == False else sparse.hstack((X_cat_cols, encoded))
        else:
            X_cat_cols = encoded
    res = ((X_cat_cols, X_num_cols)) 
    return np.hstack(res) if use_sparse == False else sparse.hstack(res)

def apply_encoder(X,N,cat_cols,num_cols,encoders):
    res = []
    for batch in X:
        res.append(apply_encoder_in_batch(batch, N, cat_cols, num_cols, encoders))
    return res 

def pad_arr(arr,i,N):
    diff_ = N - arr.shape[0] 
    if diff_ == 0:
        return arr
    if diff_ < 0:
        return arr[-N:]
    return np.pad(arr, [(diff_,0), (0,0)],'constant', constant_values=MASK_VALUE)

def apply_pad(mat, N):
    res = [0 for _ in range(0,len(mat))]
    i = 0
    for sample in mat:
        res[i] = pad_arr(sample,i,N)
        i+=1
    return np.array(res).astype(np.float32)
  
def pad_array_bef(sample, N):
  s_sample = len(sample)
  return np.pad(sample, (N - s_sample,0), 'constant',constant_values=MASK_VALUE)

def apply_pad_arr(mat, N):
  res = [0 for i in range(0,mat.shape[0])]
  i = 0
  for sample in mat:
    s_sample = len(sample)
    res[i] = np.pad(sample, (N - s_sample,0), 'constant', constant_values=MASK_VALUE)
    i+=1
  return np.array(res)