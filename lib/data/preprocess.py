from lib.data.features.price_dist_avg import apply_dist_price_avg
import numpy as np
import sklearn.preprocessing as p
from sklearn.preprocessing import RobustScaler
from lib.config.controls.paths import Paths
from lib.config.controls.twostage import TWOSTGTRANS
from lib.config.controls.dataprocess import *
from lib.config.controls.dataprocess import Preproces
from lib.config.controls.singlestgan import SINGLESTGAN
import pandas as pd
import pickle
from numpy.random import randint

import lib.config.logger as log

logger = log.get_logger()



def select_feat_of_interest(train_df, test_df):
    feats = Preproces.feats_all
    train_df = train_df[feats]
    test_df = test_df[feats]
    return train_df, test_df

def df_undersample(df, proportion=10):
    session_ids = df.session_id.values
    buyers_df = df[df.label == True]
    buyers = buyers_df.session_id.drop_duplicates().values
    
    nonbuyers_df = df[df.label == False]
    nonbuyers = nonbuyers_df.session_id.drop_duplicates().values
        
    ix = randint(0, len(nonbuyers), len(buyers) * proportion)
    
    nonbuyers_df = nonbuyers_df[nonbuyers_df.session_id.isin(nonbuyers[ix])]
    
    return pd.concat((buyers_df, nonbuyers_df))
    

def apply_undersampling(train_df, prop=5):
    train_df = df_undersample(train_df,prop)

    return train_df

def scale_feature_in_input_data(train_df,  test_df, feat, use_std_scaler=True):
    scaler = p.StandardScaler().fit(train_df[[feat]]) if use_std_scaler else p.RobustScaler().fit(train_df[[feat]])
    train_df[feat] = scaler.transform(train_df[[feat]])
    test_df[feat] = scaler.transform(test_df[[feat]])

    pickle.dump(scaler,open(Paths.scalers_path + "scaler_"+ feat + ".pkl","wb"))
    return train_df, test_df

def scale_feature_for_all(df, feats, use_std_scaler=True):
    scaler = p.StandardScaler().fit(df[feats]) if use_std_scaler else p.RobustScaler().fit(df[[feats]])
    df[feats] = scaler.transform(df[feats])
    
    pickle.dump(scaler,open(Paths.scalers_path + "scaler_"+ str(list(feats)) + ".pkl","wb"))
    return df

def apply_scale_for_input_data(train_df, test_df):
    to_scale_features = list(Preproces.to_scale_features)
    logger.info("To scale features: " + str(to_scale_features))
    # for feat in to_scale_features:
        # train_df, test_df = scale_feature_in_input_data(train_df, test_df, feat)
        
    train_df = scale_feature_for_all(train_df, to_scale_features)
    test_df = scale_feature_for_all(test_df, to_scale_features)        
   
    return train_df, test_df

def get_large_s_ids(df):
  max_seq = Preproces.max_seq_trunc
  element_group_sizes = df.session_id.groupby(df.session_id).size()
  large_groups = element_group_sizes[element_group_sizes > max_seq]
  large_s_ids = large_groups.index.unique()

  return large_s_ids

def truncate_seq(x):
  max_seq = Preproces.max_seq_trunc
  return x[-max_seq:]

def apply_truncate_in_large(df, large_s_ids): #TODO: verify if it's ordered.
  filtered =  df[df.session_id.isin(large_s_ids)].groupby('session_id').apply(truncate_seq).reset_index(drop=True)
  idxNames = df[df.session_id.isin(large_s_ids)].index
  df = df.drop(idxNames) # remove large sids
  df = pd.concat((df, filtered))
  return df 

def apply_truncate_for_df(df):
    larges_sids = get_large_s_ids(df)
    df = apply_truncate_in_large(df, larges_sids)
    return df

def apply_truncate_for_input_data(train_df, test_df):
    train_df = apply_truncate_for_df(train_df) 
    test_df = apply_truncate_for_df(test_df) 
    return train_df, test_df

def save_prepared_for_single_stg(train_df, test_df):
    train_df.to_csv(Paths.processed_path + "train_df_prepared_single.csv", index=False)
    test_df.to_csv(Paths.processed_path + "test_df_prepared_single.csv", index=False)

def load_prepared_for_single_Stg():
    train_df = pd.read_csv(Paths.processed_path + "train_df_prepared_single.csv")
    test_df = pd.read_csv(Paths.processed_path + "test_df_prepared_single.csv")
    
    # train_df['timestamp'] = pd.to_datetime(train_df.timestamp)
    # test_df['timestamp'] = pd.to_datetime(test_df.timestamp)
    return train_df, test_df


def save_prepared_for_baseline(train_df, test_df):
    train_df.to_csv(Paths.processed_path + "train_df_prepared_baseline.csv", index=False)
    test_df.to_csv(Paths.processed_path + "test_df_prepared_baseline.csv", index=False)

def load_prepared_for_baseline():
    train_df = pd.read_csv(Paths.processed_path + "train_df_prepared_baseline.csv")
    test_df = pd.read_csv(Paths.processed_path + "test_df_prepared_baseline.csv")
    
    train_df['timestamp'] = pd.to_datetime(train_df.timestamp)

    test_df['timestamp'] = pd.to_datetime(test_df.timestamp)
    return train_df, test_df

def save_prepared(train_df, test_df):
    train_df.to_csv(Paths.processed_path + "train_df_prepared.csv", index=False)
    test_df.to_csv(Paths.processed_path + "test_df_prepared.csv", index=False)

def load_prepared():
    train_df = pd.read_csv(Paths.processed_path + "train_df_prepared.csv")

    test_df = pd.read_csv(Paths.processed_path + "test_df_prepared.csv")
    
    # train_df['timestamp'] = pd.to_datetime(train_df.timestamp)
    # test_df['timestamp'] = pd.to_datetime(test_df.timestamp)
    return train_df, test_df

def select_feats_for_twosttrans(train_df, test_df):
    feats = TWOSTGTRANS.data_features
    
    train_df = train_df[feats]
    test_df = test_df[feats]
    return train_df,  test_df

def select_feats_for_singlesttrans(train_df, test_df):
    feats = SINGLESTGAN.data_features
    
    train_df = train_df[feats]
    test_df = test_df[feats]
    return train_df, test_df
    