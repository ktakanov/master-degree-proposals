from lib.config.controls.baseline import Baseline
import numpy as np
def apply_item_probs_for_input_data(train_df, test_df):
    item_id_probs = Baseline.get_item_probs() 
    
    train_df['item_id_prob'] = train_df['item_id'].map(item_id_probs).astype(np.float32)
    test_df['item_id_prob'] = test_df['item_id'].map(item_id_probs).astype(np.float32)
    
    return train_df, test_df