import gc
import operator, itertools
from decimal import *
import numpy as np
from collections import defaultdict
import pandas as pd
import swifter
from tqdm import tqdm
import pandas as pd

# tqdm.pandas(desc="") last tqdm
tqdm.pandas()

# train_df_c['item_std'] = train_df_c['item_id_month_day'].apply(item_price)

def apply_diff(row, month_avg_price, estimated_price):
  month = str(row['month'])
  it_d = str(row['item_id'])
  day = str(row['day'])
  est_price = estimated_price.get(it_d+month+day,None)

  if est_price == None: return np.nan

  avg_price = month_avg_price.get(it_d,None)
  if avg_price == None: return np.nan
  return  est_price - avg_price

def create_avg_price_data_structures(db):
    db['timestamp'] = pd.to_datetime(db.timestamp)
    sid_price = list(zip(db.item_id,db.timestamp.dt.month,db.timestamp.dt.day,db.timestamp,db.price))
 
    d = defaultdict(set)
    d2 = defaultdict(set)

    for item_id,month,day,timestamp,p in sid_price:
        d[str(item_id)].add(p)
        d2[str(item_id) + str(month) + str(day)].add(p)

    for key,value in d.items():
        d[key] = np.mean(list(d[key]))

    for key,value in d2.items():
        d2[key] = np.mean(list(d2[key]))
        
    month_avg_price = d
    estimated_price = d2

    return month_avg_price, estimated_price

def apply_dist_price_avg(df, month_avg_price, estimated_price):
    INVALID = 0
    import gc
    df['item_id_month'] = df.item_id.astype(str) + df.month.astype(str) + df.day.astype(str)
    df['price_dist_avg'] = df[['item_id','month','day']]\
        .progress_apply(lambda x: apply_diff(x, month_avg_price, estimated_price),axis=1)\
        .astype(np.float32)
        
    df.loc[df.price_dist_avg.isnull(), 'price_dist_avg'] = INVALID
    df = df.drop(['item_id_month'],axis=1)
    
    return df

def apply_dist_price_avg_for_input_data(db, train_df,  test_df):
   db = db[db.session_id.isin(train_df.session_id)]
   db = db[db.price > 0]

   month_avg_price, estimated_price = create_avg_price_data_structures(db)
   train_df = apply_dist_price_avg(train_df, month_avg_price, estimated_price)
   test_df = apply_dist_price_avg(test_df, month_avg_price, estimated_price) 

   
   return train_df, test_df