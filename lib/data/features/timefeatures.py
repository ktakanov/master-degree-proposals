import pandas as pd
from tqdm import tqdm
import pandas as pd
import numpy_indexed as npi
from multiprocessing import Pool, cpu_count
import numpy as np
from lib.utils import *
import dask.dataframe as dd
import multiprocessing 
import os

import lib.config.logger as log
from dask.distributed import LocalCluster, Client

logger = log.get_logger()

# tqdm.pandas(desc="") last tqdm
tqdm.pandas()


def diff_group(x):
    return x.diff().dt.seconds

def applyParallel(df):
    dfGrouped = df.groupby(['session_id']).timestamp
    total_cpu = cpu_count()
    with Pool(cpu_count()) as p:
        print("Pooling")
        ret_list = p.map(diff_group, [group for name, group in dfGrouped])
        print("terminate")
    return pd.concat(ret_list)

def apply_diff_by_click(sample):
    sample['time_by_item'] = sample[sample.item_click > 1].groupby(['session_id', 'item_id']).timestamp\
                .apply(lambda x: x.diff()).dt.seconds
    sample = sample.fillna(0.0)
    return sample

def apply_dwelltime(df):
  df['timestamp'] = pd.to_datetime(df['timestamp'].astype(str))
  clust = LocalCluster()
  clt = Client(clust, set_as_default=True) 
 
  ddf = dd.from_pandas(df, npartitions=16)
  
  df['dwelltime'] = ddf.groupby('session_id').timestamp\
              .apply(lambda x: x.diff().dt.seconds,meta='int')\
                .compute(num_workers=os.cpu_count(), scheduler='processes').fillna(0.0).astype(np.int32)
              
  return df

def apply_dwelltime_arr(df):

  df_cols = list(df)

  def create_time_diff(group):
    times = group[:,df_cols.index("timestamp")]
    time_diff = [0] + [(t - s).seconds for s, t in zip(times, times[1:])]
    return time_diff 

  arr_df = df.values
  grouped_arr = np.array(npi.group_by(arr_df[:, 0]).split(arr_df[:,0:]))
  total_p = grouped_arr.shape[0]
  times_grouped = []
    
  for k in range(0, total_p):
    if k % 1000 == 0:
      logger.debug("Current k: " + str(k/total_p))
    times_grouped.append(create_time_diff(grouped_arr[k]))

  flattened = flat_list(times_grouped)
  df['dwelltime'] = flattened

  return df

def apply_dwelltime_for_input_data(train_df, test_df):
  train_df = apply_dwelltime(train_df)
  test_df = apply_dwelltime(test_df)
  return train_df, test_df
  

def apply_basic_timefeatures(df):
  df['timestamp'] = pd.to_datetime(df['timestamp'].astype(str))

  df['weekday'] = (df.timestamp.dt.weekday ).astype(np.int8)+1
  # df['date'] = df.timestamp.dt.date
  df['day'] = (df.timestamp.dt.day ).astype(np.int8)+1
  df['hour'] = (df.timestamp.dt.hour ).astype(np.int8)+1
  df['month'] = (df.timestamp.dt.month ).astype(np.int8)+1# nott using for a while.
  df['week'] = (df.timestamp.dt.week ).astype(np.int8)+1

  return df

def apply_basic_timefeatures_for_input_data(train_df, test_df):
  train_df = apply_basic_timefeatures(train_df)
  test_df = apply_basic_timefeatures(test_df)
  return train_df,  test_df