import operator, itertools
from decimal import *
import numpy as np
from collections import defaultdict

def build_item_qty_rank(db, train_df):
    db = db.copy()
    db = db[db.session_id.isin(train_df.session_id)]
    
    db[db.quantity == 0] = 1
    
    sid_price = list(zip(db.item_id,db.quantity))
    d = dict()
    
    for item_id,qt in sid_price:
        if item_id not in d:
            d[item_id] = 0
        d[item_id] = d[item_id] + qt # TODO: try without qt, just qt = 1 
    item_rank = d
    return item_rank

def build_item_clicks_rank(db, train_df):
    
    item_rank = train_df.item_id.value_counts().to_dict() 
    
    return item_rank

def apply_item_qty_rank_for_input_data(db, train_df,  test_df):
    item_rank = build_item_qty_rank(db, train_df)
    train_df['item_qty_rank'] = train_df['item_id'].map(item_rank).astype(np.float32)
    test_df['item_qty_rank'] = test_df['item_id'].map(item_rank).astype(np.float32)
    
    return train_df, test_df


def apply_item_clicks_rank_for_input_data(db, train_df,  test_df):
    item_rank = build_item_clicks_rank(db, train_df)
    train_df['item_clicks_rank'] = train_df['item_id'].map(item_rank).astype(np.float32)
    test_df['item_clicks_rank'] = test_df['item_id'].map(item_rank).astype(np.float32)
    
    return train_df, test_df