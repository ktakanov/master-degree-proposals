from lib.utils import save_total_categories
import pandas as pd
import numpy as np
from random import sample
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from lib.config.controls.paths import Paths
from lib.config.controls.dataprocess import Construct
import lib.input.baseline as bs

c_cols = ['session_id','timestamp','item_id','category']
b_cols = ['session_id','timestamp','item_id','price','quantity']

raw_path = Paths.raw_path
dc_path = raw_path + 'yoochoose-clicks'
db_path = raw_path + 'yoochoose-buys'
dt_path = raw_path + 'yoochoose-test'
import lib.config.logger as log

logger = log.get_logger()



def apply_path_sample(path, use_samples):
    with_samples = path + '-sample' if use_samples else path
    with_dat = with_samples + '.dat'   
    return with_dat

def read_data_by_path(path, cols, use_samples=False):
    dtypes = {
        'session_id': str, 
        'timestamp':str,
        'item_id': str, 
        'category': str}  
    
    df = pd.read_csv(apply_path_sample(path, use_samples) ,sep=',',header=None,names=cols)
    return df

def read_recsys_data(use_samples=True, read_dc = True, read_db=True, read_dt=True):
    dc = read_data_by_path(dc_path, c_cols, use_samples) if read_dc else None
    db = read_data_by_path(db_path, b_cols, use_samples=False) if read_db else None
    dt = read_data_by_path(dt_path, c_cols, use_samples=False) if read_dt else None
    
    return dc, db, dt

def sample_data(df, part = 0.1):
    sids = df.session_id.unique()
    sample_ids = sample(list(sids), int(len(sids) * part))
    return df[df.session_id.isin(sample_ids)]

def save_sample_data(dc, db, dt):
    dc.to_csv(apply_path_sample(dc_path, True), header=False, index=False)
    db.to_csv(apply_path_sample(db_path, True), header=False, index=False)
    dt.to_csv(apply_path_sample(dt_path, True), header=False, index=False)
    
def create_split_sids(dc):
    valid_purchase_sota = pd.read_csv("low_assets/valid_purchase_sota.csv").session_id.drop_duplicates()
    test_df = dc[dc.session_id.isin(valid_purchase_sota)]
    train_df = dc[~dc.session_id.isin(valid_purchase_sota)]
    return train_df, test_df

def create_labels_for_input_data(db, train_df, test_df): #TODO: test labels with group by and if len(buys) > 1
    train_df = train_df.copy()
    test_df = test_df.copy() # avoid boring warning

    train_df['label'] = train_df.session_id.isin(db.session_id)
    test_df['label'] = test_df.session_id.isin(db.session_id)  
    return train_df, test_df

def select_feat_of_interest(train_df, test_df):
    feats = Construct.feats_of_inst
    train_df = train_df[feats]
    test_df = test_df[feats]
    return train_df, test_df

def save_constructed(train_df,  test_df, part_string='00'):
    train_df.to_csv(Paths.processed_path + "train_df_constructed" + part_string + ".csv", index=False)
    test_df.to_csv(Paths.processed_path + "test_df_constructed" + part_string + ".csv", index=False)

def load_constructed(part_string="00"):
    train_df = pd.read_csv(Paths.processed_path + "train_df_constructed" + part_string + ".csv")
    test_df = pd.read_csv(Paths.processed_path + "test_df_constructed" + part_string + ".csv")
    return train_df,  test_df

def encode_category(train_df, test_df):
    cat_encoder = LabelEncoder()
    cat_encoder.fit(list(train_df.category.values))
    le_dict = dict(zip(cat_encoder.classes_, cat_encoder.transform(cat_encoder.classes_)))

    INVALID = max(le_dict.values()) + 1
    save_total_categories(INVALID+1)
    
    logger.info("Invalid category: " + str(INVALID))

    train_df['category'] = train_df.category.apply(lambda x: le_dict.get(x, INVALID)).astype(np.int32)+1
    test_df['category'] = test_df.category.apply(lambda x: le_dict.get(x, INVALID)).astype(np.int32)+1
    
    return train_df, test_df

def categorize_hour(df):
    labels = [0,1,2,3]
    df['hour'] = pd.cut(df['hour'], bins=[0,6,12,18,24], include_lowest=True, labels=labels).astype(np.int8)
    return df

def categorize_hour_for_input_data(train_df, test_df):
    train_df = categorize_hour(train_df)
    test_df = categorize_hour(test_df)
    
    return train_df, test_df