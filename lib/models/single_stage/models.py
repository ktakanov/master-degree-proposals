from lib.models.common import *
from lib.config.controls.singlestgan import SINGLESTGAN
from lib.models.classes.transformer import *
import numpy as np
from tensorflow.keras.layers import *
from tensorflow.keras.callbacks import *
from tensorflow.keras import Model
from tensorflow.keras.losses import *
from lib.third_party.keras_crf import CRF
from tensorflow.keras.optimizers import *
from tensorflow.keras.metrics import AUC
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.initializers import RandomNormal
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import *
from sklearn.metrics import precision_score
from collections import Counter
from numpy.random import randn, seed
import tensorflow as tf

from math import ceil
import pandas as pd


import lib.config.logger as log
logger = log.get_logger()
mask = True


def build_sequencial_encoding(num_heads, denses, emb, use_gru=True, name='ip', subdense=15):
    if not use_gru: 
        x = emb 
        for k in range(0,len(denses)):
            dense = denses[k]
            n = name + '_trans_' + str(k)
            x = TransformerBlock(x.shape[-1], num_heads, dense, rate = 0.1, name = n)(x)
            x = BatchNormalization(name=name+'_trans_bn_' + str(k))(x)
    else:
        x = Bidirectional(GRU(denses[0],return_sequences=True),name='seq_gru')(emb)
    return x 

def build_final_base_tri(denses, out_model, name='ip',flat=True,dropout=0.3):
  x = out_model
  for k in range(0,len(denses)):
    dense = denses[k]
    x = Dense(dense,name=name+'_dn_base_' + str(k))(x)
    x = LeakyReLU(alpha=0.2)(x)
    x = BatchNormalization(name=name+'_bn_' + str(k))(x)

  if flat:
    last_layer = x 
    x = Flatten()(x)
    return last_layer, x
  else:
    return x

def build_final_base(dense, out_model, deep = 3, name='ip'):

  for k in range(0,deep):
    x = Dense(dense,activation='relu',name=name+'_dn_base_' + str(k))(out_model)
    x = Dropout(0.2)(x)
    if k != 0:
      out_model = x

  x = Flatten()(x)

  return x

def custom_activation(output):
	logexpsum = K.sum(K.exp(output), axis=-1, keepdims=True)
	result = logexpsum / (logexpsum + 1.0)
	return result


def define_c_model(embedding, X_in, use_gru=False, freeze_layers=False):
    T = 30
    common_base = build_sequencial_encoding(4, [T,T,T,T],embedding, use_gru=use_gru, subdense=15,name='c')
    # common_base = Dense(20,name='seq_dense_0', activation = LeakyReLU())(common_base) 
    common_base = Dense(20,name='seq_dense', activation = LeakyReLU())(common_base) 

    DENSE=20

    c_base =  common_base 
    c_in = Flatten()(c_base) 
    
    c_in = build_final_base_tri([DENSE,DENSE, DENSE], c_in, name='class',flat=False, dropout=0.2)
    
    class_layer = Dense(1,name='c_output')(c_in)
    class_layer = Activation('sigmoid')(class_layer)
    c_model = Model(X_in, class_layer)

    loss_f = 'binary_crossentropy'
    # loss_f = focal_loss(alpha=0.80)
    opt = Adam(lr=0.0002, beta_1=0.5)
    c_model.compile(optimizer = opt,loss = loss_f, metrics = ['accuracy', AUC(name='auc')])
    logger.info("c_model:\n" + str(c_model.summary()))
 
    return c_model, common_base
    

def define_discriminator(c_model, X_in, use_gru):
    # item_predictor
    common_base = c_model.layers
    DENSE=30

    base_model_name = 'seq_dense' if not use_gru else 'seq_dense'

    i_base = c_model.get_layer(base_model_name).output
    i_weights = np.array([DENSE,DENSE, DENSE])
    i_last_layer = build_final_base_tri(i_weights, i_base, name='item',flat=False, dropout=0.1)
    i_last_layer = Dense(2)(i_last_layer)

    crf = CRF(2,sparse_target=False)
    item_pred_layer = crf(i_last_layer)
    loss_f = crf.loss
    metrics = crf.accuracy

    i_model = Model(X_in, item_pred_layer)

    opt = Adam(lr=0.0002, beta_1=0.5)

    i_model.compile(optimizer = opt,loss = loss_f, metrics = metrics)
    logger.info("i_model: \n" + str(i_model.summary()))
    ## classifier 
    
    # c_base = c_model.get_layer('class_bn_2').output 
    # c_base = c_model.get_layer('c_output').output 
    # i_base = i_model.get_layer('item_bn_2').output
    c_base = c_model.get_layer('seq_dense').output 
    # c_base = Flatten()(c_base)
    

    d_out_layer = c_base
    d_out_layer = Lambda(custom_activation,name='custom_activation')(d_out_layer)
    # d_out_layer = Dense(1, 'sigmoid')(d_out_layer)
    
    loss_f = 'binary_crossentropy'

    d_model = Model(X_in, d_out_layer)
    opt = Adam(lr=0.0002, beta_1=0.5)
    d_model.compile(optimizer = opt, loss = loss_f, metrics = ['accuracy'])
    logger.info("d_model:\n" + str(d_model.summary()))
 
    return d_model,i_model

def define_generator_output(gen_outs):
    c1 = Dense(gen_outs[0]+1, activation='softmax')
    c2 = Dense(gen_outs[1]+1, activation='softmax')
    c3 = Dense(gen_outs[2]+1, activation='softmax')#day
    c4 = Dense(gen_outs[3]+1, activation='softmax')# hour
    c5 = Dense(gen_outs[4]+1, activation='softmax')#month
    c6 = Dense(gen_outs[5]+1, activation='softmax')#weekday
    c7 = Dense(1, activation='linear')#dwelltime
    c8 = Dense(1, activation='linear')#item_rank
    c9 = Dense(1, activation='linear')#item_price_std
    c10 = Dense(1, activation='linear') #item_id_prob

    return c1,c2,c3,c4,c5,c6,c7,c8,c9,c10

# define the standalone generator model
def define_generator(latent_dim=250):
    def get_class_label(out):
        return K.cast(K.expand_dims(K.argmax(out, axis = -1),axis=-1), 'float32')
    
    def apply_time_distributed(c, x, act=False):
        x = TimeDistributed(c)(x)
        if act:
            x = Activation(Lambda(lambda x: get_class_label(x)))(x) 
        return x

    # image generator input
    N = SINGLESTGAN.model['N']
    F = SINGLESTGAN.model['F'] 

    init = RandomNormal(stddev=0.2)

    input_n =  N
    inp_n = input_n
    inp_f = F
    inp_filt = 1

    in_lat = Input(shape=(latent_dim,))
    gen = Dense(inp_n * inp_f * inp_filt, activation=ReLU(), kernel_initializer=init)(in_lat)
    gen = Reshape((inp_n,inp_f))(gen)

    total_categories = load_total_categories()
    gen_outs = [input_n, total_categories, 31, 4, 12, 7, 1, 1, 1, 1] # TODO refactor this hardcoded

    x = Dense(2)(gen)
    x = LeakyReLU(alpha=0.2)(x)
    x = Dense(4)(gen)
    x = LeakyReLU(alpha=0.2)(x)
    # x = Reshape((4,10))
    # x = Dense(16, activation=LeakyReLU(alpha=0.2), input_shape=(F,1))(x)
    # x = Dense(32, activation=LeakyReLU(alpha=0.2), input_shape=(F,1))(x)
    # x = Conv1D(8, kernel_size=2)(x)
    # x = Conv2DTranspose(inp_filt, kernel_size=(2,2), strides=(2,2))(gen)
    # x = LeakyReLU(alpha=0.2)(x)
    # x = Conv2DTranspose(inp_filt, kernel_size=(2,2), strides=(2,2))(x)
    # x = LeakyReLU(alpha=0.2)(x)
    # x = Conv2DTranspose(inp_filt, kernel_size=(2,2), strides=(2,2))(x)
    # x = LeakyReLU(alpha=0.2)(x)
    # x = Conv2D(1,kernel_size=(2,2),strides=(1,2))(x)
    # x = Cropping2D(cropping=((10, 0), (2, 0)))(x)
    # inp_x = Reshape((N,F))(x)

    x = Bidirectional(GRU(8, return_sequences=True))(x)
    x = Bidirectional(GRU(16, return_sequences=True))(x)
    x = Bidirectional(GRU(32, return_sequences=True))(x)

    c1,c2,c3,c4,c5,c6,c7,c8,c9,c10 = define_generator_output(gen_outs)

    x1 = apply_time_distributed(c1, x, True)
    x2 = apply_time_distributed(c2, x, True)
    x3 = apply_time_distributed(c3, x, True)
    x4 = apply_time_distributed(c4, x, True)
    x5 = apply_time_distributed(c5, x, True)
    x6 = apply_time_distributed(c6, x, True)
    x7 = apply_time_distributed(c7, x)
    x8 = apply_time_distributed(c8, x)
    x9 = apply_time_distributed(c9, x)
    x10 = apply_time_distributed(c10, x)

    # x = define_limits((x1,x2,x3,x4,x5,x6,x7,x8,x9,x10), params)
    x = Concatenate(axis=-1)([x1,x2,x3,x4,x5,x6,x7,x8,x9,x10])
    if input_n < N:
      x = ZeroPadding1D(padding=((N - input_n),0))(x)

    
    model = Model(in_lat, x)
    return model

# define the combined generator and discriminator model, for updating the generator

def define_gan(g_model, d_model):
    d_model.trainable = False
    gan_output = d_model(g_model.output)
    model = Model(g_model.input, gan_output)
    opt = Adam(lr=0.0002, beta_1=0.5)

    model.compile(loss='binary_crossentropy', optimizer=opt)
    return model

def build_single(use_gru=False):
  logger.info("\n********* Building mode (use_gru = " + str(use_gru) + ") *************\n")
  ## build models
  N = SINGLESTGAN.model['N']
  F = SINGLESTGAN.model['F']
  latent_dim = SINGLESTGAN.model['latent_dim']

  X_in = Input(shape=(N,F))
  # create the discriminator models
  emb = build_embedding(X_in, use_gru=use_gru, N=N)
  
  c_model, common_layer = define_c_model(emb, X_in, use_gru)
  d_model, i_model = define_discriminator(c_model, X_in, use_gru) 
  # create the generator
  g_model = define_generator(latent_dim)

  logger.info("g_model:\n" + str(g_model.summary()))
  # create the gan
  gan_model = define_gan(g_model, d_model)

  disc_model = (d_model, i_model, c_model)
  return (g_model, gan_model, disc_model)