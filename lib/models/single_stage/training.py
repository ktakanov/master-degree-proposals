from math import ceil
import pandas as pd

from lib.utils import *
import matplotlib.pyplot as plt
from numpy.random import randint
from numpy import ones, zeros
from sklearn.utils import shuffle
from collections import Counter
from lib.models.two_stage.generic import generate_labels_and_predictions_by_sessions_within_clicks_for_crf
import numpy as np
from numpy import ones
from lib.models.single_stage.evaluation import *
from lib.utils import get_current_datetime, update_c_date, create_readable_df
from lib.models.single_stage.models import *
from lib.config.controls.singlestgan import SINGLESTGAN

import lib.config.logger as log
logger = log.get_logger()

mask = True

def load_input_data(name):
    X, y_class, y_items, SIT, len_inp = SINGLESTGAN.load_input(name)
    return (X, y_class, y_items, SIT, len_inp)

def load_with_filters(name):
    X, y_class, y_items, SIT, len_inp = SINGLESTGAN.load_input(name)
    return (X, y_class, y_items, SIT, len_inp)
      
def load_with_filters_test(name):
    X, y_class, y_items, SIT, len_inp = SINGLESTGAN.load_input(name)
    return (X, y_class, y_items, SIT, len_inp)

def get_batches_from_buyers(dataset, N, batch_size, last_batch):
    X,y_i = dataset['buyers_train']
    
    x_batch, y_i_batch = X[last_batch:last_batch + batch_size], y_i[last_batch: last_batch+ batch_size]
    last_batch = last_batch + batch_size
    
    x_batch = apply_pad(x_batch,N) 
    X_buyers = x_batch
    y_buyers = y_i_batch
    y_buyers = apply_pad_arr(y_buyers,N) 

    if last_batch > len(X):
        last_batch = 0
    return X_buyers, y_buyers, last_batch


def get_batches_from_all(dataset, N, batch_size, last_batch):
    X,y,y_i = dataset['all_train_shuffled']
    
    x_batch, y_batch, y_i_batch = X[last_batch:last_batch + batch_size],y[last_batch:last_batch + batch_size], y_i[last_batch: last_batch+ batch_size]
    last_batch = last_batch + batch_size
    
    x_batch = apply_pad(x_batch,N) 
    idx = np.where(y_batch == 1.0)
    
    X_buyers = x_batch[idx]
    y_buyers = y_i_batch[idx]
   
    y_buyers = apply_pad_arr(y_buyers,N) 
    

    if last_batch > len(X):
        last_batch = 0
    return ((x_batch, y_batch), (X_buyers, y_buyers), last_batch)

def manual_undersampling(X, y_class, y_items, len_inp, n=1):
  seed(4)
  buyers_ix = np.where(y_class==1.0)[0]
  X_buyers = X[buyers_ix]
  y_buyers = y_class[buyers_ix]
  len_inp_buyers = len_inp[buyers_ix]

  y_buyer_items = y_items[np.where(y_class == 1)[0]]

  y_buyers_target = y_buyer_items

  total_b = X_buyers.shape[0]

  nonb_ids = np.where(y_class == 0.0)[0]

  X_nonbuyers = X[nonb_ids]
  y_nonbuyers = y_class[nonb_ids]
  len_inp_nonbuyers = len_inp[nonb_ids]
  
  total_b = int(total_b * n)

  ix = randint(0, X_nonbuyers.shape[0], total_b)

  X_nonbuyers = X_nonbuyers[ix]
  y_nonbuyers = y_nonbuyers[ix]
  len_inp_nonbuyers = len_inp_nonbuyers[ix]
  y_nonbuyers_target = zeros((total_b))

  y_all = np.hstack((y_buyers, y_nonbuyers))
  y_items = np.hstack((y_buyers_target, y_nonbuyers_target))
  len_inp = np.hstack((len_inp_buyers, len_inp_nonbuyers))

  X_stacked = np.hstack((X_buyers, X_nonbuyers))
  X_stacked, y_all, y_items, len_inp = shuffle(X_stacked, y_all, y_items, len_inp)

  X = X_stacked
  y_class = y_all

  del X_stacked, y_all

  return X, y_class, y_items, len_inp

def shuffle_train(dataset):
  X_train, y_train_class, y_train_items = dataset['all_train_shuffled']
  dataset['all_train_shuffled'] =  shuffle(X_train, np.array(y_train_class).astype(np.float32), y_train_items)
  return dataset
   
def shuffle_train_buyers(dataset):
  X_train, y_train_items = dataset['buyers_train']
  dataset['buyers_train'] =  shuffle(X_train, y_train_items)
  return dataset

def construct_dataset(use_undersampling=True):
    dataset = {} 

    ## train
    
    o_train, train_buyers, train_nonbuyers = load_with_filters("train")
    X_train, y_train_class, y_train_items, SIT_train = o_train
    X_train_buyer, y_train_buyer = train_buyers 
    train_buyers = np.array(X_train_buyer), y_train_buyer
   
    logger.info("Total train: " + str(X_train.shape))
    i = 0 

    if use_undersampling:
      X_train, y_train_class, y_train_items = manual_undersampling(X_train, y_train_class, y_train_items, y_train_buyer)

    logger.info("Total train with undersampling: " + str(X_train.shape) + " " + str(Counter(y_train_class)))

    ## test
    o_test, test_buyers, test_nonbuyers = load_with_filters_test("test")
    X_test_buyer, y_test_buyer, y_test_buyer_sessions = test_buyers 
    test_buyers = (np.array(X_test_buyer), y_test_buyer)

    #valid
    o_valid, valid_buyers, valid_nonbuyers = load_with_filters_test("valid")
    X_valid_buyer, y_valid_buyer, y_valid_buyer_sessions = valid_buyers 
    valid_buyers = (np.array(X_valid_buyer), y_valid_buyer)


    dataset['all_train_shuffled'] =  shuffle(X_train, np.array(y_train_class).astype(np.float32), y_train_items)
   
    # construct dataset
    o_train = X_train, y_train_class, y_train_items, SIT_train
    
    dataset['buyers_train'] = (X_train_buyer, y_train_buyer) 
    dataset['nonbuyers_train'] = train_nonbuyers
    dataset['all_train'] = o_train
     
    dataset['buyers_valid'] = (X_valid_buyer, y_valid_buyer) 
    dataset['nonbuyers_valid'] = valid_nonbuyers
    dataset['valid_buyer_sessions'] = y_valid_buyer_sessions
    dataset['all_valid'] = o_valid
    
    dataset['buyers_test'] = (X_test_buyer, y_test_buyer)
    dataset['nonbuyers_test'] = test_nonbuyers
    dataset['test_buyer_sessions'] = y_test_buyer_sessions
    dataset['all_test'] = o_test
    return dataset

  
def get_selected_buyers(X_class,y_class,y_target):
  sel_buyers = []
  for k in range(0,len(X_class)):
    if y_class[k] == 1:
      sel_buyers.append(k)

  return np.array(X_class[sel_buyers]), np.array(y_target[sel_buyers])

def get_samples(dataset, dataName, n_samples):
  X, y  = dataset[dataName]
  ix = randint(0, X.shape[0], n_samples)
  X = np.array(X[ix])
  y = y[ix]
  return X,y

def get_buyers_sample(dataset, n_samples, base_train='train'):
  X,y_targets = get_samples(dataset,'buyers_' + base_train,n_samples)
  y_class = ones((len(X),))
  return (np.array(X), y_class, y_targets)

def get_nonbuyers_sample(dataset, n_samples, base_train='train'):
  X,_ = get_samples(dataset,'nonbuyers_' + base_train,n_samples)
  y = zeros((len(X),))
  return X, y

def get_balanced_samples_and_buyers(dataset, N, n_samples=1000, base_train='train',non_buyer_m = 1):
  # get 
  X_buyers, y_buyers_class, y_buyer_items = get_buyers_sample(dataset, int(n_samples), base_train) # colocar, não repetidos
  X_nonbuyers, y_nonbuyers_class = get_nonbuyers_sample(dataset, int(n_samples*non_buyer_m), base_train)
  X_pad_buyers = apply_pad(X_buyers, N) 
  # stackit 
  X_final = np.vstack((X_pad_buyers, apply_pad(X_nonbuyers, N)))
  y_final_class = np.hstack((y_buyers_class, y_nonbuyers_class))
  # randomize
  X_final, y_final_class = shuffle(X_final, y_final_class)
  return ((X_final, y_final_class),(X_pad_buyers, y_buyer_items))

def get_samples_from_all(dataset, dataName, n_samples):
  X,y,y_t,_ = dataset['all_' + dataName]

  ix = randint(0, X.shape[0], n_samples)
  X = X[ix]
  y = y[ix]
  y_t = y_t[ix]

  return X,y,y_t

def generate_latent_points(latent_dim, n_samples):
  # generate points in the latent space
  z_input = randn(latent_dim * n_samples)
  # reshape into a batch of inputs for the network
  z_input = z_input.reshape(n_samples, latent_dim)
  return z_input
 
def generate_fake_samples(generator, latent_dim, n_samples):
  # generate points in latent space
  z_input = generate_latent_points(latent_dim, n_samples)
  # predict outputs
  X = generator.predict(z_input)
  # X = get_final_inputs(params, X)
  # create class labels
  y = ones((n_samples, 1)) # flipt labels
  return X, y 

def train_gan(gan_model, latent_dim, n_batch):
  X_gan, y_gan = generate_latent_points(latent_dim, n_batch), ones((n_batch, 1))
  # y_gan = smooth_positive_labels(y_gan)
  g_loss = gan_model.train_on_batch(X_gan, y_gan)
  return g_loss

def train_session_classifier(c_model, X_train_class, y_train_class):
  c_loss, c_acc, _ = c_model.train_on_batch(X_train_class, y_train_class)

  return (c_loss, c_acc)

def train_item_predictor(i_model, X_train_class, y_train_class, y_train_targets):
  X_train_buyers, y_train_buyers = get_selected_buyers(X_train_class, y_train_class, y_train_targets)
  i_loss, i_acc = i_model.train_on_batch(np.array(X_train_buyers).astype(np.float32), y_train_buyers)
  return i_loss, i_acc

def train_item_from_buyers(i_model, X_train_buyers, y_train_buyers):
  N = SINGLESTGAN.model['N']
  y_train_buyers = apply_pad_arr(y_train_buyers, N)
  i_loss, i_acc = i_model.train_on_batch(np.array(X_train_buyers).astype(np.float32), y_train_buyers)
 
  return i_loss, i_acc

def train_discriminator(g_model, d_model, latent_dim, dataset, batch_size, N, use_buyers=True, print_examples=False):
  X_real, _, _ = get_buyers_sample(dataset, int(batch_size), 'train') if use_buyers else get_samples_from_all(dataset, 'train', batch_size)

  X_real = apply_pad(X_real, N)

  y_real = zeros(batch_size) #flip
  
  noise_base = 0.02
  y_real = noisy_labels(y_real, noise_base)

 
  d_loss1_r, d_acc1_r = d_model.train_on_batch(X_real, y_real)

  X_fake, y_fake = generate_fake_samples(g_model, latent_dim, batch_size)

  if print_examples:
    logger.info("Example real\n: " + str(create_readable_df(X_real[0])))
    logger.info("Example fake\n: " + str(create_readable_df(X_fake[0])))
  

  X_fake = apply_pad(X_fake, N) if X_fake[0].shape[0] < N else X_fake
  y_fake = noisy_labels(y_fake, noise_base)

  d_loss1_f, d_acc1_f = d_model.train_on_batch(X_fake, y_fake)

  return ((d_loss1_r, d_acc1_r),(d_loss1_f, d_acc1_f))

def append_losses(c_loss, i_loss, d_loss1_r, d_loss1_f, g_loss, losses_train):
  losses_c_train,losses_i_train,losses_d_train,losses_g_train = losses_train

  d_loss = np.mean([d_loss1_r, d_loss1_f])
  losses_c_train.append(c_loss)
  losses_i_train.append(i_loss)
  losses_d_train.append(d_loss)
  losses_g_train.append(g_loss)
  
  return losses_train

def update_losses_in_epoch(losses_train, losses_epochs):
  losses_c_train,losses_i_train,losses_d_train,losses_g_train = losses_train
  losses_c_ep,losses_i_ep,losses_d_ep,losses_g_ep = losses_epochs
 
  c_train_mean = np.mean(losses_c_train)
  i_train_mean = np.mean(losses_i_train)
  d_train_mean = np.mean(losses_d_train)
  g_train_mean = np.mean(losses_g_train)
  
  logger.info("Overview training losses: "+ str(c_train_mean) + ", " + str(i_train_mean) + ", " + str(d_train_mean) + " and " + str(g_train_mean))

  losses_c_ep.append(c_train_mean)
  losses_i_ep.append(i_train_mean)
  losses_d_ep.append(d_train_mean)
  losses_g_ep.append(g_train_mean)

  losses_epochs = losses_c_ep,losses_i_ep,losses_d_ep,losses_g_ep

  SINGLESTGAN.save_losses_train(losses_c_ep, losses_i_ep, losses_d_ep, losses_g_ep)
  return losses_epochs

def gen(X,y, batch_size, N):
    while True:
    # loop once per epoch
        num_recs = X.shape[0]
        num_batches = num_recs // batch_size

        for bid in range(num_batches):
            Xbatch = np.array(apply_pad(X[bid * batch_size : (bid + 1) * batch_size], N)).astype(np.float32)
            ybatch = np.array(y[bid * batch_size : (bid + 1) * batch_size]).astype(np.float32)
            yield Xbatch, ybatch

def train_c_model(dataset, i_model, c_model, train_epochs=None, batch_size=None, patience=None, load_cmodel=False):
  logger.info("In train_c_model,  load_c_model: " + str(load_cmodel))

  N = SINGLESTGAN.model['N'] 
  
  logger.info("Training c_model separated. ")
  X_train, y_train_class, _ = dataset['all_train_shuffled']

  X_valid, y_valid, _, _ = dataset['all_valid']
  
  train_epochs  = SINGLESTGAN.model['train_epochs'] if not train_epochs else train_epochs
  batch_size = SINGLESTGAN.model['batch_size'] if not batch_size else batch_size
  patience = SINGLESTGAN.model['es_patience'] if not patience else patience
  patience = int(patience /(7))
  
  logger.info("train_c_model, patience: " + str(patience))
  
  monitor_f = 'val_auc'
  mode_f = 'max'
  
  if not load_cmodel:
    hist = c_model.fit(gen(X_train, y_train_class, batch_size, N), 
      epochs=train_epochs, 
      validation_data=gen(X_valid,y_valid, batch_size, N),
      validation_steps=min(X_valid.shape[0]//batch_size, 1),
      callbacks=[
      EarlyStopping(patience=patience, restore_best_weights=True, monitor='val_loss',mode='min'),
      ModelCheckpoint(SINGLESTGAN.model_weights_path + "best_c_model.h5", monitor=monitor_f, verbose=1,\
        save_best_only=True, save_weights_only=True, mode=mode_f)
    ], verbose=1, steps_per_epoch=(X_train.shape[0]/batch_size))
    
    plot_history(hist.history, SINGLESTGAN.res_path + "c_model")

  logger.info("Loading c_model!")
  c_model = SINGLESTGAN.load_model_c_model(c_model)
  return c_model

def train_gan_separated(g_model, gan_model, disc_model, dataset, latent_dim,\
  train_epochs=None, batch_size=None, patience=None, with_label = None):
  d_model, i_model, c_model = disc_model

  total_train = dataset['buyers_train'][0].shape[0] 

  bat_per_epo = (total_train // batch_size)
  n_steps = bat_per_epo * train_epochs
  half_batch =batch_size//2

  N = SINGLESTGAN.model['N'] 

  logger.info("Training params: (epochs, batch_size, patience) " + str((train_epochs, batch_size, patience)))
  logger.info("Training params: (bat_per_epo, n_steps) " + str((bat_per_epo, n_steps)))

  for i in range(n_steps):
    c_step = i//bat_per_epo

    input_print = True if i % 20 == 0 else False
    ((d_loss1_r, d_acc1_r), (d_loss1_f, d_acc1_f))  = train_discriminator(g_model, d_model, latent_dim,\
    dataset, half_batch, N, True, input_print)
      # gan_model
    g_loss = train_gan(gan_model, latent_dim, batch_size)
    if i % 5 == 0:
      logger.info('>%d,%d, d1[%.5f,%.5f,%.5f,%5f],g[%.5f]'\
        % \
            (c_step,i,\
              d_loss1_r, d_acc1_r, d_loss1_f, d_acc1_f,\
              g_loss))


def train_single(g_model, gan_model, disc_model, dataset, latent_dim, session_original_clicks_test, session_buyer_targets,\
  train_epochs=None, batch_size=None, patience=None, with_label = None, gan_disabled =False):
  logger.info("Training single-stage model..")
  # d1_mode d2_model, i_model, c_model = disc_model
  d_model, i_model, c_model = disc_model
  # select trainervised dataset
  # calculate the number of batches per training epoch
  N = SINGLESTGAN.model['N'] 

  total_train = dataset['all_train_shuffled'][0].shape[0] 
  bat_per_epo = (total_train // batch_size)
  # calculate the number of training iterations
  n_steps = bat_per_epo * train_epochs

  X_valid, y_valid, _, _ = dataset['all_valid']
  X_valid_pad = apply_pad(X_valid, N)

  valid_sids = dataset['valid_buyer_sessions']
  X_valid_buyer, y_valid_buyer = dataset['buyers_valid']

  session_original_clicks_valid = load_session_original_clicks_valid()
  losses_i_train, losses_c_train, losses_d_train, losses_g_train = [],[],[],[]

  losses_i_ep, losses_c_ep, losses_d_ep, losses_g_ep = [],[],[],[]

  losses_i_ep_val, losses_c_ep_val, losses_i_auc_val, losses_c_auc_val = [],[],[],[]

  best_val_score_sess, last_val_score_sess, last_val_score,  best_val_score = (-10e3,-10e3,-10e3,-10e3)

  half_batch = int(batch_size//2)

  c_patience = 0
  i_patience = 0
  
  last_batch = 0

  max_c_patience = 3
  max_i_patience = 3

  c_disabled = False
  i_disabled = False

  i_loss, i_acc, c_loss, c_acc = (0,0,0,0)
  d_loss, g_loss = (0,0)

  test_i_loss = -10e3 
  test_c_loss = -10e3 

  cdate = get_current_datetime()

  d_loss1_r, d_acc1_r, d_loss1_f, d_acc1_f = (0,0,0,0)

  MAX_STEPS = SINGLESTGAN.model['train_epochs']

  all_patience = patience
  
  logger.info("Training params: (epochs, batch_size, patience, c_patience, i_patience) " + str((train_epochs, batch_size, patience, max_c_patience, max_i_patience)))
  logger.info("Training params: (bat_per_epo, n_steps) " + str((bat_per_epo, n_steps)))
  logger.info("c_disabled: " + str(c_disabled))

  # manually enumerate epochs
  for i in range(n_steps):
    c_step = i//bat_per_epo
    # update trainervised discriminator (c)
    # c_model
    (X_train_class, y_train_class), (X_train_buyers, y_train_buyers),last_batch = get_batches_from_all(dataset, N, batch_size, last_batch)
    # X_train_buyers, y_train_buyers,last_batch = get_batches_from_buyers(dataset, N, batch_size, last_batch)

    if not c_disabled:
      c_loss, c_acc = train_session_classifier(c_model, X_train_class, y_train_class) 
    else:
      c_loss, c_acc = (0,0)
    
    if not i_disabled:
      i_loss, i_acc = train_item_from_buyers(i_model, X_train_buyers, y_train_buyers) 
    else:
      i_loss, i_acc = (0,0)
    # # d_model 
    if not gan_disabled:
      print_i = i % (bat_per_epo * 10) == 0
      ((d_loss1_r, d_acc1_r), (d_loss1_f, d_acc1_f))  = train_discriminator(g_model, d_model, latent_dim,\
      dataset, half_batch, N, False, print_i)
      # gan_model
      g_loss = train_gan(gan_model, latent_dim, batch_size)
    else:
      g_loss = 0
      ((d_loss1_r, d_acc1_r), (d_loss1_f, d_acc1_f))  = ((0,0), (0,0))
    # losses update 
    losses_train = losses_c_train,losses_i_train,losses_d_train,losses_g_train
    losses_c_train, losses_i_train, losses_d_train, losses_g_train = append_losses(c_loss, i_loss, d_loss1_r, d_loss1_f,\
      g_loss, losses_train)

    #  logger.info("Saving models")
    if i % ceil(bat_per_epo*0.2) == 0:      
      logger.info("Current date: " + str(cdate) + " and label " + str(with_label))
      logger.info("Best item model session val score: " + str(best_val_score))
      logger.info("Best class model session val score: " + str(best_val_score_sess))
      logger.info('>%d,%d, i[%.5f,%.5f, c[%.5f,%.5f]\n, d1[%.5f,%.5f,%.5f,%5f],g[%.5f]'\
        % \
            (c_step,i,\
             i_loss, i_acc, \
             c_loss, c_acc,\
             d_loss1_r, d_acc1_r, d_loss1_f, d_acc1_f,\
             g_loss))
    if (i) % (bat_per_epo * 10) == 0 and i != 0: # finished epoch
      logger.info("-------------- I MODEL IN TEST ------------------")
      if not i_disabled: evaluate_i_model_test(dataset, gan_model, disc_model, session_original_clicks_test, session_buyer_targets, True)
      if not c_disabled: 
        logger.info("---------------C MODEL IN TEST -----------------")
        evaluate_c_model_test(dataset, gan_model, disc_model)
    # evaluate the model performance every so often
    if (i) % (bat_per_epo * 1) == 0 and i > 0: # finished epoch
      logger.info("Evaluating model")

      dataset = shuffle_train_buyers(dataset)

      losses_epoch = losses_c_ep,losses_i_ep,losses_d_ep,losses_g_ep
      update_losses_in_epoch(losses_train, losses_epoch) 
      # TODO save gan_losses_i_ep_val
      if i_disabled == False:
        losses_i_ep_val, losses_i_auc_val, i_patience, test_i_loss, test_i_acc, test_i_auc, best_val_score, last_val_score = evaluate_i_model_2(i_model, c_model, N,\
        X_valid_buyer, y_valid_buyer, valid_sids, losses_i_ep_val, losses_i_auc_val,\
          best_val_score, i_patience, c_step, all_patience, last_val_score,
          session_original_clicks_valid, session_buyer_targets)
      if c_disabled == False:
        losses_c_ep_val, losses_c_auc_val, c_patience, test_c_loss, test_c_acc, test_c_auc, best_val_score_sess, last_val_score_sess = \
        evaluate_c_model_2(i_model, c_model, X_valid_pad, y_valid, \
          losses_c_ep_val, losses_c_auc_val, best_val_score_sess, c_patience, c_step, all_patience, last_val_score_sess)
        
      SINGLESTGAN.save_losses_eval(losses_c_ep_val, losses_i_ep_val, losses_c_auc_val, losses_i_auc_val)
      if c_patience > max_c_patience:
        logger.info("c_model disabled!")
        c_disabled = True

      if i_patience > max_i_patience:
        logger.info("i_model disabled!")
        i_disabled = True

      if c_disabled and i_disabled:
        logger.info("stop!")
        return

      losses_i_train, losses_c_train, losses_d_train, losses_g_train = [], [], [], []
      # summarize_performance(params, i, g_model, si_model, latent_dim,\
                          # dataset, stats, dataName='valid', n_samples=500000, show_g = False, batch_size=20000)
  logger.info("*****************")
  logger.info("Found best_val_score_sess and best_val_score: " + str(best_val_score_sess) + " , " + str(best_val_score))

def truncate_for_last_N(X_pad, N):
  X_new = []
  for i in range(0,len(X_pad)):
    if i % 100 == 0: logger.info(i/X_pad.shape[0])
    x = X_pad[i]
    if len(x) >= N:
      la = LabelEncoder()
      item_id_arr = x[:,0] 
      x[:,0] = la.fit_transform(item_id_arr)
    X_new.append(x)
  X_new = np.array(X_new)
  return X_new

def evaluate_c_model_test(dataset, gan_model, disc_model):
  logger.info("Evaluating for c_model..")
  N = SINGLESTGAN.model['N']
  X_test, y_test, _, _ = dataset['all_test']
  
  X_test = apply_pad(X_test, N)
  # X_test = truncate_for_last_N(X_test, N)

  d_model, i_model, c_model = disc_model
  
  y_test_pred = c_model.predict(np.array(X_test))
  y_class = y_test_pred > 0.5 
  
  report = classification_report(y_test, y_class, target_names=['non_buyer','buyer'])
  
  logger.info("Final report for c_model:\n" + str(report)) 
  
  SINGLESTGAN.save_results_c_model(report) 
  auc = get_roc_curve(y_test_pred, y_test, SINGLESTGAN.res_path, final_name='c_model_roc_curve.png') 
  logger.info('AUC = %f' % (auc))


def evaluate_i_model_test(dataset, gan_model, disc_model, session_original_clicks, session_buyer_targets, print_res=True):
  logger.info("Evaluating for i_model..")
  N=SINGLESTGAN.model['N']
  X_test, y_test = dataset['buyers_test']
  test_sids = dataset['test_buyer_sessions']
  
  X_test = apply_pad(X_test, N)

  d_model, i_model, c_model = disc_model

  y_pred = i_model.predict(X_test)
     
  final_class_flat, final_y_test, y_test_proba = generate_labels_and_predictions_by_sessions_within_clicks_for_crf(test_sids, y_pred, session_buyer_targets, session_original_clicks)
 
  report = classification_report(final_y_test, final_class_flat, target_names=['non_buyer','buyer'])
  if print_res:
    logger.info("Final report for i_model:\n" + str(report)) 
    SINGLESTGAN.save_results_i_model(report) 
  auc = get_roc_curve(y_test_proba, final_y_test, SINGLESTGAN.res_path, final_name='i_model_roc_curve.png', plot= print_res) 
  logger.info('AUC = %f' % (auc))
  
def annot_max(x,y, ax=None, xi=0.94, yi=0.96):
    xmax = x[np.argmax(y)]
    ymax = y.max()
    text= "x={:.3f}, y={:.3f}".format(xmax, ymax)
    if not ax:
        ax=plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops=dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data',textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    ax.annotate(text, xy=(xmax, ymax), xytext=(xi,yi), **kw)


def plot_losses(last_folder=None):
  losses_d_ep, losses_i_ep, losses_c_ep, losses_g_ep = SINGLESTGAN.load_losses_train(last_folder)
  losses_i_ep_eval, losses_c_ep_eval, losses_i_auc_val, losses_c_auc_val = SINGLESTGAN.load_losses_eval(last_folder)

  fig, ax = plt.subplots()
  
  ax.plot(losses_d_ep,label='discriminator')
  ax.plot(losses_g_ep,label='generator')
  ax.set_title("discriminator and generator train loss")
  ax.legend(loc = "lower left")
  fig.savefig(SINGLESTGAN.res_path + "gan_losses.png")


  
  plt.figure()

  #fig, ax = plt.subplots()
  #ax.plot(losses_g_ep)
  #ax.set_title("Generator train loss")
  #fig.savefig(SINGLESTGAN.res_path + "generator.png")

  fig, axs = plt.subplots(2)

  axs[0].plot(losses_c_ep[:len(losses_c_ep_eval)])
  axs[0].plot(losses_c_ep_eval)
 
  axs[0].title.set_text('model losses c_model')
  axs[0].legend(['train', 'val'], loc='upper left')

  axs[1].plot(losses_i_ep[:len(losses_i_ep_eval)])
  axs[1].plot(losses_i_ep_eval)

  axs[1].set_title('model losses i_model')
  axs[1].legend(['train', 'val'], loc='upper left')

  fig.text(0.5, 0.04, 'Epochs', ha='center', va='center')
  fig.text(0.06, 0.5, 'Losses', ha='center', va='center', rotation='vertical')
  
  fig.tight_layout(h_pad=2)

  fig.savefig(SINGLESTGAN.res_path + "losses_models.png")

  fig, ax = plt.subplots()

  ax.plot(range(0,len(losses_c_auc_val)),losses_c_auc_val, label="c_model")
  ax.plot(range(0,len(losses_i_auc_val)),losses_i_auc_val, label="i_model")

  ax.legend(loc = "lower left")

  x = list(range(0,len(losses_c_auc_val)))

  annot_max(x, losses_c_auc_val, ax=ax, xi=0.44, yi=0.96)

  ulti_max = max((max(losses_c_auc_val), max(losses_i_auc_val)))

  ax.plot(losses_i_auc_val)

  x = list(range(0,len(losses_i_auc_val)))

  annot_max(x, losses_i_auc_val, ax=ax)

  plt.ylim(0.0, 1.15 * (ulti_max))

  ax.set_title("AUC c_model and AUC_i_model")
  fig.savefig(SINGLESTGAN.res_path + "auc_values_in_both_models_eval.png")
  
  plt.figure()


def treat_args(args):
  only_evaluate = args.only_evaluate if args.only_evaluate else False
  use_gru = args.use_gru if args.use_gru else False
  train_epochs = SINGLESTGAN.model['train_epochs'] if not args.train_epochs else args.train_epochs
  batch_size = SINGLESTGAN.model['batch_size'] if not args.batch_size else args.batch_size
  patience = SINGLESTGAN.model['es_patience'] if not args.patience else args.patience
  load_c_model = args.load_c_model if args.load_c_model else False
  with_date = args.with_date if args.with_date else None
  with_label = args.with_label
  disable_gan = args.disable_gan
  
  return disable_gan, with_label, with_date, load_c_model, only_evaluate, use_gru, train_epochs, batch_size, patience

def train_and_evalute(args):
  disable_gan, with_label, with_date, load_c_model, only_evaluate, use_gru, train_epochs, batch_size, patience = treat_args(args)

  update_c_date(with_date)
  SINGLESTGAN.updatePaths(get_current_datetime(), with_label)
  SINGLESTGAN.constructFolderPaths()

  SINGLESTGAN.save_args(args)

  logger.info("Training ")

  g_model, gan_model, disc_model = build_single(use_gru)
  d_model, i_model, c_model = disc_model 

  latent_dim = SINGLESTGAN.model['latent_dim'] 
  dataset = construct_dataset()

  session_original_clicks_test = load_session_original_clicks()
  session_buyer_targets = load_session_buyer_targets()

  SINGLESTGAN.save_models_summary(d_model, i_model, c_model, g_model) 
  SINGLESTGAN.save_models(g_model, d_model, i_model, c_model) 
  
  if only_evaluate == False:
    #train_gan_separated(g_model, gan_model, disc_model, dataset, \
    #  latent_dim, 
    #  train_epochs, batch_size, patience, with_label)

    # c_model = train_c_model(dataset, i_model, c_model, train_epochs, batch_size, patience, load_c_model)
    train_single(g_model, gan_model, disc_model, dataset, \
      latent_dim, session_original_clicks_test, session_buyer_targets, 
      train_epochs, batch_size, patience, with_label, disable_gan)
  
  # g_model, d_model, i_model, c_model = SINGLESTGAN.load_models(g_model, d_model, i_model, c_model, with_date)

  g_model, d_model, i_model, c_model = SINGLESTGAN.load_models() 
  args_str = SINGLESTGAN.show_args()
  logger.info("With args: **** " + str(args_str))

  get_last_store_path = SINGLESTGAN.get_last_path()
  logger.info("Last folder in path: " + get_last_store_path)
  
  c_model = SINGLESTGAN.load_model_c_model(c_model, get_last_store_path) 
  disc_model = d_model, i_model, c_model
  evaluate_c_model_test(dataset, gan_model, disc_model)

  i_model= SINGLESTGAN.load_model_i_model(i_model, get_last_store_path) 
  disc_model = d_model, i_model, c_model
  evaluate_i_model_test(dataset, gan_model, disc_model, session_original_clicks_test, session_buyer_targets)
  
  X_fake, _ = generate_fake_samples(g_model, latent_dim, 2)
  
  logger.info("Fake example 1: \n" + str(create_readable_df(X_fake[0])))
  logger.info("Fake example 2: \n" +str(create_readable_df(X_fake[1])))
  
  plot_losses(get_last_store_path)