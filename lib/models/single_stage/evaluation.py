
from sklearn.metrics import *
from math import ceil
import pandas as pd
from tensorflow.keras.metrics import AUC
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.initializers import RandomNormal
from lib.utils import *
from sklearn.preprocessing import MultiLabelBinarizer
import matplotlib.pyplot as plt
from numpy.random import randint
from lib.input.common import *
from lib.third_party.keras_crf import CRF
from numpy import ones, zeros
from sklearn.utils import shuffle
from collections import Counter
from lib.models.classes.transformer import *
from tensorflow.keras.layers import *
from tensorflow.keras.callbacks import *
from tensorflow.keras import Model
from tensorflow.keras.losses import *
from tensorflow.keras.optimizers import *
from lib.config.controls.singlestgan import SINGLESTGAN
from lib.models.two_stage.generic import generate_labels_and_predictions_by_sessions_within_clicks_for_crf
import numpy as np
from numpy import ones
from sklearn.metrics import precision_score
from collections import Counter
from numpy.random import randn, seed
from lib.models.common import *
from tensorflow.keras import backend as K
from sklearn.metrics import roc_curve, roc_auc_score
from lib.utils import get_current_datetime, update_c_date

import lib.config.logger as log
logger = log.get_logger()
mask = True

def get_auc_sc(y_pred, y_test):
    y_test = np.array(y_test)
    y_predicted = np.array(y_pred)
    auc_purchase = roc_auc_score(y_test, y_predicted)
    return auc_purchase

def evaluate_i_model_2(i_model, c_model, N, X_valid_buyer, y_valid_buyer, valid_sids, \
  losses_i_ep_val, losses_i_auc_val, best_val_score, i_patience, c_epoch, all_patience, \
    last_val_score, session_original_clicks_valid, session_buyer_targets_valid,  print_res=True):
  logger.info("Evaluate i_model in valid")
  logger.info("Current best_val_score: " + str(best_val_score))


  N=SINGLESTGAN.model['N']
  
  X_valid_buyer = apply_pad(X_valid_buyer, N)
  y_pred = i_model.predict(X_valid_buyer)
     
  final_class_flat, final_y_valid, y_test_proba = \
  generate_labels_and_predictions_by_sessions_within_clicks_for_crf(valid_sids, \
    y_pred, session_buyer_targets_valid, session_original_clicks_valid)

  report = classification_report(final_y_valid, final_class_flat, target_names=['non_buyer','buyer'])
  if print_res:
    logger.info("Final report for i_model:\n" + str(report)) 
    SINGLESTGAN.save_results_i_model(report) 
  auc = get_auc_sc(y_test_proba, final_y_valid) 
  logger.info('AUC = %f' % (auc))

  y_valid_buyer = apply_pad_arr(y_valid_buyer, N)
  
  test_i_loss, test_i_acc = i_model.evaluate(X_valid_buyer, y_valid_buyer,batch_size=10000,verbose=1) #TODO: not evaluate two times
  
  losses_i_ep_val.append(test_i_loss)
  losses_i_auc_val.append(auc)
  val_score = auc

  if val_score > best_val_score:
    best_val_score = val_score 
    logger.info("\tFound best item val score!" + str(best_val_score))
    SINGLESTGAN.save_models_i_model(i_model, best_val_score)
    i_patience = 0
  if val_score > last_val_score:
    i_patience = 0
  else:
    logger.info("Current i_patience: " + str(i_patience))
    if c_epoch < all_patience:
      logger.info("In all patience currently..")
    else:
      i_patience +=1
      
  last_val_score = val_score

  return  (losses_i_ep_val, losses_i_auc_val, i_patience, test_i_loss, test_i_acc, auc, best_val_score, last_val_score)


def evaluate_i_model(i_model, c_model, N, X_valid_buyer, y_valid_buyer, \
  losses_i_ep_val, best_val_score, i_patience, c_epoch, all_patience, last_val_score):

  logger.info("Evaluate i_model")
  logger.info("Current best_val_score: " + str(best_val_score))

  N = SINGLESTGAN.model['N']

  y_valid_buyer = apply_pad_arr(y_valid_buyer, N)
  X_valid_buyer = apply_pad(X_valid_buyer, N) 
 
  test_i_loss, test_i_acc = i_model.evaluate(X_valid_buyer, y_valid_buyer,batch_size=10000)
  
  losses_i_ep_val.append(test_i_loss)
  val_score = test_i_acc
  
  logger.info("Found test_i_loss, test_i_acc: " + str(test_i_loss) + " " + str(test_i_acc))

  if val_score > best_val_score:
    best_val_score = val_score 
    logger.info("\tFound best item val score!" + str(best_val_score))
    SINGLESTGAN.save_models_i_model(i_model)
    i_patience = 0
  if val_score > last_val_score:
    i_patience = 0
  else:
    logger.info("Current i_patience: " + str(i_patience))
    if c_epoch < all_patience:
      logger.info("In all patience currently..")
    else:
      i_patience +=1
      
  last_val_score = val_score

  return  (losses_i_ep_val, i_patience, test_i_loss, test_i_acc, best_val_score, last_val_score)

def calculate_c_loss(y_pred, y_valid, batch_size=1000):
  y_pred = y_pred[:,0]
  i = 0
  l = 0
  m = BinaryCrossentropy()
  assert(len(y_pred) == len(y_valid))
  l += m(y_valid, y_pred).numpy()
  return l

def evaluate_c_model_2(i_model, c_model, X_valid_pad, y_valid, \
  losses_c_ep_val, losses_c_auc_val, best_val_score_sess, \
  c_patience, c_epoch, all_patience, last_val_score_sess):
  
  logger.info("Evaluate c_model in valid")
  logger.info("Current best_val_score_sess: " + str(best_val_score_sess))

  y_pred = c_model.predict(X_valid_pad, batch_size=10000,verbose=1)

  test_c_loss = calculate_c_loss(y_pred, y_valid, 100)
  
  test_c_auc = get_auc_sc(y_pred, y_valid) 

  test_c_acc = 0.0 # TODO: remove

  losses_c_ep_val.append(test_c_loss)
  losses_c_auc_val.append(test_c_auc)
  
  val_score_sess = test_c_auc
  logger.info("Found test_c_loss, test_c_auc: " + str(test_c_loss) + " " + str(test_c_auc))
  # logger.info("Found precision, recall: " + str(test_c_p) + " " + str(test_c_r))
  if val_score_sess > best_val_score_sess:
    best_val_score_sess = val_score_sess 
    logger.info("\tFound best val score session!" + str(best_val_score_sess))
    SINGLESTGAN.save_models_c_model(c_model, best_val_score_sess)
    c_patience = 0

  if val_score_sess > last_val_score_sess:
     c_patience = 0
  else:
    logger.info("Current c_patience: " + str(c_patience))
    if c_epoch < all_patience:
      logger.info("In all patience currently..")
    else:
      c_patience += 1
  last_val_score_sess = val_score_sess

  return (losses_c_ep_val, losses_c_auc_val, c_patience, test_c_loss, test_c_acc, test_c_auc, best_val_score_sess, last_val_score_sess)

def evaluate_c_model(i_model, c_model, X_valid_pad, y_valid, \
  losses_c_ep_val, losses_c_auc_val, best_val_score_sess, \
  c_patience, c_epoch, all_patience, last_val_score_sess):
  
  logger.info("Evaluate c_model in valid")
  logger.info("Current best_val_score_sess: " + str(best_val_score_sess))

  test_c_loss, test_c_acc, test_c_auc = c_model.evaluate(X_valid_pad,y_valid, batch_size=10000,verbose=1)
  
  losses_c_ep_val.append(test_c_loss)
  losses_c_auc_val.append(test_c_auc)
  
  val_score_sess = test_c_auc
  logger.info("Found test_c_loss, test metric, test_c_auc: " + str(test_c_loss) + " " + str(test_c_acc) + " " + str(test_c_auc))
  # logger.info("Found precision, recall: " + str(test_c_p) + " " + str(test_c_r))
  if val_score_sess > best_val_score_sess:
    best_val_score_sess = val_score_sess 
    logger.info("\tFound best val score session!" + str(best_val_score_sess))
    SINGLESTGAN.save_models_c_model(c_model, best_val_score_sess)
    c_patience = 0

  if val_score_sess > last_val_score_sess:
     c_patience = 0
  else:
    logger.info("Current c_patience: " + str(c_patience))
    if c_epoch < all_patience:
      logger.info("In all patience currently..")
    else:
      c_patience += 1
  last_val_score_sess = val_score_sess

  return (losses_c_ep_val, losses_c_auc_val, c_patience, test_c_loss, test_c_acc, test_c_auc, best_val_score_sess, last_val_score_sess)
