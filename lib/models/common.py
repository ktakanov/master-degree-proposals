
from tensorflow.keras.layers import *
from lib.models.classes.transformer import *
from tensorflow.keras import backend as K
import tensorflow as tf
from lib.utils import load_total_categories
from tensorflow.keras.models import Model
from lib.utils.process_input import MASK_VALUE

import lib.config.logger as log
logger = log.get_logger()

def access_column_expand_dims(arr,col):
  return K.expand_dims(arr[:,:,col],axis=-1)

def access_column(arr,col):
  return arr[:,:,col]



def build_embedding_for_gru_emb(X_in, N):
  total_categories = load_total_categories()
  logger.info("Total categories: " + str(total_categories))

  M = 10
  M2 = 25
  M3 = 10 
  M4 = 5 
  
  # masking_layer = Masking(mask_value=MASK_VALUE)
  MASK_ZERO = False
  
  gen_outs = [N, total_categories+1, 31+1, 4+1, 12+1, 7+1]
  # X_in = masking_layer(X_in)

  BASE_GRU=64
  
  e1 = Lambda(lambda x: access_column(x,0))(X_in) # order original item 
  e1 = Embedding(gen_outs[0]+1, M, name='emb_1', mask_zero=True)(e1)
  e1 = GRU(BASE_GRU, return_sequences=True)(e1)

  e2 = Lambda(lambda x: access_column(x,1))(X_in) #category
  e2 = Embedding(gen_outs[1]+1, M2,name='emb_2', mask_zero=MASK_ZERO)(e2)
  e2 = GRU(BASE_GRU, return_sequences=True)(e2)
  
  e3 = Lambda(lambda x: access_column(x,2))(X_in) #day
  e3 = Embedding(gen_outs[2]+1, M3, mask_zero=MASK_ZERO)(e3)
  e3 = GRU(BASE_GRU, return_sequences=True)(e3)

  e4 = Lambda(lambda x: access_column(x,3))(X_in) #hour
  e4 = Embedding(gen_outs[3]+1, M4, mask_zero=MASK_ZERO)(e4)
  e4 = GRU(BASE_GRU, return_sequences=True)(e4)
  
  e5 = Lambda(lambda x: access_column(x,4))(X_in) #month
  e5 = Embedding(gen_outs[4]+1, M4, mask_zero=MASK_ZERO)(e5)
  e5 = GRU(BASE_GRU, return_sequences=True)(e5)
  
  e6 = Lambda(lambda x: access_column(x,5))(X_in) #weekday
  e6 = Embedding(gen_outs[5]+1, M4, mask_zero=MASK_ZERO)(e6)
  e6 = GRU(BASE_GRU, return_sequences=True)(e6)
  # 
  idw = Lambda(lambda x: access_column_expand_dims(x,6))(X_in) #dwelltime
  # idw = Dense(5,activation='relu',name='emb_3')(idw)
  # 
  ir = Lambda(lambda x: access_column_expand_dims(x,7))(X_in) #item_rannk
  # ir = Dense(5,activation='relu',name='emb_4')(ir)

  ip = Lambda(lambda x: access_column_expand_dims(x,8))(X_in) #item_price_std
  # ip = Dense(5,activation='relu',name='emb_5')(ip)
  
  ipr = Lambda(lambda x: access_column_expand_dims(x,9))(X_in) #item_id_prob

  e7 = Concatenate(axis=-1)([idw,ir,ipr,ipr])
  # ipr = Dense(5,activation='relu',name='emb_6')(ipr)
  
  emb = Concatenate(axis=-1)([e1,e2,e3,e4,e5,e6,e7])
  return emb

def build_embedding_for_gru(X_in, N):
  total_categories = load_total_categories()
  logger.info("Total categories: " + str(total_categories))

  M = 10
  M2 = 25
  M3 = 10 
  M4 = 5 

  # masking_layer = Masking(mask_value=MASK_VALUE)
  MASK_ZERO = False
  
  gen_outs = [N, total_categories+1, 31, 4, 12, 7]
  # X_in = masking_layer(X_in)
  
  e1 = Lambda(lambda x: access_column(x,0))(X_in) # order original item 
  e1 = Embedding(gen_outs[0]+1, M, name='emb_1')(e1)

  e2 = Lambda(lambda x: access_column(x,1))(X_in) #category
  e2 = Embedding(gen_outs[1]+1, 100,name='emb_2')(e2)
  
  e3 = Lambda(lambda x: access_column_expand_dims(x,2))(X_in) 
  # e3 = Lambda(lambda x: access_column(x,2))(X_in) #day
  # e3 = Embedding(gen_outs[2]+1, M3, mask_zero=MASK_ZERO)(e3)


  e4 = Lambda(lambda x: access_column_expand_dims(x,3))(X_in)
  # e4 = Lambda(lambda x: access_column(x,3))(X_in) #hour
  # e4 = Embedding(gen_outs[3]+1, M4, mask_zero=MASK_ZERO)(e4)
  # 
  e5 = Lambda(lambda x: access_column_expand_dims(x,4))(X_in) 
  # e5 = Lambda(lambda x: access_column(x,4))(X_in) #month
  # e5 = Embedding(gen_outs[4]+1, M4, mask_zero=MASK_ZERO)(e5)
  
  e6 = Lambda(lambda x: access_column_expand_dims(x,5))(X_in) 
  # e6 = Lambda(lambda x: access_column(x,5))(X_in) #weekday
  # e6 = Embedding(gen_outs[5]+1, M4, mask_zero=MASK_ZERO)(e6)
  # 
  idw = Lambda(lambda x: access_column_expand_dims(x,6))(X_in) 
  # idw = Dense(5,activation='relu',name='emb_3')(idw)
  # 
  ir = Lambda(lambda x: access_column_expand_dims(x,7))(X_in) #item_rannk

  ir2 = Lambda(lambda x: access_column_expand_dims(x,8))(X_in) #item_rannk
  # ir = Dense(5,activation='relu',name='emb_4')(ir)

  ipr = Lambda(lambda x: access_column_expand_dims(x,9))(X_in) #item_id_prob
  # ipr = Dense(5,activation='relu',name='emb_6')(ipr)
  
  dpa = Lambda(lambda x: access_column_expand_dims(x,10))(X_in) # dist price avg
  # dpa = Dense(5,activation='relu',name='emb_6')(dpa)
  
  
  emb = Concatenate(axis=-1)([e1,e2,e3,e4,e5,e6,ir,ir2,idw,ipr,dpa])
  

  return emb

def build_embedding_for_trans(X_in, N):
  
  total_categories = load_total_categories()

  M = 10
  M2 = 25
  M3 = 10 
  M4 = 5 


  gen_outs = [N, total_categories+1, 31, 4, 12, 7]
  

  e1 = Lambda(lambda x: access_column(x,0))(X_in) # order original item 
  e1 = TokenAndPositionEmbedding(N+1, N+1, M, name='emb_1')(e1)

  e2 = Lambda(lambda x: access_column(x,1))(X_in) #category
  e2 = TokenAndPositionEmbedding(N, total_categories+1, 100,name='emb_2')(e2)
  
  e3 = Lambda(lambda x: access_column_expand_dims(x,2))(X_in) #day
  # e3 = TokenAndPositionEmbedding(N, 31+1, M3)(e3)
  e3 = NumericalPositionEmbedding(N,5)(e3)
  
  e4 = Lambda(lambda x: access_column_expand_dims(x,3))(X_in) #hour
  # e4 = TokenAndPositionEmbedding(N, 4+1, M4)(e4)
  e4 = NumericalPositionEmbedding(N,5)(e4)
  
  e5 = Lambda(lambda x: access_column_expand_dims(x,4))(X_in) #month
  e5 = NumericalPositionEmbedding(N,5)(e5)
  
  e6 = Lambda(lambda x: access_column_expand_dims(x,5))(X_in) #weekday
  e6 = NumericalPositionEmbedding(N,5)(e6)
  # 
  idw = Lambda(lambda x: access_column_expand_dims(x,6))(X_in) #dwelltime
  idw = NumericalPositionEmbedding(N,5)(idw)
  # idw = Dense(5,activation='relu',name='emb_3')(idw)
  # 
  ir = Lambda(lambda x: access_column_expand_dims(x,7))(X_in) #item_rannk
  ir = NumericalPositionEmbedding(N,5)(ir)
  # ir = Dense(5,activation='relu',name='emb_4')(ir)

  ip = Lambda(lambda x: access_column_expand_dims(x,8))(X_in) #item_price_std
  ip = NumericalPositionEmbedding(N,5)(ip)
  # ip = Dense(5,activation='relu',name='emb_5')(ip)
  
  ipr = Lambda(lambda x: access_column_expand_dims(x,9))(X_in) #item_id_prob
  ipr = NumericalPositionEmbedding(N,5)(ipr)
  # ipr = Dense(5,activation='relu',name='emb_6')(ipr)

  ipr2 = Lambda(lambda x: access_column_expand_dims(x,10))(X_in) #item_id_prob
  ipr2 = NumericalPositionEmbedding(N,5)(ipr2)
  # ipr2 = Dense(5,activation='relu',name='emb_7')(ipr2)
  
  
  emb = Concatenate(axis=-1)([e1,e2,e3,e4,e5,e6,idw,ir,ip,ipr, ipr2])
  return emb

def build_embedding(X_in, use_gru, N):
    if use_gru:
        return build_embedding_for_gru(X_in, N)
    else:
        return build_embedding_for_trans(X_in, N)
