from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from collections import defaultdict
from math import ceil
from collections import Counter
from sklearn.metrics import *
from numpy.random import randint
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.layers import  *
from tensorflow.keras.models import Model
from sklearn.preprocessing import MultiLabelBinarizer
from tensorflow.keras.callbacks import *
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import *
from tensorflow.keras.initializers import RandomUniform
from tensorflow.keras.metrics import AUC
from sklearn.metrics import accuracy_score
from lib.config.controls.twostage import TWOSTGTRANS
from keras_self_attention import SeqSelfAttention

from focal_loss import BinaryFocalLoss

from lib.models.classes.transformer import *
from lib.utils import *
from random import seed
import pickle
from lib.third_party.keras_crf import CRF
from tcn import TCN, tcn_full_summary

from lib.input.common import load_session_buyer_targets, load_session_order_targets, load_session_buyers_click_lengths, load_session_original_clicks, plot_history
from lib.models.common import *

import lib.config.logger as log
logger = log.get_logger()

def build_model(X_in, N, MODEL='first'):

    ec = build_embedding(X_in, True, N)

    # x = Bidirectional(GRU(64,return_sequences=True, activation='relu'))(ec)
    # x = LayerNormalization()(x)
    # x = Bidirectional(GRU(64,return_sequences=True, activation='relu'))(x)
    # x = LayerNormalization()(x)
    x = TCN(nb_filters=40,kernel_size=3, dilations=(1, 2, 4, 8, 16),  dropout_rate=0.2,  use_layer_norm=False, return_sequences=True)(ec)
    x = SeqSelfAttention(attention_type=SeqSelfAttention.ATTENTION_TYPE_MUL,
                       kernel_regularizer=keras.regularizers.l2(1e-4),
                       bias_regularizer=keras.regularizers.l1(1e-4),
                       attention_regularizer_weight=1e-4,
                       name='Attention')(x)
    # # x = Bidirectional(GRU(20))(x)


    num_heads = 5  # Number of attention heads
    ff_dim = 50  # Hidden layer size in feed forward network inside transformer

    # x = TransformerBlock(ec.shape[-1], num_heads, ff_dim, rate = 0.1)(ec)
    # x = TransformerBlock(x.shape[-1], num_heads, ff_dim, rate = 0.1)(x)
    # x = TransformerBlock(x.shape[-1], num_heads, ff_dim, rate = 0.1)(x)
    # x = TransformerBlock(x.shape[-1], num_heads, ff_dim, rate = 0.1)(x)

    DENSE =  15

    if MODEL != 'second':
        x = Flatten()(x)
    x = Dense(DENSE,activation='relu')(x)
    x = Dropout(0.2)(x)
    x = Dense(DENSE,activation='relu')(x)
    x = Dropout(0.3)(x)
    x = Dense(DENSE,activation='relu')(x)

    opt = Adam()
    loss_f = BinaryFocalLoss(gamma=2)
    metrics = [f1_m, precision_m, recall_m, AUC(name='auc')]
 
    if MODEL == 'second':
        x = Dense(2,activation='relu')(x)
        crf = CRF(2, sparse_target=False)
        x = crf(x)
        loss_f = crf.loss
        metrics = [crf.accuracy]
    else:
        # x= Flatten()(x)
        x = Dense(1,activation='sigmoid')(x)

    logger.debug("Using model: " + str(MODEL))


    model = Model(inputs=X_in, outputs=x)
    model.compile(optimizer=opt, loss=loss_f, metrics=metrics)

    logger.debug("Generated model:\n") 
    logger.debug(model.summary())
    return model

def choice_fit(model, epochs, batch_size, es_patience, mode_f, modeltype, monitor_f, X_train,y_train, X_valid, y_valid, use_gen=True, for_stage=1):
    history = None
    
    checkpoint_path = TWOSTGTRANS.get_model_checkpoint_path(modeltype)
    history_path = TWOSTGTRANS.get_model_history_path(modeltype)
    
    N = TWOSTGTRANS.model['N']
    if use_gen:
        steps_train = ceil(X_train.shape[0]/batch_size)
        steps_valid = ceil(X_valid.shape[0]/batch_size)
        history = model.fit(gen(X_train, y_train, batch_size, N,steps_train, for_stage), steps_per_epoch=(steps_train),
                        initial_epoch = 0,
                        validation_data=gen(X_valid,y_valid, batch_size, N,steps_valid, for_stage),
                        validation_steps=steps_valid,
                        shuffle=True,
                        epochs=epochs,
                    use_multiprocessing=False,
                    workers=1,
                        callbacks=[
                EarlyStopping(patience=es_patience, restore_best_weights=True, monitor=monitor_f,mode=mode_f),
                ModelCheckpoint(checkpoint_path + '/best_val_weights.h5', monitor='val_loss', verbose=1,\
                                save_best_only=True, save_weights_only=True, mode='min'),
                ModelCheckpoint(checkpoint_path + '/best_val_auc_weights.h5', monitor='val_auc', verbose=1,\
                                save_best_only=True, save_weights_only=True, mode='max')
                # ModelCheckpoint(checkpoint_path + '/weights_{epoch:08d}.h5', save_weights_only=True),
            ])
    else: 
        history = model.fit(X_train,
            y_train,
            initial_epoch=0,
            batch_size=batch_size,
            validation_data=(X_valid, y_valid),
            validation_freq=1,
            shuffle=True,
            epochs=epochs,
            callbacks=[
                EarlyStopping(patience=es_patience, restore_best_weights=False,mode=mode_f,monitor=monitor_f, min_delta=0.00),
                ModelCheckpoint(checkpoint_path + 'best_val_weights.h5', monitor='val_loss', verbose=1,\
                                save_best_only=True, save_weights_only=False, mode='min'),
                ModelCheckpoint(checkpoint_path + 'best_weights.h5', monitor='loss', verbose=1,\
                                save_best_only=True, save_weights_only=False, mode=mode_f),
                ModelCheckpoint(checkpoint_path + 'weights_{epoch:08d}.h5', save_weights_only=True)
            ])
    
    pickle.dump(history.history, open(history_path + "history",'wb'))
        
def train(model, X_train, y_train, X_valid, y_valid, modeltype, train_epochs=None, batch_size=None, patience=None, use_gen=True,for_stage=1):
    model_control = TWOSTGTRANS.model 
 
    train_epochs = model_control['train_epochs'] if not train_epochs else train_epochs 
    batch_size = model_control['batch_size'] if not batch_size else batch_size          
    es_patience = model_control['es_patience'] if not patience else patience
    
    logger.info("Training for (epochs, batch_size, patience): " + str(train_epochs) + ',' +  str(batch_size) + ',' + str(patience))

    monitor_f = 'val_loss'
    mode_f = 'min'
   
    choice_fit(model, train_epochs, batch_size, es_patience, mode_f,modeltype, monitor_f, \
        X_train, y_train, X_valid, y_valid, True,for_stage)

def gen(X,y, batch_size, N, steps, for_stage=1):
    while True:
    # loop once per epoch
        num_recs = X.shape[0]
        num_batches = steps

        for bid in range(num_batches):
            Xbatch = np.array(apply_pad(X[bid * batch_size : (bid + 1) * batch_size], N)).astype(np.float32)
            ybatch = np.array(y[bid * batch_size : (bid + 1) * batch_size])
            ybatch = apply_pad_arr(ybatch, N) if for_stage == 1 else ybatch
            yield Xbatch, ybatch

def manual_undersampling(X_train, y_train, n=1):
    seed(42)
    d = Counter(y_train)
    buyers = d[1]
    nonbuyers_ = d[0]

    nonbuyers = np.where(y_train == 0)[0]
    buyers = np.where(y_train == 1)[0]

    ix = randint(0,len(nonbuyers), int(len(buyers) * n))

    subnonbuyers = nonbuyers[ix]
    X_train = np.hstack((X_train[subnonbuyers],X_train[buyers]))
    y_train = np.hstack((y_train[subnonbuyers],y_train[buyers]))
    
    X_train, y_train = shuffle(X_train, y_train,random_state=1)
    
    return X_train, y_train

def train_snd(model, train_epochs, batch_size, patience, with_crf=True):

    logger.info("Training TwoStage model in second-stage..")
    N = TWOSTGTRANS.model['N']
    # F = F
    logger.info("Reading train")
    X_train, y_train, _ = TWOSTGTRANS.load_input_snd_stg("train")

    logger.info("Reading valid")
    X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.1, random_state=42)

    logger.info("Training..")
    train(model, X_train, y_train, X_valid, y_valid, 'second', train_epochs, batch_size, patience, True, 1) 
    
    
def train_fst(model, train_epochs, batch_size, patience, proportion):

    logger.info("Training TwoStage model in first-stage..")
    N = TWOSTGTRANS.model['N']
    # F = F
    logger.info("Reading train")
    X_train, y_train, _ = TWOSTGTRANS.load_input_fst_stg("train")
    logger.info("Total in train: " + str(X_train.shape[0]))
    logger.info(str(Counter(y_train)))
    X_train, y_train = manual_undersampling(X_train, y_train, proportion)
    logger.info("Total in train after sampling: " + str(X_train.shape[0]))
    logger.info(str(Counter(y_train)))
    logger.info("Reading valid")

    X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.1, random_state=42)


    logger.info("Total in valid: " + str(X_valid.shape[0]))
    logger.info(str(Counter(y_valid)))
    logger.info("Training..")
    
    X_train = np.array(apply_pad(X_train,N)).astype(np.float32)
    X_valid = np.array(apply_pad(X_valid,N)).astype(np.float32)
    
    train(model, X_train, y_train, X_valid, y_valid, 'first', train_epochs, batch_size, patience, True, 0) 
    
def pad_with_last_n_and_transform_items_ids(X, N):
    logger.info("Transforming test ids to truncate and adapt to model input.")
    X_new = []
    for i in range(0,len(X)):
        x = X[i]
        if len(x) >= N:
            la = LabelEncoder()
            item_id_arr = x[:,0] 
            x[:,0] = la.fit_transform(item_id_arr)
        X_new.append(x)
    X_new = np.array(X_new)     
    X_new = apply_pad(X_new, N)
    return X_new

def evaluate_fst(model):
    model = TWOSTGTRANS.load_model_weights_fst(model)
    X_test, y_test, _ = TWOSTGTRANS.load_input_fst_stg("test")
    N = TWOSTGTRANS.model['N']
    X_test = pad_with_last_n_and_transform_items_ids(X_test, N)
    y_pred = model.predict(X_test) 
    TWOSTGTRANS.save_predictions_and_labels_fst(y_pred, y_test)
    
    y_pred_labels = y_pred > 0.5 # TODO: get the best threshold.

    report = classification_report(y_pred_labels, y_test, target_names=['non_buyer','buyer'])
    
    logger.info('TWOSTTRANS first results:\n' + str(report))
    
    precision, recall, _ = precision_recall_curve(y_test, y_pred)
    
    auc_score = auc(recall, precision)
    print('Logistic PR AUC: %.3f' % auc_score)
    
    TWOSTGTRANS.save_results_fst(report) 
    auc_purchase = get_roc_curve(y_pred, y_test, TWOSTGTRANS.fst_res_path)
    
    logger.info("FIRST AUC: " + str(auc_purchase))

def train_and_evaluate_fst(only_evaluate, train_epochs, batch_size, patience, proportion):
    N = TWOSTGTRANS.model['N'] 
    F = TWOSTGTRANS.model['F'] 
    logger.info("Total features: " + str(F))
    X_in = Input(shape=(N,F))
    
    model = build_model(X_in, N)
    if only_evaluate == False:
        train_fst(model, train_epochs, batch_size, patience, proportion)

    # TODO: implement threshold
    evaluate_fst(model)
    
    plot_histories_fst()

def generate_buyer_clicks_sessions_targets(db):
    db['session_id'] = db.session_id.astype(str)
    return db.groupby('session_id')['item_id'].apply(lambda x: x.tolist()).to_dict()
    
def generate_sid_targets(X_test, test_sids, all_sid_buyer_clicks):
    sid_targets = {}
    for i in range(0,len(test_sids)):        
        c_sid = str(test_sids[i])
        
        item_id_arr = X_test[i][:,0]
    
        la = LabelEncoder() 
        y_i = all_sid_buyer_clicks[c_sid]
        y_i = la.fit_transform(y_i)
        sid_targets[c_sid] = y_i
        
    return sid_targets

def generate_predictions_by_sids_with_multilabel(test_sids,  y_pred):
    i = 0
    session_preds = {} # only get the positives
    session_probas = {}  # need to get all prodictions for all values in session to create the ROC curve
    for sid in test_sids:
        sid = str(sid)
        y_pred_i = y_pred[i] > 0.5
        y_original_labels = np.where(y_pred_i == True)[0]
        session_preds[sid] = y_original_labels
        session_probas[sid] = y_pred[i] # TODO: review, mayyvbe the order mattter
        i += 1
    return session_preds, session_probas

def generate_predictions_by_sids_for_crf(session_original_clicks, y_pred, test_sids):

    N = TWOSTGTRANS.model['N']

    session_preds = {} # only get the positives
    session_probas = {}  # need to get all prodictions for all values in session to create the ROC curve

    i = 0
    for session_id in test_sids:
        session_id = str(session_id)
        
        orig_clicks = session_original_clicks[session_id] 
        test_items = np.array(y_pred[i])

        orig_clicks = pad_array_bef(orig_clicks, N)
       
        curr_pred = defaultdict(float)
        j = 0
        for item_id in orig_clicks:
            test_item_id = test_items[j]
            label = np.argmax(test_item_id)

            
            if not item_id in curr_pred:
                curr_pred[item_id] = label 
            else:
                curr_pred[item_id] = (curr_pred[item_id] + label)/2.0
            j+=1
       
        if 0 in curr_pred: 
            del curr_pred[0] # remove elements in padding

        buyers = []
        for items,probs in curr_pred.items():
            if probs > 0.5:
                buyers.append(items)
            
        session_preds[session_id] = buyers
        session_probas[session_id] = curr_pred
        i+=1 
    return session_preds, session_probas
    
def generate_predictions_by_sids(session_original_clicks, test_sids, y_pred, use_crf):
    if use_crf:
        return generate_predictions_by_sids_for_crf(session_original_clicks, y_pred,test_sids)
    else:
        return generate_predictions_by_sids_with_multilabel(test_sids,y_pred)
        
def generate_labels_and_predictions_by_sessions_within_clicks_for_crf(test_sids, y_pred, session_buyer_targets, session_original_clicks):

    sid_pred_targets, sid_pred_proba = generate_predictions_by_sids(session_original_clicks, test_sids, y_pred, True)

    y_test = []
    y_pred_class = []
    y_pred_proba = []
    
    total_targets_removeds = 0
    total_targets = 0
    
    for sid in test_sids:
        sid = str(sid)
       
        original_clicks = session_original_clicks[sid] 
        buyer_targets = session_buyer_targets[sid]
        # print(sid, order_targets)
        predictions_class = sid_pred_targets[sid]

        pred_probas = sid_pred_proba[sid] # TODO: if use multilabel need to refactor
        
        original_clicks_set = set(original_clicks)
        
        total_unique_clicks = len(original_clicks_set)

        binarize_pred = np.zeros(total_unique_clicks)
        binarize_target = np.zeros(total_unique_clicks)
        probas_for_all_clicks = np.zeros(total_unique_clicks)
        
        original_clicks_pos_dict = dict(zip(original_clicks_set, range(0,total_unique_clicks)))
       
        total_targets += len(buyer_targets) 
        total_targets_removeds += len(buyer_targets.difference(original_clicks_set))
        buyer_targets = buyer_targets.intersection(original_clicks_set)

        for buyer in buyer_targets:
            posi = original_clicks_pos_dict[buyer]
            binarize_target[posi] = 1
       
        for pred_class in predictions_class:
            posi = original_clicks_pos_dict[pred_class]
            binarize_pred[posi] = 1

        for prediction, proba in pred_probas.items():
            posi = original_clicks_pos_dict[prediction]
            probas_for_all_clicks[posi] = proba

   
        binarized_pred = binarize_target.copy()
        # probas_for_all_clicks = binarize_target.copy()
   
        y_pred_class.append(binarize_pred) 
        y_test.append(binarize_target) 
        y_pred_proba.append(probas_for_all_clicks) 

    y_test = flat_list(y_test)
    y_pred_class = flat_list(y_pred_class)
    y_pred_proba = flat_list(y_pred_proba)
    logger.info("Total targets removed: " + str(total_targets_removeds) + " total targets " + str(total_targets)) 
    return (y_pred_class, y_test, y_pred_proba)


def generate_labels_and_predictions_by_sessions_within_clicks_multi_label(test_sids, y_pred, session_order_targets, session_clicks_lengths):
    # print(sid_targets
    
    sid_pred_targets, sid_pred_proba = generate_predictions_by_sids(test_sids, y_pred, False)

    y_test = []
    y_pred_class = []
    y_pred_proba = []
    
    for sid in test_sids:
        sid = str(sid)
        
        session_length = session_clicks_lengths[sid] 
        order_targets = session_order_targets[sid]
        # print(sid, order_targets)
        predictions = sid_pred_targets[sid]
        pred_probas = sid_pred_proba[sid]
        
        binarize_pred = np.zeros(session_length)
        binarize_target = np.zeros(session_length)
        probas_for_all_clicks = np.zeros(session_length)
        
       
        for order in order_targets:
            binarize_target[order] = 1
       
        for order in predictions:
            if order < session_length:
                binarize_pred[order] = 1
                
        for i in range(0,session_length):
            if i < len(pred_probas):
                probas_for_all_clicks[i] = pred_probas[i]
        
        y_pred_class.append(binarize_pred) 
        y_test.append(binarize_target) 
        y_pred_proba.append(probas_for_all_clicks) 

    y_test = flat_list(y_test)
    y_pred_class = flat_list(y_pred_class)
    y_pred_proba = flat_list(y_pred_proba)
    
    return (y_pred_class, y_test, y_pred_proba)


def generate_labels_and_predictions_by_sessions_within_clicks(test_sids, y_pre, use_crf=True):
    # print(sid_targets
    pass 
    # if use_crf:
    #     session_buyer_targets = load_session_buyer_targets() 
    #     session_original_click = load_session_original_clicks() 
        
    #     y_pred_class, y_test, y_pred_proba = generate_labels_and_predictions_by_sessions_within_clicks_for_crf(test_sids, y_pred, \
    #         session_buyer_targets, session_original_click
        
    # return (y_pred_class, y_test, y_pred_proba)


def evaluate_snd(model):
    N = TWOSTGTRANS.model['N'] 
    logger.info("Loading model weights..")
    model = TWOSTGTRANS.load_model_weights_snd(model)
    X_test, y_test, test_sids = TWOSTGTRANS.load_input_snd_stg("test")
    # y_pred, _ = TWOSTGTRANS.load_predictions_and_labels_snd()
    
    X_test = apply_pad(X_test, N)
    y_pred = model.predict(X_test)
    
    logger.info("Constructing predictions")
    
    session_buyer_targets = load_session_buyer_targets() 
    session_original_click = load_session_original_clicks() 
    
    y_pred_class, y_test, y_pred_proba = generate_labels_and_predictions_by_sessions_within_clicks_for_crf(test_sids, y_pred, \
        session_buyer_targets, session_original_click)

    report = classification_report(y_pred_class, y_test, target_names=['non_buyer','buyer'])
    logger.info('TWOSTTRANS second results:\n' + str(report))
   
    get_roc_curve(y_pred_proba, y_test, TWOSTGTRANS.snd_res_path)
    TWOSTGTRANS.save_results_snd(report)    
    
def plot_histories_fst():
    logger.info("Plotting losses for first-stage.")
    fst_history_path = TWOSTGTRANS.get_model_history_path('first')
    
    fst_history = pickle.load(open(fst_history_path + "history", "rb"))
    
    plot_history(fst_history, fst_history_path)
    
def  plot_histories_snd():
    logger.info("Plotting losses for second-stage.")
    
    snd_history_path = TWOSTGTRANS.get_model_history_path('second')
    
    snd_history = pickle.load(open(snd_history_path + "history", "rb"))
    
    plot_history(snd_history, snd_history_path)

def train_and_evaluate_snd(only_evaluate, train_epochs, batch_size, patience):
    N = TWOSTGTRANS.model['N'] 
    F = TWOSTGTRANS.model['F'] 
    X_in = Input(shape=(N,F))
    model = build_model(X_in, N, 'second')

    if only_evaluate==False:
        logger.info("Total features: " + str(F))
        train_snd(model,train_epochs, batch_size, patience)
    evaluate_snd(model)
    
    plot_histories_snd() 