from lib.config.controls.baseline import Baseline
from sklearn.metrics import *
from sklearn.ensemble import RandomForestRegressor
from imblearn.under_sampling import RandomUnderSampler
from sklearn.datasets import make_classification
from sklearn.metrics import classification_report
    

from sklearn.metrics import auc
from lib.utils import get_roc_curve
import matplotlib.pyplot as plt


import lib.config.logger as log
logger = log.get_logger()

def load_inputs_fst_stg(load_train=True,load_valid=True,load_test=True):
    train_inputs = Baseline.load_input_fst_stg("train") if load_train else None
    valid_inputs = Baseline.load_input_fst_stg("valid") if load_valid else None
    test_inputs = Baseline.load_input_fst_stg("test") if load_test else None
    
    return train_inputs, valid_inputs, test_inputs

def train_fst_stg(X_train, y_train, X_valid, y_valid):
    min_s_split = Baseline.first_stage_min_sample_split_param

    best_f1 = 0
    best_model = None 
    
    for mins in min_s_split:
        rnd = RandomForestRegressor(n_jobs=20, min_samples_split=mins)
        rnd.fit(X_train, y_train)

        y_pred = rnd.predict(X_valid) > 0.5
        f1_s = f1_score(y_valid, y_pred, average='micro')
        logger.debug("Found f1-score: " + str(f1_s))

        if f1_s > best_f1:
            logger.debug("Found best for min_split_sample: " + str(mins) + "\nwith f1-score: " + str(f1_s))
            best_model = rnd
            best_f1 = f1_s
        else:
            break

    logger.debug("Saving best weights for baseline..")            
    Baseline.save_model_fst(best_model)

def get_best_thres_fst(X_valid, y_valid, base_model):
    thresholds = list(range(1,100))
    best_f1 = 0
    best_thres = 0
    
    threshold_dec = 0.01 
    
    for thres in thresholds:
        logger.info("Using thres: " + str(thres * threshold_dec))
        
        y_pred = base_model.predict(X_valid) > thres*threshold_dec
        f1_s = f1_score(y_valid, y_pred)
        

        if f1_s > best_f1:
            logger.debug("\tfound best metric for thres" + str(thres*threshold_dec))
            logger.debug("\tfound metric: " + str(f1_s))
            best_f1 = f1_s
            best_thres = thres*threshold_dec
    logger.debug("Found best_thres: " + str(best_thres))
    return best_thres


def evaluate_fst(best_model, best_thres, X_test, y_test):
    y_pred = best_model.predict(X_test)
    y_label = y_pred > best_thres
    
    report = classification_report(y_test, y_label, target_names=['non_buyer','buyer'])
    logger.info("Found results for first stage:\n " + str(report))
    Baseline.save_results_fst(report)
    get_roc_curve(y_pred, y_test, Baseline.fst_stg_res_path)


def train_and_undersampling(X_train, y_train, X_valid, y_valid):
    rus = RandomUnderSampler()
    X_train, y_train = rus.fit_resample(X_train, y_train)

    logger.info("Training baseline for fist-stage")
    train_fst_stg(X_train, y_train, X_valid, y_valid)
    
def train_and_evaluate_fst(only_evaluate=False, optimize_thres=False):
    train_inputs, valid_inputs, test_inputs = load_inputs_fst_stg()
    X_train, y_train, _ = train_inputs
    X_valid, y_valid, _ = valid_inputs
    X_test, y_test, _ = test_inputs

    if only_evaluate == False:
        train_and_undersampling(X_train, y_train, X_valid, y_valid)
    
    model = Baseline.load_model_fst()
    
    logger.info("Evaluating baseline for fist-stage")
    best_thres = get_best_thres_fst(X_valid, y_valid, model) if optimize_thres else 0.5
    
    evaluate_fst(model, best_thres, X_test, y_test)