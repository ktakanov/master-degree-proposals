from lib.config.controls.baseline import Baseline
from sklearn.datasets import make_classification
from sklearn.metrics import classification_report
from sklearn.metrics import f1_score
from lib.utils import * 

import lib.config.logger as log
logger = log.get_logger()

from sklearn.ensemble import RandomForestRegressor

def load_inputs_snd_stg(load_train=True,load_valid=True,load_test=True):
    train_inputs = Baseline.load_input_snd_stg("train") if load_train else None
    valid_inputs = Baseline.load_input_snd_stg("valid") if load_valid else None
    test_inputs = Baseline.load_input_snd_stg("test") if load_test else None
    
    return train_inputs, valid_inputs, test_inputs

def train_snd_stg(X_train, y_train, X_valid, y_valid):
    
    min_s_split = Baseline.second_stage_min_sample_split_param

    best_f1 = 0
    best_model = None 
    
    for mins in min_s_split:
        rnd = RandomForestRegressor(n_jobs=20, min_samples_split=mins)
        rnd.fit(X_train, y_train)

        y_pred = rnd.predict(X_valid) > 0.5
        f1_s = f1_score(y_valid, y_pred, average='micro')
        logger.debug("Found f1-score: " + str(f1_s))

        if f1_s > best_f1:
            logger.debug("Found best for min_split_sample: " + str(mins) + "\nwith f1-score: " + str(f1_s))
            best_model = rnd
            best_f1 = f1_s
        else:
            break

    logger.debug("Saving best weights for baseline..")            
    Baseline.save_model_snd(best_model)
            
    return best_model

def get_best_thres_snd(X_valid, y_valid, base_model):
    thresholds = list(range(1,20))
    best_f1 = 0
    best_thres = 0
    for thres in thresholds:
        y_pred = base_model.predict(X_valid) > thres*0.05
        f1_s = f1_score(y_valid, y_pred)

        if f1_s > best_f1:
            logger.debug("\tfound best metric for thres" + str(thres*0.05))
            logger.debug("\tfound metric: " + str(f1_s))
            best_f1 = f1_s
            best_thres = thres*0.05
    logger.debug("Found best_thres: " + str(best_thres))
    return best_thres

def evaluate_snd(best_model, best_thres, X_test, y_test):
    y_pred = best_model.predict(X_test)
    y_label = y_pred > best_thres
    
    report = classification_report(y_test, y_label, target_names=['non_buyer','buyer'])
    logger.info("Found results for second stage:\n " + str(report))
    Baseline.save_results_snd(report)
    get_roc_curve(y_pred, y_test, Baseline.snd_stg_res_path)


def get_inputs_by_loaded(train_inp, valid_inp, test_inp):
    X_train, y_train = train_inp
    X_valid, y_valid = valid_inp
    X_test, y_test = test_inp
    
    X_train = flat_list(X_train) 
    y_train = flat_list(y_train) 

    X_valid = flat_list(X_valid) 
    y_valid = flat_list(y_valid) 

    X_test = flat_list(X_test) 
    y_test = flat_list(y_test) 

    return X_train, y_train, X_valid, y_valid, X_test, y_test
    
def train_and_evaluate_snd(only_evaluate=False, optimize_thres=False):
    X_train, y_train, X_valid, y_valid, X_test, y_test = get_inputs_by_loaded(*load_inputs_snd_stg())
    
    logger.info("Training baseline for second-stage")

    if only_evaluate == False:
        train_snd_stg(X_train, y_train, X_valid, y_valid)
    
    best_model = Baseline.load_model_snd()
    logger.info("Evaluating baseline for second-stage")
    best_thres = get_best_thres_snd(X_valid, y_valid, best_model) if optimize_thres else 0.5
    
    evaluate_snd(best_model, best_thres, X_test, y_test)