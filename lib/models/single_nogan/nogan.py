from lib.config.controls.singlestnogan import SINGLESTNOGAN
from tensorflow.keras import backend as K
from tensorflow.keras.losses import binary_crossentropy
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.initializers import RandomNormal
from lib.utils import *
from sklearn.preprocessing import MultiLabelBinarizer
from numpy.random import randint
from numpy.random import random
from lib.input.common import *
from numpy import ones, zeros
from sklearn.utils import shuffle
from collections import Counter
from lib.models.classes.transformer import *
from tensorflow.keras.layers import *
from tensorflow.keras import Model
from tensorflow.keras.losses import *
from tensorflow.keras.optimizers import *

from lib.models.two_stage.generic import generate_labels_and_predictions_by_sessions_within_clicks_for_crf
import numpy as np
from numpy import ones
from sklearn.utils import shuffle
from sklearn.metrics import *
from collections import Counter
from numpy.random import randn
from lib.models.single_stage import *
import lib.config.logger as log
logger = log.get_logger()
mask = True

def append_losses(c_loss, i_loss, d_loss1_r, d_loss1_f, g_loss, losses_train):
  losses_c_train,losses_i_train,losses_d_train,losses_g_train = losses_train

  d_loss = np.mean([d_loss1_r, d_loss1_f])
  losses_c_train.append(c_loss)
  losses_i_train.append(i_loss)
  losses_d_train.append(d_loss)
  losses_g_train.append(g_loss)
  
  return losses_train

def update_losses_in_epoch(losses_train, losses_epochs):
  losses_c_train,losses_i_train,losses_d_train,losses_g_train = losses_train
  losses_c_ep,losses_i_ep,losses_d_ep,losses_g_ep = losses_epochs

  losses_c_ep.append(np.mean(losses_c_train))
  losses_i_ep.append(np.mean(losses_i_train))
  losses_d_ep.append(np.mean(losses_d_train))
  losses_g_ep.append(np.mean(losses_g_train))
  
  SINGLESTNOGAN.save_losses(losses_c_ep, losses_i_ep, losses_d_ep, losses_g_ep)
  return losses_epochs

def evaluate_i_model_nogan(i_model, c_model, N, X_valid_buyer, y_valid_buyer, \
  losses_i_ep_val, best_val_score, i_patience, c_epoch, all_patience):

  N = SINGLESTNOGAN.model['N']

  y_valid_buyer = apply_pad_arr(y_valid_buyer, N)
  X_valid_buyer = apply_pad(X_valid_buyer, N) 
 
  test_i_loss, test_i_acc = i_model.evaluate(X_valid_buyer, y_valid_buyer)
  
  losses_i_ep_val.append(test_i_loss)
  val_score = test_i_loss
  
  logger.info("Found test_i_loss, test_i_acc: " + str(test_i_loss) + " " + str(test_i_acc))

  if val_score < best_val_score:
    best_val_score = val_score 
    logger.info("\tFound best item val score!" + str(best_val_score))
    SINGLESTNOGAN.save_models_with_better_i_model(i_model, c_model)
    i_patience = 0
  else:
    if c_epoch < all_patience:
      logger.info("In all patience currently..")
    else:
      logger.info("Current i_patience: " + str(i_patience))
      i_patience +=1
  return  (losses_i_ep_val, i_patience, test_i_loss, test_i_acc, best_val_score)

def evaluate_c_model_nogan(i_model, c_model, X_valid_pad, y_valid, \
  losses_c_ep_val, best_val_score_sess, c_patience, c_epoch, all_patience):
  
  K.clear_session()

  y_pred = c_model.predict(X_valid_pad, batch_size=10000,verbose=1)
  y_pred = y_pred > 0.5
  
  y_pred = np.array(flat_list(y_pred))
 
  test_c_loss_tmp = [] 
  for i in range(0,len(y_valid),1000):
    test_c_loss = K.eval(binary_crossentropy(K.variable(y_valid[i:i+1000]), K.variable(y_pred[i:i+1000])))
    test_c_loss_tmp.append(test_c_loss)

  test_c_loss = np.mean(test_c_loss_tmp)
  
  losses_c_ep_val.append(test_c_loss)
  test_c_f1 = f1_score(y_valid, y_pred)
  test_c_p = precision_score(y_valid, y_pred)
  test_c_r = recall_score(y_valid, y_pred)
  
  
  # np.save("gan_losses_c_ep_val",losses_c_ep_val)
  val_score_sess = test_c_loss 

  logger.info("Found test_c_loss, test_c_f1: " + str(test_c_loss) + " " + str(test_c_f1))
  logger.info("Found precision, recall: " + str(test_c_p) + " " + str(test_c_r))

  if val_score_sess < best_val_score_sess:
    best_val_score_sess = val_score_sess 
    logger.info("\tFound best val score session!" + str(best_val_score_sess))
    SINGLESTNOGAN.save_models_with_better_c_model(i_model, c_model)
    c_patience = 0
  else:
    if c_epoch < all_patience:
      logger.info("In all patience currently..")
    else:
      logger.info("Current c_patience: " + str(c_patience))
      c_patience += 1

  return (losses_c_ep_val, c_patience, test_c_loss, test_c_f1, best_val_score_sess)

def train_nogan(disc_model, dataset, latent_dim, train_epochs, batch_size, patience):
  logger.info("Training single-stage model WITHOUT GAN..")
  # d1_mode d2_model, i_model, c_model = disc_model
  d_model, i_model, c_model = disc_model
  # select trainervised dataset
  # calculate the number of batches per training epoch
  N = SINGLESTNOGAN.model['N'] 
  n_epochs = SINGLESTNOGAN.model['train_epochs'] if not train_epochs else train_epochs
  n_batch = SINGLESTNOGAN.model['batch_size'] if not batch_size else batch_size
  all_patience = SINGLESTNOGAN.model['es_patience'] if not patience else patience

  total_train = dataset['buyers_train'][0].shape[0] * 2
  bat_per_epo = (total_train // n_batch)
  # calculate the number of training iterations
  n_steps = bat_per_epo * n_epochs

  X_valid, y_valid, _, _ = dataset['all_valid']
  X_valid_pad = apply_pad(X_valid, N)

  X_valid_buyer, y_valid_buyer = dataset['buyers_valid']

  losses_i_train, losses_c_train, losses_d_train, losses_g_train = [],[],[],[]

  losses_i_ep, losses_c_ep, losses_d_ep, losses_g_ep = [],[],[],[]

  losses_i_ep_val, losses_c_ep_val = [],[]

  best_val_score_sess, last_val_score_sess, last_best_val_score,  best_val_score = (10e3,10e3,10e3,10e3)

  half_batch = int(n_batch//2)

  c_patience = 0
  i_patience = 0

  max_c_patience = 5
  max_i_patience = 5

  c_disabled = False
  i_disabled = False

  i_loss, i_acc, c_loss, c_acc = (0,0,0,0)
  d_loss, g_loss = (0,0)

  test_i_loss = 10e3 
  test_c_loss = 10e3 

  d_loss1_r, d_acc1_r, d_loss1_f, d_acc1_f = (0,0,0,0)

  MAX_STEPS = SINGLESTNOGAN.model['train_epochs']
  
  last_batch = 0
  
  logger.info('\nn_epochs=%d, n_batch=%d, 1/2=%d, b/e=%d, steps=%d\n' % (n_epochs, n_batch, half_batch, bat_per_epo, n_steps))
  # manually enumerate epochs
  for i in range(n_steps):
    c_step = i//bat_per_epo
    if c_step == MAX_STEPS:
      logger.info("STEPS FINISHED!")
      return
    # update trainervised discriminator (c)
    # c_model
    (X_train_class, y_train_class), (X_train_buyers, y_train_buyers),last_batch = get_batches_from_all(dataset, N, n_batch, last_batch)

    if not c_disabled:
      c_loss, c_acc = train_session_classifier(c_model, X_train_class, y_train_class) 
    else:
      c_loss, c_acc = (0,0)
    
    if not i_disabled:
      i_loss, i_acc = train_item_from_buyers(i_model, X_train_buyers, y_train_buyers) 
    else:
      i_loss, i_acc = (0,0)
    # losses update 
    losses_train = losses_c_train,losses_i_train,losses_d_train,losses_g_train
    losses_c_train, losses_i_train, losses_d_train, losses_g_train = append_losses(c_loss, i_loss, d_loss1_r, d_loss1_f,\
      g_loss, losses_train)

    if i % 2 == 0:
      logger.info('>%d, i[%.5f,%.5f, c[%.5f,%.5f]\n,g[%.5f]'\
        % \
            (i+1,\
             i_loss, i_acc, \
             c_loss, c_acc,\
             g_loss))
      logger.info("Current step: " + str(i//bat_per_epo))
    # evaluate the model performance every so often
    if (i) % (bat_per_epo * 1) == 0: # finished epoch
      SINGLESTNOGAN.save_losses_eval(losses_c_ep, losses_i_ep)
      logger.debug("Evaluating model")

      losses_epoch = losses_c_ep,losses_i_ep,losses_d_ep,losses_g_ep
      update_losses_in_epoch(losses_train, losses_epoch) 
      # TODO save gan_losses_i_ep_val
      if i_disabled == False:
        losses_i_ep_val, i_patience, test_i_loss, test_i_acc, best_val_score = evaluate_i_model_nogan(i_model, c_model, N,\
        X_valid_buyer, y_valid_buyer, losses_i_ep_val, best_val_score, i_patience, c_step, all_patience)
      if c_disabled == False:
         losses_c_ep_val, c_patience, test_c_loss, test_c_acc, best_val_score_sess = evaluate_c_model_nogan(i_model, c_model, X_valid_pad, y_valid, \
          losses_c_ep_val, best_val_score_sess, c_patience, c_step, all_patience)

      if c_patience > max_c_patience:
        logger.debug("c_model disabled!")
        c_disabled = True

      if i_patience > max_i_patience:
        logger.debug("i_model disabled!")
        i_disabled = True

      if c_disabled and i_disabled:
        logger.debug("stop!")
        return

      losses_i_train, losses_c_train, losses_d_train, losses_g_train = [], [], [], []
      # summarize_performance(params, i, g_model, si_model, latent_dim,\    
                          # dataset, stats, dataName='valid', n_samples=500000, show_g = False, batch_size=20000)
  return losses_train, losses_epoch  

def truncate_for_last_N(X_pad, N):
  X_new = []
  for i in range(0,len(X_pad)):
    if i % 100 == 0: logger.debug(i/X_pad.shape[0])
    x = X_pad[i]
    if len(x) >= N:
      la = LabelEncoder()
      item_id_arr = x[:,0] 
      x[:,0] = la.fit_transform(item_id_arr)
    X_new.append(x)
  X_new = np.array(X_new)
  return X_new

def evaluate_c_model_test_nogan(dataset, disc_model):
  logger.info("Evaluating for c_model..")
  N=SINGLESTNOGAN.model['N']
  X_test, y_test, _, _ = dataset['all_test']
  
  X_test = apply_pad(X_test, N)
  # X_test = truncate_for_last_N(X_test, N)

  d_model, i_model, c_model = disc_model
  y_test_pred = c_model.predict(np.array(X_test))
  y_class = y_test_pred > 0.5 
  
  report = classification_report(y_test, y_class, target_names=['non_buyer','buyer'])
  
  logger.info("Final report for c_model:\n" + str(report)) 
  
  SINGLESTNOGAN.save_results_c_model_nogan(report) 
  
  get_roc_curve(y_test_pred, y_test, SINGLESTNOGAN.res_path, final_name='c_model_roc_curve_nogan.png') 


def evaluate_i_model_test_nogan(dataset, disc_model):
  logger.info("Evaluating for i_model..")
  N=SINGLESTNOGAN.model['N']
  X_test, y_test = dataset['buyers_test']
  test_sids = dataset['test_buyer_sessions']
  
  X_test = apply_pad(X_test, N)

  d_model, i_model, c_model = disc_model

  y_pred = i_model.predict(X_test)
     
  session_buyer_targets = load_session_buyer_targets() 
  session_original_clicks = load_session_original_clicks()  
  
  final_class_flat, final_y_test, y_test_proba = generate_labels_and_predictions_by_sessions_within_clicks_for_crf(test_sids, y_pred, session_buyer_targets, session_original_clicks)
 
  report = classification_report(final_y_test, final_class_flat, target_names=['non_buyer','buyer'])

  logger.info("Final report for i_model:\n" + str(report)) 
  SINGLESTNOGAN.save_results_i_model_nogan(report) 
  get_roc_curve(y_test_proba, final_y_test, SINGLESTNOGAN.res_path, final_name='i_model_roc_curve_nogan.png') 


def plot_losses():
  losses_d_ep, losses_i_ep, losses_c_ep, losses_g_ep = SINGLESTNOGAN.load_losses()
  losses_i_ep_eval, losses_c_ep_eval = SINGLESTNOGAN.load_losses_eval()

  fig, axs = plt.subplots(2)

  axs[0].plot(losses_c_ep[:len(losses_c_ep_eval)])
  axs[0].plot(losses_c_ep_eval)
 
  axs[0].title.set_text('model losses c_model')
  axs[0].legend(['train', 'val'], loc='upper left')
 
  axs[1].plot(losses_i_ep[:len(losses_i_ep_eval)])
  axs[1].plot(losses_i_ep_eval)

  axs[1].set_title('model losses i_model')
  axs[1].legend(['train', 'val'], loc='upper left')
  
  fig.text(0.5, 0.04, 'Epochs', ha='center', va='center')
  fig.text(0.06, 0.5, 'Losses', ha='center', va='center', rotation='vertical')
  
  fig.tight_layout(h_pad=2)

  fig.savefig(SINGLESTNOGAN.res_path + "losses_models.png")

def train_and_evalute_nogan(only_evaluate=False, train_epochs=None, batch_size=None, patience=None, use_gru=False):
  logger.info("Training single-stage without GAN!")
  _, _, disc_model = build_single(use_gru)

  latent_dim = SINGLESTNOGAN.model['latent_dim'] 
  dataset = construct_dataset()
  
  if only_evaluate == False:
    train_nogan(disc_model, dataset, latent_dim, train_epochs, batch_size, patience)
  
  d_model, i_model, c_model = disc_model 
  
  SINGLESTNOGAN.load_models_with_better_c_model(i_model, c_model) 
  disc_model = d_model, i_model, c_model
  evaluate_c_model_test_nogan(dataset, disc_model)

  SINGLESTNOGAN.load_models_with_better_i_model(i_model, c_model) 
  disc_model = d_model, i_model, c_model
  evaluate_i_model_test_nogan(dataset,  disc_model)
  
  plot_losses()