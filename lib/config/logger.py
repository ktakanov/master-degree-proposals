
import logging
import logging.config
from datetime import datetime
from lib.config.controls.paths import Paths
import sys
from lib.utils import get_current_datetime


def __get_log_level(lognumber):
    level = 0
    if lognumber == 0:
        level=logging.INFO
    if lognumber == 1:
        level= logging.DEBUG
    if lognumber == 2:
        level= logging.WARNING
    if lognumber == 3:
        level= logging.ERROR
    if lognumber == 4:
        level= logging.CRITICAL 
    return level

def set_log_level(loglevel):   
    logger = logging.getLogger('sample_logger')
    logger.setLevel(int(__get_log_level(loglevel)))

def init_logger(loglevel=0):
    format = "[%(filename)s:%(lineno)s - %(funcName)s()]:%(levelname)s %(message)s"
    logging.basicConfig(format=format, level=__get_log_level(int(loglevel)))
    logging.config.dictConfig({'version': 1,'disable_existing_loggers': False})

    logger = logging.getLogger('sample_logger')
    today = get_current_datetime()

    output_file_handler = logging.FileHandler(Paths.log_path + today + ".log")
    stdout_handler = logging.StreamHandler(sys.stdout)

    logger.addHandler(output_file_handler)
    logger.addHandler(stdout_handler)
    logger.propagate = False
    
    return logger

def get_logger():
    logger = logging.getLogger('sample_logger')
    
    if not logger.hasHandlers():        
        logger = init_logger()    
    return logger        