from os import stat
import pickle
import numpy as np
from numpy.lib.function_base import kaiser 
class Paths:
    raw_path = 'assets/raw/'
    processed_path = 'assets/processed/'
    log_path = 'assets/log/'
    log_file = 'assets/log/'
    input_path = 'assets/input/'
    weights_path = "assets/weights/"
    results_path = "results/"

class Construct:
   feats_of_inst = ["session_id", "timestamp", "item_id", "category", "dwelltime", "weekday", "day", "hour", "month", "week", "item_id_prob", "item_rank", "price_dist_avg", "label"]
class Preproces:
    max_seq_trunc = 13
    
    to_scale_features = ['dwelltime','item_rank','price_dist_avg','item_id_prob']
    to_scale_features =set.intersection(set(Construct.feats_of_inst), set(to_scale_features))
class Baseline:
    item_prob_max_session = 25
    
    raw_path = Paths.raw_path
    item_probs_path = raw_path + "item_id_probs.pickle"
    slices_path = Paths.input_path + "baseline/slices/"
    second_stage_path = Paths.input_path + "baseline/second/"
    first_stage_path = Paths.input_path + "baseline/first/"
    first_stage_min_sample_split_param = [100,150,200,250,300,500]
    second_stage_min_sample_split_param = [100,150,200,250,300,500]
    
    fst_stg_weights_path = Paths.weights_path + "baseline/first/"
    snd_stg_weights_path = Paths.weights_path + "baseline/second/"
    
    fst_stg_res_path = Paths.results_path + "baseline/first/"
    snd_stg_res_path = Paths.results_path + "baseline/second/"
        
    @staticmethod
    def save_slice_input_fst_stg(X,y, SIT, name,i):
        np.save(Baseline.slices_path + "X_" + str(name) + "_" + str(i),X)
        np.save(Baseline.slices_path + "y_" + str(name)+ "_" + str(i),y)
        np.save(Baseline.slices_path + "SIT_" + str(name)+ "_" + str(i),SIT)

    @staticmethod
    def save_input_fst_stg(X,y,SIT,name):
        np.save(Baseline.first_stage_path + "X_" + str(name), X)
        np.save(Baseline.first_stage_path + "y_" + str(name),y)
        np.save(Baseline.first_stage_path + "SIT_" + str(name),SIT)

    @staticmethod
    def load_input_fst_stg(name):
        X = np.load(Baseline.first_stage_path + "X_" + str(name) + ".npy")
        y = np.load(Baseline.first_stage_path + "y_" + str(name) + ".npy")
        SIT = np.load(Baseline.first_stage_path + "SIT_" + str(name)  + ".npy")
        return X,y,SIT
    
    @staticmethod
    def load_slice_input_fst_stg(name,i):
        X=np.load(Baseline.slices_path + "X_" + str(name) + "_" + str(i) + ".npy")
        y=np.load(Baseline.slices_path + "y_" + str(name)+ "_" + str(i) + ".npy")
        SIT=np.load(Baseline.slices_path + "SIT_" + str(name)+ "_" + str(i) + ".npy")
        return X,y,SIT
    
    @staticmethod
    def save_input_snd_stg(X,y,name):
        np.save(Baseline.second_stage_path + "X_" + str(name),X)
        np.save(Baseline.second_stage_path + "y_" + str(name),y)

    @staticmethod
    def load_input_snd_stg(name):
        X = np.load(Baseline.second_stage_path + "X_" + str(name) + ".npy",allow_pickle=True)
        y = np.load(Baseline.second_stage_path + "y_" + str(name) + ".npy",allow_pickle=True)
        return X,y
    
    @staticmethod
    def get_item_probs():
        item_id_probs =  pickle.load(open(Baseline.item_probs_path,'rb'))
        return item_id_probs

    @staticmethod
    def save_item_probs(item_probs):
        item_id_probs =  pickle.dump(item_probs, open(Baseline.item_probs_path,'wb'))
        return item_id_probs

    @staticmethod
    def save_model_fst(model):
        filename = Baseline.fst_stg_weights_path + "model.pickle"
        pickle.dump(model, open(filename, 'wb'))
        
    @staticmethod
    def load_model_fst():
        filename = Baseline.fst_stg_weights_path + "model.pickle"
        return pickle.load(open(filename, 'rb'))

    @staticmethod
    def save_model_snd(model):
        filename = Baseline.snd_stg_weights_path + "model.pickle"
        pickle.dump(model, open(filename, 'wb'))
        
    @staticmethod
    def load_model_snd():
        filename = Baseline.snd_stg_weights_path + "model.pickle"
        return pickle.load(open(filename, 'rb'))
    
    @staticmethod
    def save_results_fst(report):
        with open(Baseline.fst_stg_res_path + "report.txt", 'w') as f:
            print(report,file=f)  # Python 3.x

    @staticmethod
    def save_results_snd(report):
        with open(Baseline.snd_stg_res_path + "report.txt", 'w') as f:
            print(report,file=f)  # Python 3.x

class TWOSTGTRANS:
    data_features = ['session_id','item_id','category','day','hour','month','weekday','dwelltime','item_rank','item_id_prob','label'] 
    input_features = data_features[1:len(data_features) - 1]

    fst_res_path = Paths.results_path + "two_stage/first/"
    snd_res_path = Paths.results_path + "two_stage/second/"
    fst_inp_path = Paths.input_path + "two_stage/first/"
    snd_inp_path = Paths.input_path + "two_stage/second/"
    
    model_path = "assets/model/two_stage/"


    model = {
        'train_epochs' : 20,
        'batch_size' : 300,
        'es_patience' : 50,
        'N' : Preproces.max_seq_trunc,
        'F': len(input_features)
    }
       
    @staticmethod
    def save_results_fst(report):
        with open(TWOSTGTRANS.fst_res_path + "report.txt", 'w') as f:
            print(report,file=f)  # Python 3.x

    @staticmethod
    def save_results_snd(report):
        with open(TWOSTGTRANS.snd_res_path + "report.txt", 'w') as f:
            print(report,file=f)  # Python 3.x

    @staticmethod
    def save_predictions_and_labels_fst(y_pred, y_label):
        np.save(TWOSTGTRANS.fst_res_path + "y_pred", y_pred)
        np.save(TWOSTGTRANS.fst_res_path + "y_label", y_label)

    @staticmethod
    def save_predictions_and_labels_snd(y_pred, y_label):
        np.save(TWOSTGTRANS.snd_res_path + "y_pred", y_pred)
        np.save(TWOSTGTRANS.snd_res_path + "y_label", y_label)

    @staticmethod
    def load_predictions_and_labels_fst():
        y_pred = np.load(TWOSTGTRANS.fst_res_path + "y_pred.npy")
        y_label = np.save(TWOSTGTRANS.fst_res_path + "y_label.npy")
        
        return (y_pred, y_label)       
    @staticmethod
    def load_predictions_and_labels_snd():
        y_pred = np.load(TWOSTGTRANS.snd_res_path + "y_pred.npy")
        y_label = np.save(TWOSTGTRANS.snd_res_path + "y_label.npy")
        
        return (y_pred, y_label)

    @staticmethod
    def get_model_checkpoint_path(modeltype):  #TODO: refactor 
        model_folder = 'first' if modeltype == 'first' else 'second'
        return TWOSTGTRANS.model_path + model_folder + "/checkpoint/"

    @staticmethod
    def get_model_history_path(modeltype):  #TODO: refactor 
        model_folder = 'first' if modeltype == 'first' else 'second'
        return Paths.results_path + "two_stage/" + model_folder + "/history/"

    @staticmethod
    def load_model_weights_fst(model):
        checkpoint_path = TWOSTGTRANS.get_model_checkpoint_path('first') 
        model.load_weights(checkpoint_path + "best_val_weights.h5") 
        return model

    @staticmethod
    def load_model_weights_snd(model):
        checkpoint_path = TWOSTGTRANS.get_model_checkpoint_path('second') 
        model.load_weights(checkpoint_path + "best_val_weights.h5") 
        return model

    @staticmethod
    def save_input_snd_stg(X,y,SIT,name):
        np.save(TWOSTGTRANS.snd_inp_path + "X_" + str(name), X)
        np.save(TWOSTGTRANS.snd_inp_path + "y_" + str(name), y)
        np.save(TWOSTGTRANS.snd_inp_path + "SIT_" + str(name), SIT)

    @staticmethod
    def load_input_snd_stg(name):
        X = np.load(TWOSTGTRANS.snd_inp_path + "X_" + str(name)+".npy", allow_pickle=True)
        y = np.load(TWOSTGTRANS.snd_inp_path + "y_" + str(name)+".npy", allow_pickle=True)
        SIT = np.load(TWOSTGTRANS.snd_inp_path + "SIT_" + str(name)+".npy", allow_pickle=True)
        return X,y, SIT
    
    @staticmethod
    def save_input_fst_stg(X,y,SIT,name):
        np.save(TWOSTGTRANS.fst_inp_path + "X_" + str(name), X)
        np.save(TWOSTGTRANS.fst_inp_path + "y_" + str(name), y)
        np.save(TWOSTGTRANS.fst_inp_path + "SIT_" + str(name), SIT)

    @staticmethod
    def load_input_fst_stg(name):
        X = np.load(TWOSTGTRANS.fst_inp_path + "X_" + str(name)+".npy",allow_pickle=True)
        y = np.load(TWOSTGTRANS.fst_inp_path + "y_" + str(name)+".npy",allow_pickle=True)
        SIT = np.load(TWOSTGTRANS.fst_inp_path + "SIT_" + str(name)+".npy",allow_pickle=True)

        return X,y, SIT
class SINGLESTGAN:
    data_features = ['session_id','item_id','category','day','hour','month','weekday','dwelltime','item_rank','item_id_prob','label'] 
    input_features = data_features[1:len(data_features) - 1]
    
    res_path = Paths.results_path + "single_stage/"
    inp_path = Paths.input_path + "single_stage/"
        
    model_path = "assets/model/single_stage/"
    model_weights_path = "assets/model/single_stage/weights/"
    model_losses_path = "assets/model/single_stage/losses/"

    model = {
        'latent_dim' : 250,
        'train_epochs' : 60,
        'batch_size' : 2000,
        'es_patience' : 50,
        'N' : Preproces.max_seq_trunc,
        'F': len(input_features)
    }

    @staticmethod
    def save_predictions_and_labels_c_model(y_pred, y_label):
        np.save(SINGLESTGAN.res_path + "c_model_y_pred", y_pred)
        np.save(SINGLESTGAN.res_path + "c_model_y_label", y_label)

    @staticmethod
    def save_predictions_and_labels_i_model(y_pred, y_label):
        np.save(SINGLESTGAN.res_path + "i_model_y_pred", y_pred)
        np.save(SINGLESTGAN.res_path + "i_model_y_label", y_label)
           
    @staticmethod
    def save_results_c_model(report):
        with open(SINGLESTGAN.res_path + "c_model_report.txt", 'w') as f:
            f.write(report)
            f.close()

    @staticmethod
    def save_results_i_model(report):
        with open(TWOSTGTRANS.fst_res_path + "i_model_report.txt", 'w') as f:
            f.write(report)
            f.close()
            
           
    @staticmethod
    def save_results_c_model_nogan(report):
        with open(SINGLESTGAN.res_path + "c_model_report_nogan.txt", 'w') as f:
            f.write(report)
            f.close()

    @staticmethod
    def save_results_i_model_nogan(report):
        with open(TWOSTGTRANS.fst_res_path + "i_model_report_nogan.txt", 'w') as f:
            f.write(report)
            f.close()            

    @staticmethod
    def save_losses(losses_c_ep, losses_i_ep, losses_d_ep, losses_g_ep):
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_i_ep",losses_i_ep)
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_c_ep",losses_c_ep)
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_d_ep",losses_d_ep)
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_g_ep",losses_g_ep)

    @staticmethod 
    def save_models_with_better_i_model(i_model, c_model):
        i_model.save_weights(SINGLESTGAN.model_weights_path + "best_i_model.h5")
        c_model.save_weights(SINGLESTGAN.model_weights_path + "best_c_model_with_better_i.h5")

    @staticmethod 
    def load_models_with_better_i_model(i_model, c_model):
        i_model.load_weights(SINGLESTGAN.model_weights_path + "best_i_model.h5")
        c_model.load_weights(SINGLESTGAN.model_weights_path + "best_c_model_with_better_i.h5")
        return i_model, c_model

    @staticmethod 
    def save_models_with_better_c_model(i_model, c_model):
        c_model.save_weights(SINGLESTGAN.model_weights_path + "best_c_model.h5")
        i_model.save_weights(SINGLESTGAN.model_weights_path + "best_i_model_with_better_c.h5")

    @staticmethod 
    def load_models_with_better_c_model(i_model, c_model):
        c_model.load_weights(SINGLESTGAN.model_weights_path + "best_c_model.h5")
        i_model.load_weights(SINGLESTGAN.model_weights_path + "best_i_model_with_better_c.h5")
        return i_model, c_model

    @staticmethod 
    def save_models(g_model, d_model, i_model, c_model,train=False):
        train_b = '_train' if train else ''
        i_model.save_weights(SINGLESTGAN.model_weights_path + "best_i_model%s.h5" % (train_b))
        c_model.save_weights(SINGLESTGAN.model_weights_path + "best_c_model%s.h5" % (train_b))
        g_model.save_weights(SINGLESTGAN.model_weights_path + "best_gen_model%s.h5" % (train_b))
        d_model.save_weights(SINGLESTGAN.model_weights_path + "best_d_model%s.h5"  % (train_b))

    @staticmethod
    def save_input(X, y_class, y_items, SIDS, name):
        np.save(SINGLESTGAN.inp_path + "X_" + str(name), X)
        np.save(SINGLESTGAN.inp_path + "y_class_" + str(name), y_class)
        np.save(SINGLESTGAN.inp_path + "y_items_" + str(name), y_items)
        np.save(SINGLESTGAN.inp_path + "SIT_" + str(name), SIDS)

    @staticmethod
    def load_input(name):
        X = np.load(SINGLESTGAN.inp_path + "X_" + str(name) + ".npy", allow_pickle=True)
        y_class = np.load(SINGLESTGAN.inp_path + "y_class_" + str(name) + ".npy", allow_pickle=True)
        y_items = np.load(SINGLESTGAN.inp_path + "y_items_" + str(name) + ".npy", allow_pickle=True)
        SIDS = np.load(SINGLESTGAN.inp_path + "SIT_" + str(name) + ".npy", allow_pickle=True)
      
        return X, y_class, y_items, SIDS
    
class SINGLESTNOGAN(SINGLESTGAN):
    
    model = {
        'latent_dim' : 250,
        'train_epochs' : 150,
        'batch_size' : 6000,
        'es_patience' : 50,
        'N' : Preproces.max_seq_trunc,
        'F': len(SINGLESTGAN.input_features)
    }
