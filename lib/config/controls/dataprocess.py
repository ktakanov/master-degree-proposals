class Construct:
   feats_of_inst = ["session_id", "timestamp", "item_id",\
      "category", "dwelltime", "weekday", "day", "hour", "month", "week", "item_id_prob", "item_qty_rank","item_clicks_rank", 'price_dist_avg', "label"]
   cat_cols = ['item_id','category','day','hour','month','weekday','week']
   num_cols = ['dwelltime','item_qty_rank','item_id_prob','price_dist_avg']
class Preproces:
    max_seq_trunc = 13
    feats_all = ['session_id', 'timestamp', 'item_id','category','day','hour','month','weekday','dwelltime',"item_qty_rank","item_clicks_rank",'price_dist_avg','item_id_prob', 'label'] 
    feats_of_inst = ['session_id', 'item_id','category','day','hour','month','weekday','dwelltime',"item_qty_rank","item_clicks_rank",'price_dist_avg','item_id_prob', 'label'] 
    
    to_scale_features =  ['day','hour','month','weekday']+ ['dwelltime',"item_qty_rank","item_clicks_rank",'item_id_prob','price_dist_avg']
    to_scale_features =set.intersection(set(feats_of_inst), set(to_scale_features))