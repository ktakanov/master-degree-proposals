import numpy as np
from scipy import sparse
from lib.third_party.keras_crf import CRF
from lib.models.classes.transformer import *
from tensorflow.keras.models import model_from_json
from lib.config.controls.paths import Paths
from lib.config.controls.dataprocess import Preproces
from lib.utils import get_current_datetime, change_last_folder_path
from tensorflow.keras.models import load_model
from tensorflow.keras.layers import Embedding
import os
from lib.config.controls.dataprocess import Preproces

class SINGLESTGAN:
    
    use_time_as_numerical=True
    data_features = Preproces.feats_of_inst
    input_features = data_features[1:len(data_features) - 1] 
    
    cat_features = input_features[:6] if use_time_as_numerical == False else input_features.copy()[:2]
    num_features = input_features[len(cat_features):] if use_time_as_numerical == False  else  input_features.copy()[2:]
   
    
    res_path = Paths.results_path + "single_stage/"
    inp_path = Paths.input_path + "single_stage/"
    inp_path_sub_train = Paths.input_path + "single_stage/train/"
    inp_path_sub_test = Paths.input_path + "single_stage/test/"
        
    model_weights_path = "assets/model/single_stage/weights/"
    model_losses_path = "assets/model/single_stage/losses/"


    model = {
        'latent_dim' : 100,
        'train_epochs' : 1000,
        'batch_size' : 13*4,
        'es_patience' : 100,
        'N' : Preproces.max_seq_trunc,
        'F': len(input_features)
    }

    @classmethod
    def get_models_weights_path(cls):
        return cls.model_weights_path

    @classmethod
    def generate_last_path(cls, new_path, new_label=''):
        cls.last_path = new_path if new_label == '' else new_path + "_" + new_label + "/"
        return cls.last_path

    @classmethod
    def get_last_path(cls):
        return cls.last_path

    @classmethod
    def updatePaths(cls, new_path, new_label=''):
        if new_path  == None or new_path == False:
            return
        new_path = cls.generate_last_path(new_path, new_label) 
        cls.res_path = cls.res_path + new_path
        cls.model_weights_path = cls.model_weights_path + new_path
        cls.model_losses_path = cls.model_losses_path + new_path

    @classmethod
    def constructFolderPaths(cls):
        os.makedirs(cls.res_path, exist_ok=True) 
        os.makedirs(cls.inp_path_sub_train, exist_ok=True) 
        os.makedirs(cls.inp_path_sub_test, exist_ok=True) 
        os.makedirs(cls.model_weights_path, exist_ok=True) 
        os.makedirs(cls.model_losses_path, exist_ok=True)

    @classmethod
    def constructPaths(cls):
        cls.res_path += get_current_datetime() +  '/'
        cls.model_weights_path += get_current_datetime() + '/'
        cls.model_losses_path += get_current_datetime() + '/'
 

    @staticmethod
    def save_predictions_and_labels_c_model(y_pred, y_label):
        np.save(SINGLESTGAN.res_path + "c_model_y_pred", y_pred)
        np.save(SINGLESTGAN.res_path + "c_model_y_label", y_label)

    @staticmethod
    def save_predictions_and_labels_i_model(y_pred, y_label):
        np.save(SINGLESTGAN.res_path + "i_model_y_pred", y_pred)
        np.save(SINGLESTGAN.res_path + "i_model_y_label", y_label)

    @staticmethod
    def save_args(args):
        with open(SINGLESTGAN.res_path + "_args.txt", 'w') as f:
            f.write(str(args))
            f.close()

    @staticmethod
    def show_args():
        with open(SINGLESTGAN.res_path + "_args.txt", 'r') as f:
             data = f.read()
             return data

    @staticmethod
    def save_models_summary(d_model, i_model, c_model, g_model):
        with open(SINGLESTGAN.res_path + "models_summary.txt", 'w') as f:
            f.write("--------------------------\n")
            f.write( "Discriminator model\n")
            d_model.summary(print_fn=lambda x: f.write(x + '\n'))
            f.write("--------------------------\n")
            f.write("Customer classifier model\n")
            c_model.summary(print_fn=lambda x: f.write(x + '\n'))
            f.write("--------------------------\n")
            f.write("Item predictor model\n")
            i_model.summary(print_fn=lambda x: f.write(x + '\n'))
            f.write("--------------------------\n")
            f.write("Generator model\n")
            g_model.summary(print_fn=lambda x: f.write(x + '\n'))
            f.close()
           
    @staticmethod
    def save_results_c_model(report):
        with open(SINGLESTGAN.res_path + "c_model_report.txt", 'w') as f:
            f.write(report)
            f.close()

    @staticmethod
    def save_results_i_model(report):
        with open(SINGLESTGAN.res_path + "i_model_report.txt", 'w') as f:
            f.write(report)
            f.close()
           
    @staticmethod
    def save_results_c_model_nogan(report):
        with open(SINGLESTGAN.res_path + "c_model_report.txt", 'w') as f:
            f.write(report)
            f.close()

    @staticmethod
    def save_results_i_model_nogan(report):
        with open(SINGLESTGAN.res_path + "i_model_report.txt", 'w') as f:
            f.write(report)
            f.close()            

    @staticmethod
    def load_losses_train(last_folder):
        path = SINGLESTGAN.model_losses_path

        losses_i_ep = np.load(path + "gan_losses_i_ep.npy")
        losses_c_ep = np.load(path + "gan_losses_c_ep.npy")
        losses_d_ep = np.load(path + "gan_losses_d_ep.npy")
        losses_g_ep = np.load(path + "gan_losses_g_ep.npy")
        
        return losses_d_ep, losses_i_ep, losses_c_ep, losses_g_ep

    @staticmethod
    def save_losses_train(losses_c_ep, losses_i_ep, losses_d_ep, losses_g_ep):
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_i_ep",losses_i_ep)
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_c_ep",losses_c_ep)
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_d_ep",losses_d_ep)
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_g_ep",losses_g_ep)

    @staticmethod
    def load_losses_eval(last_folder=None):
        path = SINGLESTGAN.model_losses_path

        losses_i_ep = np.load(path + "gan_losses_i_ep_eval.py.npy")
        losses_c_ep = np.load(path + "gan_losses_c_ep_eval.py.npy")
        losses_i_auc = np.load(path + "gan_i_auc_eval.py.npy")
        losses_c_auc = np.load(path + "gan_c_auc_eval.py.npy")
        
        return losses_i_ep, losses_c_ep, losses_i_auc, losses_c_auc

    @staticmethod
    def save_losses_eval(losses_c_ep, losses_i_ep, losses_c_auc_val, losses_i_auc_val):
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_i_ep_eval.py",losses_i_ep)
        np.save(SINGLESTGAN.model_losses_path + "gan_losses_c_ep_eval.py",losses_c_ep)
        np.save(SINGLESTGAN.model_losses_path + "gan_c_auc_eval.py",losses_c_auc_val)
        np.save(SINGLESTGAN.model_losses_path + "gan_i_auc_eval.py",losses_i_auc_val)

    @staticmethod 
    def save_models_i_model(i_model, best_score):
        i_model.save_weights(SINGLESTGAN.model_weights_path + "best_i_model.h5")
        with open(SINGLESTGAN.res_path + "best_i_model_score_in_validation.txt", 'w') as f:
            f.write(str(best_score))
            f.close()            

    @staticmethod 
    def load_model_i_model(i_model, last_folder=None):
        path = SINGLESTGAN.model_weights_path

        i_model.load_weights(path + "best_i_model.h5")
        return i_model

    @staticmethod 
    def save_models_c_model(c_model, best_score):
        c_model.save_weights(SINGLESTGAN.model_weights_path + "best_c_model.h5")
        with open(SINGLESTGAN.res_path + "best_c_model_score_in_validation.txt", 'w') as f:
            f.write(str(best_score))
            f.close()            

    @staticmethod 
    def load_model_c_model(c_model, last_folder=None):
        path = SINGLESTGAN.model_weights_path
        c_model.load_weights(path + "best_c_model.h5")
        return c_model

    @staticmethod 
    def save_models(g_model, d_model, i_model, c_model):
        i_model.save(SINGLESTGAN.model_weights_path + "i_model")
        c_model.save(SINGLESTGAN.model_weights_path + "c_model")
        g_model.save(SINGLESTGAN.model_weights_path + "gen_model")
        d_model.save(SINGLESTGAN.model_weights_path + "d_model" )

    @staticmethod 
    def load_models():
        path = SINGLESTGAN.model_weights_path

        custom_objs = {'TokenAndPositionEmbedding': TokenAndPositionEmbedding,
        'NumericalPositionEmbedding': NumericalPositionEmbedding,
        'TransformerBlock': TransformerBlock,
        'MultiHeadSelfAttention': MultiHeadSelfAttention,
        'Embedding': Embedding}

        c_model = load_model(path + "c_model", custom_objects = custom_objs)
        g_model = load_model(path + "gen_model", custom_objects = custom_objs)
        d_model = load_model(path + "d_model", custom_objects = custom_objs)
        i_model = load_model(path + "i_model", custom_objects = \
        dict(custom_objs, **{'CRF': CRF, 'crf_loss': CRF.loss, 'viterbi_accuracy': CRF.accuracy})) #TODO: fix this
        
        return (g_model, d_model, i_model, c_model)
    

    @staticmethod
    def save_input_sparse(X, y_class, y_items, SIDS, name):
        sparse.save_npz(SINGLESTGAN.inp_path + "X_sparse_" + str(name), X)
        sparse.save_npz(SINGLESTGAN.inp_path + "y_class_sparse_" + str(name), y_class)
        sparse.save_npz(SINGLESTGAN.inp_path + "y_items_sparse_" + str(name), y_items)
        sparse.save_npz(SINGLESTGAN.inp_path + "SIT_sparse_" + str(name), SIDS)    
        
    @staticmethod
    def load_input_sparse(name):
        X = sparse.load_npz(SINGLESTGAN.inp_path + "X_sparse_" + str(name) + ".npz")
        y_class = sparse.load_npz(SINGLESTGAN.inp_path + "y_class_sparse_" + str(name) + ".npz")
        y_items = sparse.load_npz(SINGLESTGAN.inp_path + "y_items_sparse" + str(name) + ".npz")
        SIDS = sparse.load_npz(SINGLESTGAN.inp_path + "SIT_sparse_" + str(name) + ".npz")
      
        return X, y_class, y_items, SIDS

    @staticmethod
    def save_input(X, y_class, y_items, SIDS, len_inp, name):
        np.save(SINGLESTGAN.inp_path + name + "/" + "X_" + str(name), np.array(X,dtype='object'))
        np.save(SINGLESTGAN.inp_path +  name + "/" + "y_class_" + str(name), y_class)
        np.save(SINGLESTGAN.inp_path +  name + "/" + "y_items_" + str(name), y_items)
        np.save(SINGLESTGAN.inp_path +  name + "/" + "SIT_" + str(name), SIDS)
        np.save(SINGLESTGAN.inp_path +  name + "/" + "len_inp" + str(name), len_inp)


    @staticmethod
    def save_input_for_k(X, y_class, y_items, SIDS, len_inp, name, k):
        np.save(SINGLESTGAN.inp_path + name + "/" + "X_" + str(name) + str(k), np.array(X,dtype='object'))
        np.save(SINGLESTGAN.inp_path +  name + "/" + "y_class_" + str(name) + str(k), y_class)
        np.save(SINGLESTGAN.inp_path +  name + "/" + "y_items_" + str(name) + str(k), y_items)
        np.save(SINGLESTGAN.inp_path +  name + "/" + "SIT_" + str(name) + str(k), SIDS)
        np.save(SINGLESTGAN.inp_path +  name + "/" + "len_inp" + str(name) + str(k), len_inp)


    @staticmethod
    def load_input(name):
        X = np.load(SINGLESTGAN.inp_path + "X_" + str(name) + ".npy", allow_pickle=True)
        y_class = np.load(SINGLESTGAN.inp_path + "y_class_" + str(name) + ".npy", allow_pickle=True)
        y_items = np.load(SINGLESTGAN.inp_path + "y_items_" + str(name) + ".npy", allow_pickle=True)
        SIDS = np.load(SINGLESTGAN.inp_path + "SIT_" + str(name) + ".npy", allow_pickle=True)
        len_inp = np.load(SINGLESTGAN.inp_path + "len_inp" + str(name) + ".npy", allow_pickle=True)
      
        return X, y_class, y_items, SIDS, len_inp