import numpy as np
from lib.config.controls.paths import Paths
from lib.config.controls.dataprocess import Preproces

class TWOSTGTRANS:
    data_features = Preproces.feats_of_inst
    cat_index_feats = [0,1,2,3,4,5] 
    num_index_feats = [6,7,8,9] 
    input_features = data_features[1:len(data_features) - 1]

    fst_res_path = Paths.results_path + "two_stage/first/"
    snd_res_path = Paths.results_path + "two_stage/second/"
    fst_inp_path = Paths.input_path + "two_stage/first/"
    snd_inp_path = Paths.input_path + "two_stage/second/"
    
    model_path = "assets/model/two_stage/"

    model = {
        'train_epochs' : 50,
        'batch_size' : 64,
        'es_patience' : 50,
        'N' : Preproces.max_seq_trunc,
        'F': len(input_features)
    }
       
    @staticmethod
    def save_results_fst(report):
        with open(TWOSTGTRANS.fst_res_path + "report.txt", 'w') as f:
            print(report,file=f)  # Python 3.x

    @staticmethod
    def save_results_snd(report):
        with open(TWOSTGTRANS.snd_res_path + "report.txt", 'w') as f:
            print(report,file=f)  # Python 3.x

    @staticmethod
    def save_predictions_and_labels_fst(y_pred, y_label):
        np.save(TWOSTGTRANS.fst_res_path + "y_pred", y_pred)
        np.save(TWOSTGTRANS.fst_res_path + "y_label", y_label)

    @staticmethod
    def save_predictions_and_labels_snd(y_pred, y_label):
        np.save(TWOSTGTRANS.snd_res_path + "y_pred", y_pred)
        np.save(TWOSTGTRANS.snd_res_path + "y_label", y_label)

    @staticmethod
    def load_predictions_and_labels_fst():
        y_pred = np.load(TWOSTGTRANS.fst_res_path + "y_pred.npy")
        y_label = np.save(TWOSTGTRANS.fst_res_path + "y_label.npy")
        
        return (y_pred, y_label)       
    @staticmethod
    def load_predictions_and_labels_snd():
        y_pred = np.load(TWOSTGTRANS.snd_res_path + "y_pred.npy")
        y_label = np.save(TWOSTGTRANS.snd_res_path + "y_label.npy")
        
        return (y_pred, y_label)

    @staticmethod
    def get_model_checkpoint_path(modeltype):  #TODO: refactor 
        model_folder = 'first' if modeltype == 'first' else 'second'
        return TWOSTGTRANS.model_path + model_folder + "/checkpoint/"

    @staticmethod
    def get_model_history_path(modeltype):  #TODO: refactor 
        model_folder = 'first' if modeltype == 'first' else 'second'
        return Paths.results_path + "two_stage/" + model_folder + "/history/"

    @staticmethod
    def load_model_weights_fst(model):
        checkpoint_path = TWOSTGTRANS.get_model_checkpoint_path('first') 
        model.load_weights(checkpoint_path + "best_val_weights.h5") 
        return model

    @staticmethod
    def load_model_weights_snd(model):
        checkpoint_path = TWOSTGTRANS.get_model_checkpoint_path('second') 
        model.load_weights(checkpoint_path + "best_val_weights.h5") 
        return model

    @staticmethod
    def save_input_snd_stg(X,y,SIT,name):
        np.save(TWOSTGTRANS.snd_inp_path + "X_" + str(name), X)
        np.save(TWOSTGTRANS.snd_inp_path + "y_" + str(name), y)
        np.save(TWOSTGTRANS.snd_inp_path + "SIT_" + str(name), SIT)

    @staticmethod
    def load_input_snd_stg(name):
        X = np.load(TWOSTGTRANS.snd_inp_path + "X_" + str(name)+".npy", allow_pickle=True)
        y = np.load(TWOSTGTRANS.snd_inp_path + "y_" + str(name)+".npy", allow_pickle=True)
        SIT = np.load(TWOSTGTRANS.snd_inp_path + "SIT_" + str(name)+".npy", allow_pickle=True)
        return X,y, SIT
    
    @staticmethod
    def save_input_fst_stg(X,y,SIT,name):
        np.save(TWOSTGTRANS.fst_inp_path + "X_" + str(name), X)
        np.save(TWOSTGTRANS.fst_inp_path + "y_" + str(name), y)
        np.save(TWOSTGTRANS.fst_inp_path + "SIT_" + str(name), SIT)

    @staticmethod
    def load_input_fst_stg(name):
        X = np.load(TWOSTGTRANS.fst_inp_path + "X_" + str(name)+".npy",allow_pickle=True)
        y = np.load(TWOSTGTRANS.fst_inp_path + "y_" + str(name)+".npy",allow_pickle=True)
        SIT = np.load(TWOSTGTRANS.fst_inp_path + "SIT_" + str(name)+".npy",allow_pickle=True)

        return X,y, SIT