import os

class Paths:
    raw_path = 'assets/raw/'
    processed_path = 'assets/processed/'
    scalers_path = 'assets/processed/scalers/'
    log_path = 'assets/log/'
    log_file = 'assets/log/'
    input_path = 'assets/input/'
    weights_path = "assets/weights/"
    results_path = "results/"

    @classmethod
    def constructFolderPaths(cls):
        d = vars(cls)
        paths = d.keys()

        for var in paths:
            if 'path' in var:
                path = d[var]
                os.makedirs(path, exist_ok=True) 

Paths.constructFolderPaths()