from lib.config.controls.paths import Paths
import numpy as np
import pickle

class Baseline:
    item_prob_max_session = 25
    
    raw_path = Paths.raw_path
    item_probs_path = raw_path + "item_id_probs.pickle"
    slices_path = Paths.input_path + "baseline/slices/"
    second_stage_path = Paths.input_path + "baseline/second/"
    first_stage_path = Paths.input_path + "baseline/first/"
    first_stage_min_sample_split_param = [100,150,200,250,300,500]
    second_stage_min_sample_split_param = [100,150,200,250,300,500]
    
    fst_stg_weights_path = Paths.weights_path + "baseline/first/"
    snd_stg_weights_path = Paths.weights_path + "baseline/second/"
    
    fst_stg_res_path = Paths.results_path + "baseline/first/"
    snd_stg_res_path = Paths.results_path + "baseline/second/"
        
    @staticmethod
    def save_slice_input_fst_stg(X,y, SIT, name,i):
        np.save(Baseline.slices_path + "X_" + str(name) + "_" + str(i),X)
        np.save(Baseline.slices_path + "y_" + str(name)+ "_" + str(i),y)
        np.save(Baseline.slices_path + "SIT_" + str(name)+ "_" + str(i),SIT)

    @staticmethod
    def save_input_fst_stg(X,y,SIT,name):
        np.save(Baseline.first_stage_path + "X_" + str(name), X)
        np.save(Baseline.first_stage_path + "y_" + str(name),y)
        np.save(Baseline.first_stage_path + "SIT_" + str(name),SIT)

    @staticmethod
    def load_input_fst_stg(name):
        X = np.load(Baseline.first_stage_path + "X_" + str(name) + ".npy")
        y = np.load(Baseline.first_stage_path + "y_" + str(name) + ".npy")
        SIT = np.load(Baseline.first_stage_path + "SIT_" + str(name)  + ".npy")
        return X,y,SIT
    
    @staticmethod
    def load_slice_input_fst_stg(name,i):
        X=np.load(Baseline.slices_path + "X_" + str(name) + "_" + str(i) + ".npy")
        y=np.load(Baseline.slices_path + "y_" + str(name)+ "_" + str(i) + ".npy")
        SIT=np.load(Baseline.slices_path + "SIT_" + str(name)+ "_" + str(i) + ".npy")
        return X,y,SIT
    
    @staticmethod
    def save_input_snd_stg(X,y,name):
        np.save(Baseline.second_stage_path + "X_" + str(name),X)
        np.save(Baseline.second_stage_path + "y_" + str(name),y)

    @staticmethod
    def load_input_snd_stg(name):
        X = np.load(Baseline.second_stage_path + "X_" + str(name) + ".npy",allow_pickle=True)
        y = np.load(Baseline.second_stage_path + "y_" + str(name) + ".npy",allow_pickle=True)
        return X,y
    
    @staticmethod
    def get_item_probs():
        item_id_probs =  pickle.load(open(Baseline.item_probs_path,'rb'))
        return item_id_probs

    @staticmethod
    def save_item_probs(item_probs):
        item_id_probs =  pickle.dump(item_probs, open(Baseline.item_probs_path,'wb'))
        return item_id_probs

    @staticmethod
    def save_model_fst(model):
        filename = Baseline.fst_stg_weights_path + "model.pickle"
        pickle.dump(model, open(filename, 'wb'))
        
    @staticmethod
    def load_model_fst():
        filename = Baseline.fst_stg_weights_path + "model.pickle"
        return pickle.load(open(filename, 'rb'))

    @staticmethod
    def save_model_snd(model):
        filename = Baseline.snd_stg_weights_path + "model.pickle"
        pickle.dump(model, open(filename, 'wb'))
        
    @staticmethod
    def load_model_snd():
        filename = Baseline.snd_stg_weights_path + "model.pickle"
        return pickle.load(open(filename, 'rb'))
    
    @staticmethod
    def save_results_fst(report):
        with open(Baseline.fst_stg_res_path + "report.txt", 'w') as f:
            print(report,file=f)  # Python 3.x

    @staticmethod
    def save_results_snd(report):
        with open(Baseline.snd_stg_res_path + "report.txt", 'w') as f:
            print(report,file=f)  # Python 3.x