from lib.config.controls.singlestgan import SINGLESTGAN
from os import stat
import numpy as np
from numpy.lib.function_base import kaiser 
from lib.config.controls.dataprocess import Preproces
from lib.config.controls.singlestgan import SINGLESTGAN
from lib.config.controls.paths import Paths
    
class SINGLESTNOGAN():
    
    res_path = Paths.results_path + "single_stage_nogan/"
    inp_path = Paths.input_path + "single_stage_nogan/"
        
    model_path = "assets/model/single_stage_nogan/"
    model_weights_path = "assets/model/single_stage_nogan/weights/"
    model_losses_path = "assets/model/single_stage_nogan/losses/"

    
    model = {
        'latent_dim' : 250,
        'train_epochs' : 150,
        'batch_size' : 6000,
        'es_patience' : 50,
        'N' : Preproces.max_seq_trunc,
        'F': len(SINGLESTGAN.input_features)
    }

    @staticmethod
    def save_predictions_and_labels_c_model(y_pred, y_label):
        np.save(SINGLESTNOGAN.res_path + "c_model_y_pred", y_pred)
        np.save(SINGLESTNOGAN.res_path + "c_model_y_label", y_label)

    @staticmethod
    def save_predictions_and_labels_i_model(y_pred, y_label):
        np.save(SINGLESTNOGAN.res_path + "i_model_y_pred", y_pred)
        np.save(SINGLESTNOGAN.res_path + "i_model_y_label", y_label)
           
    @staticmethod
    def save_results_c_model(report):
        with open(SINGLESTNOGAN.res_path + "c_model_report.txt", 'w') as f:
            f.write(report)
            f.close()

    @staticmethod
    def save_results_i_model(report):
        with open(SINGLESTNOGAN.res_path + "i_model_report.txt", 'w') as f:
            f.write(report)
            f.close()
            
           
    @staticmethod
    def save_results_c_model_nogan(report):
        with open(SINGLESTNOGAN.res_path + "c_model_report_nogan.txt", 'w') as f:
            f.write(report)
            f.close()

    @staticmethod
    def save_results_i_model_nogan(report):
        with open(SINGLESTNOGAN.res_path + "i_model_report_nogan.txt", 'w') as f:
            f.write(report)
            f.close()            

    @staticmethod
    def save_losses(losses_c_ep, losses_i_ep, losses_d_ep, losses_g_ep):
        np.save(SINGLESTNOGAN.model_losses_path + "gan_losses_i_ep",losses_i_ep)
        np.save(SINGLESTNOGAN.model_losses_path + "gan_losses_c_ep",losses_c_ep)
        np.save(SINGLESTNOGAN.model_losses_path + "gan_losses_d_ep",losses_d_ep)
        np.save(SINGLESTNOGAN.model_losses_path + "gan_losses_g_ep",losses_g_ep)
        
    @staticmethod
    def load_losses():
        i = np.load(SINGLESTNOGAN.model_losses_path + "gan_losses_i_ep.npy")
        c = np.load(SINGLESTNOGAN.model_losses_path + "gan_losses_c_ep.npy")
        d = np.load(SINGLESTNOGAN.model_losses_path + "gan_losses_d_ep.npy")
        g = np.load(SINGLESTNOGAN.model_losses_path + "gan_losses_g_ep.npy")
        return i,c,d,g

    @staticmethod
    def load_losses_eval():
        losses_i_ep = np.load(SINGLESTNOGAN.model_losses_path + "nogan_losses_i_ep_eval.py.npy")
        losses_c_ep = np.load(SINGLESTNOGAN.model_losses_path + "nogan_losses_c_ep_eval.py.npy")
        return losses_i_ep, losses_c_ep

    @staticmethod
    def save_losses_eval(losses_c_ep, losses_i_ep):
        np.save(SINGLESTNOGAN.model_losses_path + "nogan_losses_i_ep_eval.py",losses_i_ep)
        np.save(SINGLESTNOGAN.model_losses_path + "nogan_losses_c_ep_eval.py",losses_c_ep)

    @staticmethod 
    def save_models_with_better_i_model(i_model, c_model):
        i_model.save_weights(SINGLESTNOGAN.model_weights_path + "best_i_model.h5")
        c_model.save_weights(SINGLESTNOGAN.model_weights_path + "best_c_model_with_better_i.h5")

    @staticmethod 
    def load_models_with_better_i_model(i_model, c_model):
        i_model.load_weights(SINGLESTNOGAN.model_weights_path + "best_i_model.h5")
        c_model.load_weights(SINGLESTNOGAN.model_weights_path + "best_c_model_with_better_i.h5")
        return i_model, c_model

    @staticmethod 
    def save_models_with_better_c_model(i_model, c_model):
        c_model.save_weights(SINGLESTNOGAN.model_weights_path + "best_c_model.h5")
        i_model.save_weights(SINGLESTNOGAN.model_weights_path + "best_i_model_with_better_c.h5")

    @staticmethod 
    def load_models_with_better_c_model(i_model, c_model):
        c_model.load_weights(SINGLESTNOGAN.model_weights_path + "best_c_model.h5")
        i_model.load_weights(SINGLESTNOGAN.model_weights_path + "best_i_model_with_better_c.h5")
        return i_model, c_model

    @staticmethod 
    def save_models(g_model, d_model, i_model, c_model,train=False):
        train_b = '_train' if train else ''
        i_model.save_weights(SINGLESTNOGAN.model_weights_path + "best_i_model%s.h5" % (train_b))
        c_model.save_weights(SINGLESTNOGAN.model_weights_path + "best_c_model%s.h5" % (train_b))
        g_model.save_weights(SINGLESTNOGAN.model_weights_path + "best_gen_model%s.h5" % (train_b))
        d_model.save_weights(SINGLESTNOGAN.model_weights_path + "best_d_model%s.h5"  % (train_b))

    @staticmethod
    def save_input(X, y_class, y_items, SIDS, name):
        np.save(SINGLESTNOGAN.inp_path + "X_" + str(name), X)
        np.save(SINGLESTNOGAN.inp_path + "y_class_" + str(name), y_class)
        np.save(SINGLESTNOGAN.inp_path + "y_items_" + str(name), y_items)
        np.save(SINGLESTNOGAN.inp_path + "SIT_" + str(name), SIDS)

    @staticmethod
    def load_input(name):
        X = np.load(SINGLESTNOGAN.inp_path + "X_" + str(name) + ".npy", allow_pickle=True)
        y_class = np.load(SINGLESTNOGAN.inp_path + "y_class_" + str(name) + ".npy", allow_pickle=True)
        y_items = np.load(SINGLESTNOGAN.inp_path + "y_items_" + str(name) + ".npy", allow_pickle=True)
        SIDS = np.load(SINGLESTNOGAN.inp_path + "SIT_" + str(name) + ".npy", allow_pickle=True)
      
        return X, y_class, y_items, SIDS