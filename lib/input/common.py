from sklearn.preprocessing import LabelEncoder
import pickle
from matplotlib import pyplot as plt
from lib.config.controls.paths import Paths
from lib.config.controls.twostage import TWOSTGTRANS

import lib.config.logger as log
logger = log.get_logger()

def create_sessions_buyers(db):
    db['session_id'] = db.session_id.astype(str)
    db['item_id'] = db.item_id.astype(int)
    session_buyers = db.groupby('session_id')['item_id'].apply(lambda g: set(g.values.tolist())).to_dict()
    return session_buyers

def generate_buyer_clicks_sessions_original_item_ids(test_df):
    test_df['session_id'] = test_df[test_df.label == True].session_id.astype(str)
    test_df['item_id'] = test_df.item_id.astype(int)
    return test_df.groupby('session_id')['item_id'].apply(lambda x: x.tolist()).to_dict()
    
def generate_buyer_clicks_sessions_targets(db):
    db['session_id'] = db.session_id.astype(str)

    return db.groupby('session_id')['item_id'].apply(lambda x: set(x.tolist())).to_dict()

def generate_buyer_sessions_order_targets(session_original_clicks, session_buyer_targets):
    session_order_targets = {} 
   

    no_buyers = 0 
    for sid, original_clicks in session_original_clicks.items():
        items_targets = session_buyer_targets[sid]

        la = LabelEncoder()
        la.fit(original_clicks)
        
        inter_o_clicks = set(original_clicks).intersection(items_targets)
        new_encoded_arr = la.transform(list(inter_o_clicks))
        
        if len(inter_o_clicks) == 0:
            no_buyers += 1
            # continue
        
        session_order_targets[sid] = new_encoded_arr

    logger.info("Total no buyers: " + str(no_buyers))

    return session_order_targets

def generate_buyer_sessions_clicks_lengths(test_df):
    buyers = test_df[test_df.label == True].copy()
    buyers['session_id'] = buyers.session_id.astype(str)
    return buyers.groupby('session_id')['item_id'].apply(lambda x: len(x)).to_dict()

def build_buyer_sessions_order_targets(test_df, db):
    session_original_clicks_test = generate_buyer_clicks_sessions_original_item_ids(test_df)
    pickle.dump(session_original_clicks_test, open(Paths.processed_path + "session_original_clicks_test.bin", "wb")) 

    session_buyer_targets = generate_buyer_clicks_sessions_targets(db)
    pickle.dump(session_buyer_targets, open(Paths.processed_path + "session_buyer_targets.bin", "wb")) 
    
    session_order_targets = generate_buyer_sessions_order_targets(session_original_clicks_test, session_buyer_targets)
    pickle.dump(session_order_targets, open(Paths.processed_path + "session_order_targets_test_df.bin", "wb")) 

    buyers_clicks_lengths = generate_buyer_sessions_clicks_lengths(test_df)
    pickle.dump(buyers_clicks_lengths, open(Paths.processed_path + "buyer_clicks_lengths.bin", "wb")) 

def load_session_buyer_targets():
    return pickle.load(open(Paths.processed_path + "session_buyer_targets.bin", "rb"))

def load_session_original_clicks():
    return pickle.load(open(Paths.processed_path + "session_original_clicks_test.bin", "rb"))

def load_session_original_clicks_valid():
    return pickle.load(open(Paths.processed_path + "session_original_clicks_valid.bin", "rb"))

def load_session_order_targets():
    return pickle.load(open(Paths.processed_path + "session_order_targets_test_df.bin", "rb"))

def load_session_buyers_click_lengths():
    return pickle.load(open(Paths.processed_path + "buyer_clicks_lengths.bin", "rb"))

def plot_history(history, path):
    plt.figure()
    plt.plot(history['loss'])
    plt.plot(history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig(path + "loss.png")