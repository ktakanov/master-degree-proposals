import multiprocessing
from collections import Counter
import resource
from lib.input.common import create_sessions_buyers
from scipy import sparse
from sklearn.preprocessing import LabelEncoder
import numpy as np
import gc
import lib.config.logger as log
from lib.utils import flat_list
from lib.config.controls.singlestgan import SINGLESTGAN 
from sklearn.preprocessing import OneHotEncoder
from multiprocessing import Manager, Pool
from sklearn.utils import shuffle
logger = log.get_logger()
import numpy_indexed as npi
from lib.utils.process_input import fit_one_hots, get_gen_outs,apply_encoder_in_batch
from lib.input.baseline.second_stage import create_feat_from_group
from lib.input.single_stg.scale import load_pt_scaler_by_feat_stg


features = SINGLESTGAN.input_features # we not use dwelltime to nodes, just to edges
# features = ['item_id','category','day','hour','weekday','time_by_item','item_click'] # we not use dwelltime to nodes, just to edges
numeric_features = []


DEBUG = True

def smooth_feat(y):
	return y + 0.1 

cat_cols = SINGLESTGAN.cat_features 


features_group_withsid = ['session_id'] + features + ['label']

def create_input_arrays(df, session_buyers, input_name):
  X = []
  y_class = []
  y_items = []
  sids = []
  len_inp = []
  
  N = SINGLESTGAN.model['N']
  
  encoders = fit_one_hots(get_gen_outs(), use_sparse=True)

  arr_df = df[features_group_withsid].values
  grouped = np.array(npi.group_by(arr_df[:, 0]).split(arr_df[:,0:]),dtype='object')
  
  logger.info("Total grouped: " + str(len(grouped)))
  global total_fixed_label
  total_fixed_label = 0
  
  def create_inputs_from_group(group, current_i):    
    global total_fixed_label
    item_col = features_group_withsid.index('item_id') # target_row not has sid
    clicks_arr = group[:,item_col].flatten().tolist()
   
    if current_i % 500 == 0: 
      logger.info("Current input: " + str(round(current_i/len(grouped), 2)))
    
    if len(np.unique(clicks_arr)) > 100:
      logger.error("Problem with clicks_arr: " + str(len(clicks_arr)))

    label_col = features_group_withsid.index('label') # target_row not has sid
    label = group[:,label_col][-1]
    
    sid = str(group[-1][0])
    
    items_label = [0]
    new_encoded_arr = LabelEncoder().fit_transform(clicks_arr) + 1

    id_in_buyer = sid in session_buyers 

    if label == True: 
      label = 1 
    else: 
      label = 0

    
    if label == 1 and id_in_buyer:
      buyers = set(session_buyers[sid])
      if len(buyers) == 0:
        # logger.info("No buyers for this sid: " + str(sid))
        total_fixed_label += 1
        label = 0
      else:
        t_labels = np.zeros(len(clicks_arr))
        
        for i in range(0,len(t_labels)): 
          if clicks_arr[i] in buyers: 
            t_labels[i] = 1
        items_label = t_labels
        if np.array(items_label == 0).all():
          total_fixed_label += 1
          # logger.info("No targets in sid "+ str(sid))
          label = 0
    elif label == 1 and items_label[0] == 0 and len(items_label) == 1:
      label = 0
      total_fixed_label += 1
        # logger.info("No targets in sid "+ str(sid))
      
    y_class.append(label)
    y_items.append(items_label)

    sids.append(sid)
    group[:,item_col]  = new_encoded_arr
    group = group[:,:label_col] #remove label col
    
    group = group[:,1:] # remove session id col
    node_features = np.array(group,dtype=np.float32)
    index_cat_cols =  list(range(0,len(cat_cols)))
    index_num_cols = list(range(len(cat_cols), len(features)))
    
    node_features = apply_encoder_in_batch(node_features, N,  index_cat_cols, index_num_cols, encoders, use_sparse=True)
    X.append(node_features)
    
    
    len_inp.append(node_features.shape[0])
    
    current_i += 1
  total_p = grouped.shape[0]
  # np.vectorize(create_inputs_from_group)
  for k in range(0, total_p):
      create_inputs_from_group(grouped[k], k)

  logger.info("Total removed: " + str(total_fixed_label/total_p))

  del grouped
  return  shuffle(sids, X, y_class, y_items, len_inp)

def create_input_from_split(train_df, session_buyers, input_name):
    logger.info("* Spliting ..")
    SPLIT = 10
    session_ids = np.array_split(train_df.session_id.unique(), SPLIT)

    train_sids = Manager().list()
    x_train  = Manager().list()
    y_train_class  = Manager().list()
    y_train_items  = Manager().list()
    len_inp  = Manager().list()
        
    pool = Pool(processes=SPLIT)
    subdfs = [(train_df[train_df.session_id.isin(sids)], session_buyers, input_name) for sids in session_ids] 

    results = pool.starmap(create_input_arrays, subdfs) 
    
    x_train = [res[1] for res in results]
    y_train_class = [res[2] for res in results]
    y_train_items = [res[3] for res in results]
    len_inp = [res[4] for res in results]
    train_sids = [res[0] for res in results]
    
    x_train = flat_list(x_train)
    y_train_class = flat_list(y_train_class)
    y_train_items = flat_list(y_train_items)
    train_sids = flat_list(train_sids)
    len_inp = flat_list(len_inp)
    
    return shuffle(train_sids, x_train, y_train_class,y_train_items,len_inp)

def save_in_batches(X,y_class,y_items,sids,len_inp,input_name, bsz):
    logger.info("Saving batches..")
    c_batch = 0
    for k in range(0,len(sids), bsz):
        SINGLESTGAN.save_input_for_k(X[k:k+bsz], y_class[k:k+bsz], \
        y_items[k:k+bsz], sids[k:k+bsz], len_inp[k:k+bsz], input_name,str(c_batch))
        c_batch += 1

  
def create_input_for_data(train_df, test_df, db, batch_size=6400):
    import warnings
    warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning) 
    session_buyers = create_sessions_buyers(db)

    logger.info("Total buyers in train_df: "+ str(train_df.groupby('session_id').agg({'label': 'first'}).value_counts()))

    train_sids, X_train, y_train_class, y_train_items, train_len_inp = create_input_from_split(train_df, session_buyers, "train")

    save_in_batches(X_train, y_train_class, y_train_items, train_sids, train_len_inp, "train", batch_size)

    # SINGLESTGAN.save_input(X_train, y_train_class, y_train_items, train_sids, train_len_inp, "train")
    del X_train, y_train_class, y_train_items, train_len_inp, train_sids
    
    test_sids, X_test, y_test_class, y_test_items, test_len_inp = create_input_from_split(test_df, session_buyers, "test")

    logger.info("Total buyers in test_df: "+str(test_df.groupby('session_id').agg({'label': 'first'}).value_counts()))
    

    save_in_batches(X_test, y_test_class, y_test_items, test_sids, test_len_inp, "test", batch_size)

    # SINGLESTGAN.save_input(X_test, y_test_class, y_test_items, test_sids, test_len_inp, "test")