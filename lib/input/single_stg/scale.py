from numpy.core import numeric
from lib.input.common import create_sessions_buyers
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import numpy as np
import gc
import lib.config.logger as log
from lib.utils import flat_list
from lib.config.controls.singlestgan import SINGLESTGAN 
import numpy as np
from sklearn.preprocessing import PowerTransformer, MinMaxScaler, StandardScaler
from lib.config.controls.paths import Paths
from lib.utils import load_total_categories

logger = log.get_logger()
import numpy_indexed as npi
import pickle
gc.collect()
numeric_features = SINGLESTGAN.num_features

features = SINGLESTGAN.input_features # we not use dwelltime to nodes, just to edges
DEBUG = True


def scale_feature_in_input_data(train_df,  test_df, feat):
    scaler = PowerTransformer(standardize=True)
    # scaler = StandardScaler()
    total_cats = load_total_categories()
    
    if feat == 'day':
      scaler.fit([[float(k)] for k in range(0,31+1)])
    elif feat == 'hour':
      scaler.fit([[float(k)] for k in range(0,4+1)])
    elif feat == 'month':
      scaler.fit([[float(k)] for k in range(0,12+1)])
    elif feat == 'weekday':
      scaler.fit([[float(k)] for k in range(0,7+1)])
    elif feat == 'week':
      scaler.fit([[float(k)] for k in range(0,55+1)])      
    elif feat == 'category':
      scaler.fit([[float(k)] for k in range(0,total_cats+1)])
    else:
      scaler.fit(train_df[[feat]].astype(np.float32))

    train_df[feat] = scaler.transform(train_df[[feat]].astype(np.float32))
    test_df[feat] = scaler.transform(test_df[[feat]].astype(np.float32))

    pickle.dump(scaler,open(Paths.scalers_path + "_pt_scaler_"+ feat + "_singlestg.pkl","wb"))
    return train_df, test_df

def save_item_id_scaler():
    scaler = PowerTransformer(standardize=True)
    # scaler = StandardScaler()
    scaler.fit([[float(k)] for k in range(0,SINGLESTGAN.model['N'] +1)])
    pickle.dump(scaler,open(Paths.scalers_path + "_pt_scaler_"+ 'item_id' + "_singlestg.pkl","wb"))

def scale_for_gan(train_df, test_df):
  features_to_scale = numeric_features.copy()
  
  for feat in features_to_scale:
    train_df, test_df = scale_feature_in_input_data(train_df, test_df, feat)
    
  return train_df, test_df

def get_item_scaler(N):
  inp = [[k] for k in range(0,N+1)]
  ptscaler = PowerTransformer(standardize=True).fit(inp)
  t_inp = ptscaler.transform(inp)
  pickle.dump(ptscaler,open(Paths.scalers_path + "_pt_scaler_"+ 'item_id' + "_singlestg.pkl","wb"))

  return ptscaler

def load_pt_scaler_by_feat_stg(feat):
    return pickle.load(open(Paths.scalers_path + '_pt_scaler_' + feat + "_singlestg.pkl","rb"))

def load_mm_scaler_by_feat_stg(feat):
    return pickle.load(open(Paths.scalers_path + '_mm_scaler_' + feat + "_singlestg.pkl","rb"))


# def create_readable_df_stg(inp, gen_outs, encoders, ):
#   cat_cols = range(0,len(SINGLESTGAN.cat_features))

#   N,F = inp.shape
  
#   inp = inp.copy()
#   inp = inp.reshape((N,F))

  
#   new_feat = np.zeros((N, len(SINGLESTGAN.input_features)))
  
  
#   k = 0
#   for feat in SINGLESTGAN.input_features:
#     scaler = load_pt_scaler_by_feat_stg(feat)

#     new_feat[:,k] = scaler.inverse_transform(
#       inp[:,k].reshape(-1,1)
#     ).squeeze()
#     k+=1

#   df = pd.DataFrame(new_feat,columns=features)

#   return df


def create_readable_df_stg(inp, gen_outs, encoders, ):
  cat_cols = range(0,len(SINGLESTGAN.cat_features))

  N,F = inp.shape
  
  inp = inp.copy()
  inp = inp.reshape((N,F))
  
  new_feat = np.zeros((N, len(gen_outs)))
  k = 0 
  i = 0
  while k < len(cat_cols):
    cat_part = inp[:,i:i+gen_outs[k]+1]
  
    new_feat[:,k] = encoders[k].inverse_transform(
      cat_part
    ).squeeze()
    i += gen_outs[k]+1
    k+=1
  
  gen_outs_plus = np.array(gen_outs.copy(),dtype=np.int)
  gen_outs_plus[cat_cols[0]: cat_cols[-1]]  = (np.array(gen_outs_plus[cat_cols[0]: cat_cols[-1]]) +1)
  gen_outs_index = np.cumsum(np.array(gen_outs_plus))
  
  for feat in numeric_features:
    scaler = load_pt_scaler_by_feat_stg(feat)

    gen_idx = gen_outs_index[k]
    new_feat[:,k] = scaler.inverse_transform(
      inp[:,gen_idx].reshape(-1,1)
    ).squeeze()
    k+=1

  df = pd.DataFrame(new_feat,columns=features)

  return df
