from lib.data.features import item_probs
import numpy as np 
from collections import defaultdict
import numpy_indexed as npi
from lib.config.controls.baseline import Baseline

from lib.input.common import create_sessions_buyers

from collections import defaultdict

from .common import *

import lib.config.logger as log

logger = log.get_logger()

def create_f4_f6_f7(group, df_cols, item_arr, unique_items):
  times = group[:,df_cols.index("timestamp")]
  time_diff = [0] + [(t - s).seconds for s, t in zip(times, times[1:])]
  
  f4d = defaultdict(int)
  f6_f7d = defaultdict(list)

  last = item_arr[0]
  for i in range(0,len(item_arr)):
    item_id = item_arr[i] 
    f4d[item_id] += time_diff[i]

    if item_id == last:
        f6_f7d[item_id].append(time_diff[i])

    last = item_id

  def create_f4(item_id):
    return f4d.get(item_id)

  def create_f6(item_id):
    t_list = f6_f7d.get(item_id)
    clicks = 1 if t_list is None else len(t_list) + 1
    return clicks

  def create_f7(item_id):
    t_list = f6_f7d.get(item_id)
    max_dur = max(t_list) if t_list is not None else 0
    return max_dur

  f4_arr = np.vectorize(create_f4)(unique_items)
  f6_arr = np.vectorize(create_f6)(unique_items)
  f7_arr = np.vectorize(create_f7)(unique_items)
  return (f4_arr, f6_arr, f7_arr)

def create_f5(unique_item, item_id_probs):
  def get_item_prob(item_id):
    return item_id_probs.get(item_id,0.0)
  return np.vectorize(get_item_prob)(unique_item)


def create_feat_from_group(groupi, df_cols, item_id_probs):
  index_col = df_cols.index('item_id')
  items_array = groupi[:,index_col]

  unique_items, counts = np.unique(items_array, return_counts=True)
  N = unique_items.shape[0]
  F = len(snd_stg_feat_cols)
  X = np.zeros((N,F))

  session_id = groupi[0][0]
 
  first = items_array[0]
  last = items_array[-1]

  f1 = np.array(first == unique_items)
  f2 = np.array(last == unique_items)
  f3 = counts
  f4,f6,f7 = create_f4_f6_f7(groupi, df_cols, items_array, unique_items)
  f5 = create_f5(unique_items, item_id_probs)

  X[:,snd_stg_feat_cols.index('session_id')] = session_id
  X[:,snd_stg_feat_cols.index('item_id')] = unique_items 
  X[:,snd_stg_feat_cols.index('F1')] = f1
  X[:,snd_stg_feat_cols.index('F2')] = f2
  X[:,snd_stg_feat_cols.index('F3')] = f3
  X[:,snd_stg_feat_cols.index('F4')] = f4
  X[:,snd_stg_feat_cols.index('F5')] = f5
  X[:,snd_stg_feat_cols.index('F6')] = f6
  X[:,snd_stg_feat_cols.index('F7')] = f7
  return X


def create_features(df, item_id_probs, session_buyers, debug=False):
  X = []
  y = []

  df = df[df.label == True] # use just buyers
  arr_df = df.values
  grouped_arr = np.array(npi.group_by(arr_df[:, 0]).split(arr_df[:,0:]))
  total_p = grouped_arr.shape[0]
    
  for k in range(0, total_p):
    session_id = str(grouped_arr[k][0][0])
    X_i = create_feat_from_group(grouped_arr[k], list(df), item_id_probs)
    items_array = X_i[:,1] 
    buyers = session_buyers[session_id] if session_id in session_buyers else set()
    y_i = list(map(lambda x: x in buyers, items_array))

    X.append(X_i)
    y.append(y_i)
  return X,y


def build_input(df, item_id_probs, session_buyers):
    X,y = create_features(df, item_id_probs, session_buyers)
    return X,y

def build_snd_stg_input_for_data(db, item_id_probs, train_df, test_df):

  sessions_buyers = create_sessions_buyers(db)
  passing_datas = (('train',train_df), ('test',test_df))
  
  for name_df in passing_datas:
      name, df = name_df 
      logger.info("Constructing input data: " + str(name))
      X, y = build_input(df, item_id_probs, sessions_buyers)
      logger.debug("X example: " + str(X[0]))
      logger.debug("y example: " + str(y[0]) + str("\n"))

      logger.info("Saving input data: " + str(name))
      Baseline.save_input_snd_stg(X,y,name)
