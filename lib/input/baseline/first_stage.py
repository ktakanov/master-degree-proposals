from collections import defaultdict
from  functools import reduce
from pathlib import Path
import numpy_indexed as npi
import numpy as np
from .common import *
from lib.utils import *
from lib.config.controls.baseline import Baseline

import lib.config.logger as log
logger = log.get_logger()

def create_feat_from_group(groupi, df_cols, item_id_probs, debug=False):
  index_col = df_cols.index('item_id')
  items_array = groupi[:,index_col]

  time_col = df_cols.index("timestamp") 
  times = groupi[:,time_col]
  time_diff = [0] + [(t - s).seconds for s, t in zip(times, times[1:])]

  item_all_counts = defaultdict(int)
  item_consecutive_time = defaultdict(list)

  global last
  last = items_array[0]

  def add_to_dicts(row):
    global last
    item_id = row[0]
    time_item = row[1]
    item_all_counts[item_id] += 1
    if last == item_id:
      item_consecutive_time[item_id].append(time_item)
    last = item_id

  maxi_duration = 0.0
  max_consecutive_clicks = 1

  items_array_times = groupi[:,[index_col, time_col]]
  np.apply_along_axis(add_to_dicts, axis=1, arr=items_array_times)

  for key,value in item_consecutive_time.items():
    times_list = item_consecutive_time[key]
    time_diff_by_item = [0] + [(t - s).seconds for s, t in zip(times_list, times_list[1:])]
    max_consecutive_clicks = max(max_consecutive_clicks, len(time_diff_by_item))
    maxi_duration = max(maxi_duration, max(time_diff_by_item)) 

  def get_item_prob(item_id):
    return item_id_probs.get(item_id,0.0)
  
  item_probs = np.vectorize(get_item_prob)(items_array)
  session_id = groupi[0][0]

  N = len(fst_stg_feat_cols)
  X = np.zeros(N)

  p1 = groupi.shape[0]
  p2 = np.mean(list(item_all_counts.values()))

  p3 = (times[0] - times[-1]).seconds
  p3 = 0 if np.isnan(p3) else p3
  p4 = max(time_diff)
  p5 = np.mean(time_diff)
  p6 = max(list(item_all_counts.values()))
  p7 = np.mean(item_probs)
  p8 = max(item_probs)
  p9 = (1 -  reduce(lambda x, y: x*y, list(map(lambda x: 1 - x, item_probs))))
  p10 = max_consecutive_clicks
  p11 = maxi_duration

  X[fst_stg_feat_cols.index('P1')] = p1
  X[fst_stg_feat_cols.index('P2')] = p2
  X[fst_stg_feat_cols.index('P3')] = p3
  X[fst_stg_feat_cols.index('P4')] = p4
  X[fst_stg_feat_cols.index('P5')] = p5
  X[fst_stg_feat_cols.index('P6')] = p6
  X[fst_stg_feat_cols.index('P7')] = p7
  X[fst_stg_feat_cols.index('P8')] = p8
  X[fst_stg_feat_cols.index('P9')] = p9
  X[fst_stg_feat_cols.index('P10')] = p10
  X[fst_stg_feat_cols.index('P11')] = p11

  return (session_id, X)


def create_features(df, item_id_probs):
  X = []
  SIT = [] # session_ids with unique_items ordered
  y = []

  arr_df = df.values
  grouped_arr = np.array(npi.group_by(arr_df[:, 0]).split(arr_df[:,0:]),dtype='object')

  total_p = grouped_arr.shape[0]
  
  for k in range(0, total_p):
      S_i, X_i = create_feat_from_group(grouped_arr[k], list(df), item_id_probs)
      SIT.append(S_i)
      y.append(grouped_arr[k][:,list(df).index("label")][0])
      X.append(X_i)
  return X, SIT,y

def develop_input_for_single_df(df, item_probs, name):
  X, SIT, y = create_features(df, item_probs)  
  Baseline.save_input_fst_stg(X,y, SIT, name) 

def build_model_input_with_slices(df, item_probs, name, parts=10):
  s_parts = np.array_split(df.session_id.unique(), parts)
  np.save(Baseline.slices_path + str(name) + "_s_parts_" + str(parts), s_parts)

  i = 0
  for sids_part in s_parts:
    logger.debug("Current part: " + str(i))
    df_part = df[df.session_id.isin(sids_part)]
    X_i, SIT_i, y_i = create_features(df_part, item_probs)  
    Baseline.save_slice_input_fst_stg(X_i,y_i, SIT_i, name,i) 
    i+=1

def build_input_with_slices_and_concat(df, item_probs,name, parts=10, rebuild=False):
    s_parts_path = Baseline.slices_path + str(name) + "_s_parts_" + str(parts) + ".npy"
    if not Path(s_parts_path).is_file or rebuild == True:
      logger.info("s_parts not found, we are going to rebuild..")
      build_model_input_with_slices(df, item_probs, name, parts)
      s_parts = np.load(Baseline.slices_path + str(name) + "_s_parts_" + str(parts) + ".npy",allow_pickle=True)
    else:
      logger.info("s_parts found we are not going to rebuild: " + str(name))

    s_parts = np.load(s_parts_path,allow_pickle=True)
    X_ls,SIT_ls,y_ls = [],[],[]
    i=0 
    for _ in s_parts:
      logger.debug("build_model_input_with_slices: current i: " + str(i))
      X_i,y_i,SIT_i = Baseline.load_slice_input_fst_stg(name,i)
      X_ls.append(X_i)
      y_ls.append(y_i)
      SIT_ls.append(SIT_i)
      i+=1

    X = flat_list(X_ls)
    y = flat_list(y_ls)
    SIT = flat_list(SIT_ls)
    logger.debug("build_model_input_with_slices: saving input  ")
    Baseline.save_input_fst_stg(X,y,SIT,name)

def build_fst_stg_input_for_data(item_id_probs, train_df, test_df, rebuild):
  build_input_with_slices_and_concat(train_df, item_id_probs, 'train',rebuild=rebuild) # the building was develop in parts to avoid too memory consumption.
  develop_input_for_single_df(test_df, item_id_probs, 'test')