from tqdm import tqdm
import numpy as np
import pandas as pd
from lib.config.controls.baseline import Baseline
from collections import defaultdict
tqdm.pandas()

snd_stg_feat_cols = (['session_id','item_id','F1','F2','F3','F4','F5','F6','F7'])
fst_stg_feat_cols = (['P1','P2','P3','P4','P5','P6','P7','P8','P9','P10','P11'])

import lib.config.logger as log
logger = log.get_logger()

def create_item_id_probs(train_df):
    df_summary = train_df.copy()
    train_df['timestamp'] = pd.to_datetime(train_df.timestamp)
    session_id_t = df_summary.groupby('session_id').timestamp.first().to_dict()
    
    df_summary['session_t'] = df_summary['session_id'].map(session_id_t)
    item_id_sessions = df_summary.groupby('item_id').progress_apply(lambda x: list(zip(x['session_id'].values, x['session_t'].values, x['label'].values))).to_dict()
    #TODO: we can't use session label we need to use the db as label as we want the item prob not session prob
    MAX_T = Baseline.item_prob_max_session
    item_id_probs = defaultdict(int)
    i = 0
    for key,value in item_id_sessions.items():
        # value = list(set(value))[:500]
        value = list(set(value))
        selected_by_time = sorted(value, key = lambda x: x[1])[:MAX_T]

        labels = list(map(lambda x: 1 if x[2] == True else 0, selected_by_time))
        
        item_id_probs[key] += np.mean(labels)
    i+=1
    return item_id_probs


def create_item_id_probs_with_db(db, train_df):
    df_summary = train_df.copy()
    db['timestamp'] = pd.to_datetime(db.timestamp)
    df_summary['timestamp']= pd.to_datetime(df_summary.timestamp)
    df_summary = df_summary[['item_id', 'timestamp']]
    df_summary['item_label'] = False
    
    db_summary = db[['item_id','timestamp']]
    db_summary['item_label'] = True
    
    final_summary = pd.concat([db_summary, df_summary])
    
    
    item_id_sessions = final_summary.groupby('item_id').progress_apply(lambda x:  list(zip(x['timestamp'].values.tolist(), x['item_label'].values.tolist()))).to_dict()
    
    #TODO: we can't use session label we need to use the db as label as we want the item prob not session prob
    MAX_T = Baseline.item_prob_max_session
    item_id_probs = defaultdict(int)
    i = 0
    for key,value in item_id_sessions.items():
        # value = list(set(value))[:500]        
        # value = list(set(value))
        
        selected_by_time = sorted(value, key = lambda x: x[0])[:MAX_T]
        
        labels = list(map(lambda x: 1 if x[1] == True else 0, selected_by_time))
        
        item_id_probs[key] += np.mean(labels)
        
    i+=1
    return item_id_probs

def load_or_build_item_probs(build_item_args, train_df, db):
    if build_item_args==True:
        logger.info("Building item probs.")
        
        db = db[db.session_id.astype(str).isin(train_df.session_id.astype(str))]
        item_probs = create_item_id_probs_with_db(db, train_df)
        logger.info("Item_probs size: " + str(len(item_probs)))
        logger.info("Saving item_probs")
        Baseline.save_item_probs(item_probs)
        return item_probs
    else:
        return  Baseline.get_item_probs()
    
