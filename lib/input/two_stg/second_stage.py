from lib.input.common import create_sessions_buyers
import numpy as np
from os import path
import gc
from lib.config.controls.twostage import TWOSTGTRANS
import numpy_indexed as npi
from sklearn.preprocessing import LabelEncoder


import lib.config.logger as log
logger = log.get_logger()

from lib.config.controls.twostage import TWOSTGTRANS



features =  TWOSTGTRANS.data_features[1:-1]
 # we not use dwelltime to nodes, just to edges
# features = ['item_id','category','day','hour','weekday','time_by_item','item_click'] # we not use dwelltime to nodes, just to edges
numeric_features = []

DEBUG = True

features_group_withsid = ['session_id'] + features + ['label']

def create_input_arrays(df, session_buyers, create_targets=True, use_multi_label=False):
  global total_invalid
  total_invalid = 0
  X = [] 
  y = []
  sids = []
  f_name = "checkpoint"
  # current_i = 0

  df = df[df.label == True]
  
  arr_df = df[features_group_withsid].values
  grouped = np.array(npi.group_by(arr_df[:, 0]).split(arr_df[:,0:]))
  logger.debug("Total groups: " + str(len(grouped)))

  def create_inputs_from_group(group, current_i):
    global total_invalid

    sid = str(group[-1][0])
    sids.append(sid)
    item_col = features_group_withsid.index('item_id') # target_row not has sid
    clicks_arr = group[:,item_col].flatten().tolist()


    la = LabelEncoder()
    la.fit(clicks_arr)
    new_encoded_arr = la.transform(clicks_arr)
    

    group[:,item_col]  = new_encoded_arr
    
    label_col = features_group_withsid.index('label') # target_row not has sid
    label = group[:,label_col][-1]
    group = group[:,:label_col] #remove label col
    
    group = group[:,1:] # remove session id col
    node_features = np.array(group,dtype=np.float32)

    X.append(node_features)

    if create_targets:

      target_label = [0]
      
      buyers = set(session_buyers[sid]) if sid in session_buyers else set()
      if len(buyers) == 0:
        logger.error("No buyers for this sid: " + str(sid))
  

      if use_multi_label:
        clicks_set = set(clicks_arr) 
    
        intersection = clicks_set.intersection(buyers)
        transformed_labels = la.transform(list(intersection))
        transformed_labels = transformed_labels[~np.isnan(transformed_labels)] #remove nan (buyers that not appears in clicks)
    
        target_label = transformed_labels
    
        if len(target_label) == 0:
          label = 0
      else:
        t_labels = np.zeros(len(clicks_arr))
        for i in range(0,len(t_labels)): 
          if clicks_arr[i] in buyers: t_labels[i] = 1
        target_label = t_labels
        
      y.append(target_label)
    current_i += 1

  total_p = grouped.shape[0]
  # np.vectorize(create_inputs_from_group)

  for k in range(0, total_p):
      create_inputs_from_group(grouped[k], k)
  del grouped

  logger.debug("Total invalids: " + str(total_invalid))
  return  sids, X, y

 
def create_inputs_for_second(db, train_df, test_df, use_multi_label=False):

  session_buyers = create_sessions_buyers(db)
  sid_train, X_train, y_train = create_input_arrays(train_df, session_buyers,use_multi_label=use_multi_label)
  sid_test, X_test, y_test = create_input_arrays(test_df, session_buyers,use_multi_label=use_multi_label)
  
  logger.debug("X_train[0] example: " + str(X_train[0])) 
  logger.debug("y_train[0] example: " + str(y_train[0])) 
  
  logger.info("Saving two-stage input..")
  TWOSTGTRANS.save_input_snd_stg(X_train,y_train,sid_train,"train")
  TWOSTGTRANS.save_input_snd_stg(X_test,y_test,sid_test,"test")