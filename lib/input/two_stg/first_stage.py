from sklearn.preprocessing import LabelEncoder
from lib.input.common import create_sessions_buyers
import numpy as np
from lib.config.controls.twostage import TWOSTGTRANS
import numpy_indexed as npi
from collections import Counter
from sklearn.utils import shuffle
import lib.config.logger as log
logger = log.get_logger()

from lib.config.controls.twostage import TWOSTGTRANS


features =  TWOSTGTRANS.data_features[1:-1]
# features = ['item_id','category','day','hour','weekday','time_by_item','item_click'] # we not use dwelltime to nodes, just to edges
numeric_features = []

DEBUG = True

features_group_withsid = ['session_id'] + features + ['label']


def encode_node_features(node_features):
    
    
    pass

def create_input_arrays(df, session_buyers):
    global total_invalid
    total_invalid = 0
    X = [] 
    y_label = []
    sids = []

    arr_df = df[features_group_withsid].values
    grouped = np.array(npi.group_by(arr_df[:, 0]).split(arr_df[:,0:]),dtype='object')
    logger.debug("Total groups",len(grouped))
    
    global total_fixed_label
    total_fixed_label = 0
    

    def create_inputs_from_group(group, current_i):
        global total_fixed_label
        global total_invalid
    
        
        if current_i % 500 == 0:
            logger.debug("Current i",current_i)

        label_col = features_group_withsid.index('label') # target_row not has sid
        label = group[:,label_col][-1]

        item_col = features_group_withsid.index('item_id') 
        clicks_arr = group[:,item_col].flatten().tolist()

   
        sid = str(group[-1][0])
        
        if label == True:
            label = 1

        item_col = features_group_withsid.index('item_id') # target_row not has sid
        clicks_arr = group[:,item_col].flatten().tolist()
        new_encoded_arr = LabelEncoder().fit_transform(clicks_arr) + 1

        id_in_buyer = sid in session_buyers 
        items_label = [0]
        
    
        if label == 1 and id_in_buyer:
       
            buyers = set(session_buyers[sid])
    
            if len(buyers) == 0:
                # logger.info("No buyers for this sid: " + str(sid))
                total_fixed_label += 1
                label = 0
            else:
                t_labels = np.zeros(len(clicks_arr))
                
                for i in range(0,len(t_labels)): 
                    if clicks_arr[i] in buyers: 
                      t_labels[i] = 1
                items_label = t_labels
                if np.array(items_label == 0).all():
                    total_fixed_label += 1
                # logger.info("No targets in sid "+ str(sid))
                    label = 0
           
            # logger.info("No targets in sid "+ str(sid))
      
        y_label.append(label)
        sids.append(sid)
        
        group[:,item_col]  = new_encoded_arr
        group = group[:,:label_col] #remove label col
        
        group = group[:,1:] # remove session id col
        node_features = np.array(group,dtype=np.float32)
        

        X.append(node_features)

        current_i += 1

    total_p = grouped.shape[0]

    logger.info("Total removed: " + str(total_fixed_label/total_p))    
    # np.vectorize(create_inputs_from_group)
    for k in range(0, total_p):
        create_inputs_from_group(grouped[k], k)
    del grouped
    return  shuffle(sids, X, y_label)

def create_inputs_for_first(train_df, test_df, db):
    session_buyers = create_sessions_buyers(db)
    
    logger.info("train_df.head()\n" +str(train_df.head()))
    logger.info("train_df.describe()\n" + str(train_df.describe()))
    
    sid_train, X_train, y_train = create_input_arrays(train_df, session_buyers)
        
    logger.info("test_df.head()\n" +str(test_df.head()))
    logger.info("test_df.describe()\n" + str(test_df.describe()))
    
    logger.info(Counter(y_train))
    sid_test, X_test, y_test = create_input_arrays(test_df, session_buyers)
   
    logger.info("Saving two-stage input..")
    TWOSTGTRANS.save_input_fst_stg(X_train,y_train,sid_train,"train")
    TWOSTGTRANS.save_input_fst_stg(X_test,y_test,sid_test,"test")