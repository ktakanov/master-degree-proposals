tree_assets = ["assets/input",
"assets/input/baseline",
"assets/input/baseline/first",
"assets/input/baseline/second",
"assets/input/baseline/slices",
"assets/input/single_stage",
"assets/input/two_stage",
"assets/input/two_stage/first",
"assets/input/two_stage/second",
"assets/log",
"assets/misc",
"assets/model",
"assets/model/single_stage",
"assets/model/single_stage/losses",
"assets/model/single_stage/weights",
"assets/model/two_stage",
"assets/model/two_stage/first",
"assets/model/two_stage/first/checkpoint",
"assets/model/two_stage/first/history",
"assets/model/two_stage/second",
"assets/model/two_stage/second/checkpoint",
"assets/model/two_stage/second/history",
"assets/processed",
"assets/raw",
"assets/weights",
"assets/weights/baseline",
"assets/weights/baseline/first",
"assets/weights/baseline/second"]

import os
import wget

for directory in tree_assets:
  if not os.path.exists(directory):
    os.makedirs(directory)
    print("Successful created directory: ",directory)
  else:
    print("Directory ",directory,"exists..")
